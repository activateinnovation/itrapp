//
//  ShootersPage.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ShootersPage.h"
#include<unistd.h> 
#include<netdb.h>

@interface ShootersPage ()

@end

@implementation ShootersPage
@synthesize tableView;
@synthesize scoringPage;
@synthesize settingsPage;
@synthesize stayLoggedIn;
@synthesize logInViewController;
@synthesize signUpViewController;
@synthesize userDefaults;
@synthesize container;
@synthesize addShooterView;
@synthesize firstName;
@synthesize lastName;
@synthesize phoneNum;
@synthesize emailAddress;
@synthesize emergencyContact;
@synthesize emergencyContactNum;
@synthesize chokeType;
@synthesize shootersArray;
@synthesize managedObjectContext;
@synthesize connection;
@synthesize parseShooters;
@synthesize numOfParseShooters;
@synthesize numOfSavedShooters;
@synthesize appDelegate;
@synthesize updatedData;
@synthesize spinner;
@synthesize cancelAddShooterOutlet;
@synthesize navBar;
@synthesize addShooterOutlet;
@synthesize shooterToDelete;
@synthesize indexPathToDelete;
@synthesize editShooter;
@synthesize ataNumber;
@synthesize ataCategoriesArray;
@synthesize ataCategory;
@synthesize ataClass;
@synthesize ataClassesArray;
@synthesize handicap;
@synthesize handicapNumbers;
@synthesize pickerView;



- (void) viewWillAppear:(BOOL)animated{
    
    //Pulls all current shooters from persistent memory and sorts them by firstname from the Core Data database
   
    self.parseShooters = [[NSMutableArray alloc] init];
    
    self.shootersArray = [[NSMutableArray alloc] init];
    self.updatedData = [[UpdatedData alloc] init];
    self.updatedData.delegate = self;
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];

    [spinner startAnimating];
   
    self.ataCategoriesArray = [[NSMutableArray alloc] initWithObjects: @"N/A", @"Lady",  @"Lady II",  @"Sub-Veteran",  @"Veteran",  @"Sr. Veteran",  @"Sub-Junior",  @"Junior",  @"Jr. Gold", nil];
    self.ataClassesArray = [[NSMutableArray alloc] initWithObjects: @"N/A", @"AAA", @"AA", @"A", @"B", @"C", @"D", nil];
    self.handicapNumbers = [[NSMutableArray alloc] init];
    
    for(int x = 18; x < 28; x++){
        
        
        [self.handicapNumbers addObject:[NSNumber numberWithInt:x]];
        
    }
    self.cancelAddShooterOutlet.enabled = NO;
    [self.updatedData getShootersNew:NO];
    //[self.updatedData getCoaches];
    //[self.updatedData getShoots];
     //[self.updatedData getShootShooters];
    
  /*  if(shootersArray.count == 0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Shooters" message:@"It looks like you have not added any shooters in your account. Please make sure that add shooters to your team to get started." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }*/
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
    [background setFrame: CGRectMake(0, 0, 320, self.tableView.frame.size.height)];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.tableView setBackgroundView:background];
    

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [refreshControl setTintColor:[UIColor colorWithRed:(91/255.f) green:(192/255.f) blue:(232/255.f) alpha:1.0f]];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    self.tabBar.delegate = self;
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
    
    self.settingsPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Settings"];
   
   [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex: 0]];
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    //NSLog(@"%@", [PFUser currentUser].username);
    
    //self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 60, 320, 520)];
    self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];
    self.addShooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height + 300)];
    self.addShooterView.clearsContextBeforeDrawing = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [tap setNumberOfTouchesRequired:1];
    
    [self.addShooterView addGestureRecognizer:tap];
    
    
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 240)];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.pickerView.tag = 1;
    //[self.addShooterView addSubview:self.pickerView];
   
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    //id delegate = [[UIApplication sharedApplication] delegate];
   ;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) retrieveData{
    
    [self.shootersArray removeAllObjects];

    [self.updatedData getShootersNew: NO];
    //NSLog(@"ShootersPageData: %@", self.shootersArray);
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    [self.updatedData getShootersNew:NO];
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshControl endRefreshing];
    });
}

-(void) gotShooters:(NSArray *)shooters{
    
    [self.shootersArray removeAllObjects];
    self.shootersArray = [shooters mutableCopy];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [spinner stopAnimating];
    });
}


#pragma mark TABLEVIEW DELEGATE METHODS
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    //Subscription *subscription = [Subscription MR_findFirstByAttribute:@"parseId" withValue:[PFUser currentUser][@"subscriptionId"]];
    
    PFQuery *subQuery = [PFQuery queryWithClassName:@"Subscription"];
    [subQuery whereKey:@"objectId" containsString:[PFUser currentUser][@"SubscriptionId"]];
    [subQuery fromLocalDatastore];
    PFObject *subscription = [PFObject objectWithClassName:@"Subscription"];
    subscription = [subQuery getFirstObject];
    int shooterCap = [subscription[@"shooterCap"] intValue];
    NSLog(@"Shooter Cap: %d", shooterCap);

    
    
    if(self.shootersArray.count > shooterCap){
        
       return shooterCap;
        
    }
    else{
        
        return [self.shootersArray count];
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    MSCMoreOptionTableViewCell *cell = (MSCMoreOptionTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) {
        
        cell = [[MSCMoreOptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       
        cell.delegate = self;
    
    }
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    [cell setBackgroundColor:[UIColor clearColor]];
    
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iTrappRoundedRectangle.png"]];
    [background setFrame: CGRectMake(10, 25, 300, 145)];
    [background setContentMode:UIViewContentModeScaleToFill];

    
    PFObject *shooterTemp = [PFObject objectWithClassName: @"Shooter"];
    shooterTemp = [self.shootersArray objectAtIndex:indexPath.row];
    
    UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 7, 185, 25)];
    [cellTitle setFont:[UIFont boldSystemFontOfSize:18]];
    [cellTitle setText: [NSString stringWithFormat:@"%d. %@ %@", (int)(indexPath.row + 1), shooterTemp[@"fName"], shooterTemp[@"lName"]]];
    [cellTitle setTextColor:[UIColor colorWithRed:(105/255.f) green: (138/255.f) blue: (51/255.f) alpha:1.0f]];
    
    
    UILabel *shooterPhone = [[UILabel alloc] initWithFrame:CGRectMake(180, 9, 180, 25)];
    [shooterPhone setTextColor:[UIColor darkGrayColor]];
    [shooterPhone setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"phone"] isEqualToString:@""]){
        shooterPhone.text = @"Phone: NA";
    }else{
        shooterPhone.text = [NSString stringWithFormat:@"Phone: %@", shooterTemp[@"phone"]];
    }
    
    UILabel *shooterEmail = [[UILabel alloc] initWithFrame:CGRectMake(28, 36, 260, 25)];
    [shooterEmail setTextColor:[UIColor darkGrayColor]];
     [shooterEmail setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"email"] isEqualToString:@""]){
        shooterEmail.text = @"Email: NA";
    }else{
        shooterEmail.text = [NSString stringWithFormat:@"Email: %@", shooterTemp[@"email"]];
    }
  
    UILabel *shooterEmergencyContact = [[UILabel alloc] initWithFrame:CGRectMake(28, 56, 240, 25)];
    [shooterEmergencyContact setTextColor:[UIColor darkGrayColor]];
     [shooterEmergencyContact setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"emergencyContact"] isEqualToString:@""]){
        shooterEmergencyContact.text = @"E-Contact: NA";
    }else{
        shooterEmergencyContact.text = [NSString stringWithFormat:@"E-Contact: %@", shooterTemp[@"emergencyContact"]];
    }

    UILabel *shooterEmergencyPhone = [[UILabel alloc] initWithFrame:CGRectMake(28, 77, 240, 25)];
    [shooterEmergencyPhone setTextColor:[UIColor darkGrayColor]];
     [shooterEmergencyPhone setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"emergencyContactNum"] isEqualToString:@""]){
        shooterEmergencyPhone.text = @"E-Phone: NA";
    }else{
        shooterEmergencyPhone.text = [NSString stringWithFormat:@"E-Phone: %@", shooterTemp[@"emergencyContactNum"]];
    }
    
    
    UILabel *ataNumber1 = [[UILabel alloc] initWithFrame:CGRectMake(28, 98, 240, 25)];
    [ataNumber1 setTextColor:[UIColor darkGrayColor]];
    [ataNumber1 setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"ataNumber"] isEqualToString:@""] || shooterTemp[@"ataNumber"] == NULL){
        ataNumber1.text = @"ATA Number: NA";
    }else{
        ataNumber1.text = [NSString stringWithFormat:@"ATA Number: %@", shooterTemp[@"ataNumber"]];
    }
    
    UILabel *handicap1 = [[UILabel alloc] initWithFrame:CGRectMake(28, 116, 240, 25)];
    [handicap1 setTextColor:[UIColor darkGrayColor]];
    [handicap1 setFont:[UIFont systemFontOfSize:14]];
    if(!shooterTemp[@"handicap"]){
        handicap1.text = @"Handicap: NA";
    }else{
        handicap1.text = [NSString stringWithFormat:@"Handicap: %d yrds", [shooterTemp[@"handicap"] intValue]];
    }
    
    UILabel *class1 = [[UILabel alloc] initWithFrame:CGRectMake(28, 134, 240, 25)];
    [class1 setTextColor:[UIColor darkGrayColor]];
    [class1 setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"class"] isEqualToString:@""] || shooterTemp[@"class"] == NULL){
        class1.text = @"Class: NA";
    }else{
        class1.text = [NSString stringWithFormat:@"Class: %@", shooterTemp[@"class"]];
    }
    
    UILabel *category1 = [[UILabel alloc] initWithFrame:CGRectMake(120, 134, 240, 25)];
    [category1 setTextColor:[UIColor darkGrayColor]];
    [category1 setFont:[UIFont systemFontOfSize:14]];
    if([shooterTemp[@"category"] isEqualToString:@""] || shooterTemp[@"category"] == NULL){
        
        category1.text = @"Category: NA";
    }else{
        category1.text = [NSString stringWithFormat:@"Category: %@", shooterTemp[@"category"]];
    }


    [cell addSubview:background];
    [cell sendSubviewToBack:background];

    [cell addSubview:category1];
    [cell addSubview:class1];
    [cell addSubview:handicap1];
    [cell addSubview:ataNumber1];
    [cell addSubview:shooterEmergencyContact];
    [cell addSubview:shooterEmergencyPhone];
    [cell addSubview:shooterEmail];
    [cell addSubview:shooterPhone];
    [cell addSubview: cellTitle];
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];

    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;

    NSArray *finalEmails = [[NSArray alloc] initWithObjects:self.shootersArray[indexPath.row][@"email"], nil];
    [picker setToRecipients: finalEmails];
    [picker setSubject:@"iTrapp Direct Message"];

    
    if([MFMailComposeViewController canSendMail] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
    }
    else if([self isNetworkAvailable] == NO){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"There is no network connection available to send emails currently." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if([self isNetworkAvailable] == NO && [MFMailComposeViewController canSendMail]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"These is no network connection and your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    
    
    [self dismissViewControllerAnimated:NO completion: ^{
        
        if(result == MFMailComposeResultSent){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Sent" message:@"The email was successfully sent." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if(result == MFMailComposeResultFailed)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"The email failed to send." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            //  NSLog(@"Canceled");
            
        }
    }];

    
}


#pragma mark TAB BAR DELEGATE

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    
    if(item.tag == 2){
        
        [self presentViewController:scoringPage
                           animated:YES
                         completion:nil];
        
    }
    else if(item.tag == 3){
        
        [self presentViewController:settingsPage
                           animated:YES
                         completion:nil];
        
    }
    else{
        
        
    
    }
}


- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
    
       // if([self isNetworkAvailable]){
            
            
            
       // NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        // Retrieve the first person who have the given firstname
        NSString *objectId = [[shootersArray objectAtIndex:indexPath.row] objectId];
            self.indexPathToDelete = indexPath;
            PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
            [query fromLocalDatastore];
            self.shooterToDelete = [query getObjectWithId:objectId];
            NSLog(@"Shooter To Delete: %@", self.shooterToDelete);//[Shooter MR_findFirstByAttribute:@"objectId" withValue:objectId inContext:localContext];
        
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Shooter" message:[NSString stringWithFormat: @"This will delete all records of this shooter and related shoots. Are you sure you want to delete %@ %@ from your team?", shooterToDelete[@"fName"], shooterToDelete[@"lName"]] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"DELETE", nil];
            
            [alert show];
            alert.tag = 1;
            
         //       }
       // else{
            
           /* UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to delete a shooter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }*/
        
    }
}

#pragma mark ADD SHOOTER VIEW
//This action will flip the tableview to reveal the add shooter form
- (IBAction)addShooter:(id)sender {
   
    PFQuery *subQuery = [PFQuery queryWithClassName:@"Subscription"];
    [subQuery fromLocalDatastore];
    [subQuery whereKey:@"objectId" containsString:[PFUser currentUser][@"SubscriptionId"]];
    PFObject *subscription = [PFObject objectWithClassName:@"Subscription"];
    subscription = [subQuery getFirstObject];
    int shooterCap = [subscription[@"shooterCap"] intValue];
    NSLog(@"Shooter Cap: %d", shooterCap);
    
    if(self.shootersArray.count > shooterCap)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reached Shooter Cap" message:@"You may not add any more shooters to this account level. If you woud like to manage more shooters, please head to our website at http://itrappshoot.com to manage your account settings." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else{
        
        [UIView transitionWithView:self.container
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromBottom
                        animations:^{
                            
                            [self setUpAddShooterView];
                            [self.container addSubview: self.addShooterView];
                            [self.view bringSubviewToFront:self.container];
                            
                        }
                        completion:NULL];
        
    }
}


-(void) setUpAddShooterView {
    
    
    [self.addShooterView setBackgroundColor:[UIColor lightGrayColor]];
    self.navBar.prompt = nil;
    self.cancelAddShooterOutlet.enabled = YES;
    self.addShooterOutlet.enabled = NO;
    
    for(UIView *view in self.addShooterView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    
    //ADD A SCORLL VIEW HERE
    
    UIScrollView *scroll = [[UIScrollView alloc] init];
    scroll.frame = CGRectMake(0, 0, self.addShooterView.frame.size.width, self.addShooterView.frame.size.height - 300);
    [scroll setScrollEnabled:YES];
    [scroll setUserInteractionEnabled:YES];
    scroll.contentSize = self.addShooterView.frame.size;
    scroll.delegate = self;
    
    
    [self.addShooterView addSubview:scroll];
    
    //[scroll addSubview:self.addShooterView];
    
    UIImageView *backgroundImage;
    
    UIButton *addShooter;
    UILabel *shooterInfoLabel;
    UILabel *ATAInformation;
    
    if(self.view.frame.size.width > 320)
    {
    
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
        [backgroundImage setFrame:self.addShooterView.frame];
        [self.addShooterView addSubview:backgroundImage];
        [self.addShooterView sendSubviewToBack:backgroundImage];
        
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 43, 280, 50)];
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 9, 93, 276, 50)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 12, 143, 283, 52)];
        emailAddress = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 193, 280, 50)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 246, 280, 45)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 298, 280, 45)];
        chokeType = [[UISegmentedControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3.12, 355, 280, 50)];
        
        ataNumber = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 + 10, 455, 240, 50)];
        handicap = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 + 10, 510, 240, 50)];
        ataClass = [[UITextField alloc] initWithFrame:CGRectMake(235, 565, 150, 50)];
        ataCategory = [[UITextField alloc] initWithFrame:CGRectMake(385, 565, 150, 50)];
        
        addShooter = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.5 + 5, 635, 150, 50)];
        shooterInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(310, 11, 200, 25)];
        ATAInformation = [[UILabel alloc] initWithFrame:CGRectMake(300, 425, 200, 25)];
    }
    else{
        
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
        [backgroundImage setFrame:CGRectMake(0, 0, 320, self.addShooterView.frame.size.height - 300)];
        [self.addShooterView addSubview:backgroundImage];
        [self.addShooterView sendSubviewToBack:backgroundImage];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(40, 27, 240, 50)];

        lastName = [[UITextField alloc] initWithFrame:CGRectMake(42, 79, 236, 50)];
        
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(38, 132, 242, 52)];
         emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(40, 187, 240, 50)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(7, 247, 150, 50)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(164, 247, 150, 50)];
        
        ataNumber = [[UITextField alloc] initWithFrame:CGRectMake(40, 405, 240, 50)];
        handicap = [[UITextField alloc] initWithFrame:CGRectMake(40, 460, 240, 50)];
        ataClass = [[UITextField alloc] initWithFrame:CGRectMake(7, 515, 150, 50)];
        ataCategory = [[UITextField alloc] initWithFrame:CGRectMake(164, 515, 150, 50)];
        
         chokeType = [[UISegmentedControl alloc] initWithFrame: CGRectMake(20, 317, 280, 40)];
          addShooter = [[UIButton alloc] initWithFrame:CGRectMake(85, 657, 150, 50)];
         shooterInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 4, 200, 25)];
        ATAInformation = [[UILabel alloc] initWithFrame:CGRectMake(15, 375, 200, 25)];
        
    }

    [self.chokeType setBackgroundColor:[UIColor whiteColor]];
    [self.chokeType setTintColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f]];
    [self.chokeType.layer setBorderColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f].CGColor];
    [self.chokeType.layer setCornerRadius:5.0f];
    [self.chokeType.layer setBorderWidth:2.0f];
    
    
    
    [shooterInfoLabel setTextAlignment:NSTextAlignmentLeft];
    [shooterInfoLabel setText:@"Shooter Information"];
    [shooterInfoLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [shooterInfoLabel setTextColor:[UIColor colorWithRed:(105/255.f) green:(138/255.f) blue:(51/255.f) alpha:1.0f]];
    
    
    [ATAInformation setTextAlignment:NSTextAlignmentLeft];
    [ATAInformation setFont:[UIFont boldSystemFontOfSize:16]];
    [ATAInformation setText:@"ATA Information (Optional)"];
    [ATAInformation setTextColor: [UIColor colorWithRed:(105/255.f) green:(138/255.f) blue:(51/255.f) alpha:1.0f]];
    
    
    
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setFont:[UIFont systemFontOfSize:14]];
    [firstName setReturnKeyType:UIReturnKeyNext];
    [firstName setTextAlignment:NSTextAlignmentCenter];
    [firstName setBackground:[UIImage imageNamed:@"iTRAPPfirstName.png"]];
    //[firstName setBackgroundColor:[UIColor clearColor]];
    [firstName setBorderStyle:UITextBorderStyleNone];
    [firstName setPlaceholder:@"First Name"];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
   
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackgroundColor:[UIColor clearColor]];
    [lastName setFont:[UIFont systemFontOfSize:14]];
    [lastName setBorderStyle:UITextBorderStyleNone];
    [lastName setPlaceholder:@"Last Name"];
    [lastName setTextAlignment:NSTextAlignmentCenter];
    [lastName setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor clearColor]];
    [phoneNum setBorderStyle:UITextBorderStyleNone];
    [phoneNum setFont:[UIFont systemFontOfSize:14]];
    [phoneNum setPlaceholder:@"   Phone Number (Optional)"];
    [phoneNum setTextAlignment:NSTextAlignmentCenter];
    [phoneNum setBackground:[UIImage imageNamed:@"iTRAPPphone.png"]];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setReturnKeyType:UIReturnKeyNext];
    [emailAddress setBackgroundColor:[UIColor clearColor]];
    [emailAddress setBorderStyle:UITextBorderStyleNone];
    [emailAddress setFont:[UIFont systemFontOfSize:14]];
    [emailAddress setPlaceholder:@"   Email Address (Required)"];
    [emailAddress setTextAlignment:NSTextAlignmentCenter];
    [emailAddress setBackground:[UIImage imageNamed:@"iTRAPPemailAddressBox.png"]];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

    [emergencyContact setKeyboardType:UIKeyboardTypeDefault];
    [emergencyContact setReturnKeyType:UIReturnKeyNext];
    [emergencyContact setBackgroundColor:[UIColor clearColor]];
    [emergencyContact setBorderStyle:UITextBorderStyleNone];
    [emergencyContact setFont:[UIFont systemFontOfSize:14]];
    [emergencyContact setPlaceholder:@"E-Contact (Optional)"];
    [emergencyContact setTextAlignment:NSTextAlignmentCenter];
    [emergencyContact setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [emergencyContact addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emergencyContactNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [emergencyContactNum setReturnKeyType:UIReturnKeyNext];
    [emergencyContactNum setBackgroundColor:[UIColor clearColor]];
    [emergencyContactNum setBorderStyle:UITextBorderStyleNone];
    [emergencyContactNum setFont:[UIFont systemFontOfSize:14]];
    [emergencyContactNum setPlaceholder:@"E-Phone # (Optional)"];
    [emergencyContactNum setTextAlignment:NSTextAlignmentCenter];
    [emergencyContactNum setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [emergencyContactNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

    [ataNumber setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [ataNumber setReturnKeyType:UIReturnKeyNext];
    [ataNumber setBackgroundColor:[UIColor clearColor]];
    [ataNumber setFont:[UIFont systemFontOfSize:14]];
    [ataNumber setBorderStyle:UITextBorderStyleNone];
    [ataNumber setPlaceholder:@"ATA Number"];
    [ataNumber setTextAlignment:NSTextAlignmentCenter];
    [ataNumber setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [ataNumber addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    handicap.inputView = self.pickerView;
    //[handicap setKeyboardType:UIKeyboardTyp];
    handicap.delegate = self;
    [handicap setReturnKeyType:UIReturnKeyNext];
    [handicap setBackgroundColor:[UIColor clearColor]];
    [handicap setFont:[UIFont systemFontOfSize:14]];
    [handicap setBorderStyle:UITextBorderStyleNone];
    [handicap setPlaceholder:@"ATA Handicap Yrds"];
    [handicap setTextAlignment:NSTextAlignmentCenter];
    [handicap setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [handicap addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
   
    ataCategory.inputView = self.pickerView;
    [ataCategory setKeyboardType:UIKeyboardTypeDefault];
    ataCategory.delegate = self;
    [ataCategory setReturnKeyType:UIReturnKeyNext];
    [ataCategory setBackgroundColor:[UIColor clearColor]];
    [ataCategory setFont:[UIFont systemFontOfSize:14]];
    [ataCategory setBorderStyle:UITextBorderStyleNone];
    [ataCategory setPlaceholder:@"ATA Category"];
    [ataCategory setTextAlignment:NSTextAlignmentCenter];
    [ataCategory setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [ataCategory addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    ataClass.delegate = self;
    ataClass.inputView = self.pickerView;
    [ataClass setKeyboardType:UIKeyboardTypeDefault];
    [ataClass setReturnKeyType:UIReturnKeyDone];
    [ataClass setBackgroundColor:[UIColor clearColor]];
    [ataClass setFont:[UIFont systemFontOfSize:14]];
    [ataClass setBorderStyle:UITextBorderStyleNone];
    [ataClass setPlaceholder:@"ATA Class"];
    [ataClass setTextAlignment:NSTextAlignmentCenter];
    [ataClass setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [ataClass addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [chokeType insertSegmentWithTitle:@"Full" atIndex:0 animated:NO];
    [chokeType insertSegmentWithTitle:@"Modified" atIndex:1 animated:NO];
    [chokeType insertSegmentWithTitle:@"Improved" atIndex:2 animated:NO];
    [chokeType setSelectedSegmentIndex:1];
  

    [addShooter setBackgroundImage:[UIImage imageNamed:@"iTRAPPsaveshooter.png"] forState:UIControlStateNormal];
    [addShooter addTarget:self action:@selector(addShooter) forControlEvents:UIControlEventTouchUpInside];
    [addShooter setEnabled:true];
    

    [scroll addSubview:ATAInformation];
    [scroll addSubview:shooterInfoLabel];
    [scroll addSubview:ataNumber];
    [scroll addSubview:ataClass];
    [scroll addSubview:ataCategory];
    [scroll addSubview:handicap];
    [scroll addSubview:emergencyContact];
    [scroll addSubview:emergencyContactNum];
    [scroll addSubview:firstName];
    [scroll addSubview:lastName];
    [scroll addSubview:phoneNum];
    [scroll addSubview:emailAddress];
  
    [scroll addSubview:chokeType];
    [scroll addSubview:addShooter];

}


//Adds shooter to parse and core data
-(void) addShooter {

    if((firstName.text.length < 1 || lastName.text.length < 1 || self.emailAddress.text.length < 4))
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
  
    NSString *choke= [self.chokeType titleForSegmentAtIndex:[self.chokeType selectedSegmentIndex]];

    NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", self.firstName.text, self.lastName.text]];
            
            PFObject *shooterObject = [PFObject objectWithClassName:@"Shooter"];
            shooterObject[@"appObjectId"] = tempHashId;
            shooterObject[@"fName"] = self.firstName.text;
            shooterObject[@"lName"] = self.lastName.text;
            shooterObject[@"phone"] = self.phoneNum.text;
            shooterObject[@"email"] = self.emailAddress.text;
            shooterObject[@"emergencyContact"] = self.emergencyContact.text;
            shooterObject[@"ataNumber"] = self.ataNumber.text;
            shooterObject[@"handicap"] = [NSNumber numberWithInt: [self.handicap.text intValue]];
            shooterObject[@"class"] = self.ataClass.text;
            shooterObject[@"category"] = self.ataCategory.text;
            shooterObject[@"emergencyContactNum"] = self.emergencyContactNum.text;
            shooterObject[@"choke"] = choke;
            shooterObject[@"userId"] = [PFUser currentUser].objectId;
        
        [shooterObject pin];
        [shooterObject saveEventually];
    
        if([self isNetworkAvailable]){
            [self.updatedData getShootersNew:YES];
        }
        else{
            [self.updatedData getShootersNew:NO];
        }
        
    for(UIView *view in self.addShooterView.subviews){
            if ([view isKindOfClass:[UIView class]]) {
                [view removeFromSuperview];
                
            }
        }
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addShooterView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
 
    self.addShooterOutlet.enabled = YES;
     self.navBar.prompt = @"Swipe left to edit shooter";
    self.cancelAddShooterOutlet.enabled = NO;
        
    }
}


-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        //NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        //NSLog(@"-> connection established!\n");
        return YES;
    }
}

-(void) nextOnKeyboard : (id) sender {
    
   if(sender == self.firstName)
   {
       [self.firstName resignFirstResponder];
       [self.lastName becomeFirstResponder];
   }
   else if(sender == self.lastName){
       
       [self.lastName resignFirstResponder];
       [self.self.phoneNum becomeFirstResponder];
   }
   else if(sender == self.phoneNum){
       
       [self.phoneNum resignFirstResponder];
       [self.emailAddress becomeFirstResponder];
   }
   else if (sender == self.emailAddress){
       
       [self.emailAddress resignFirstResponder];
       [self.emergencyContact becomeFirstResponder];
       
   }
   else if (sender == self.emergencyContact){
       
       [self.emergencyContact resignFirstResponder];
       [self.emergencyContactNum becomeFirstResponder];
       
   }
   else if(sender == self.emergencyContactNum){
       
       [self.emergencyContactNum resignFirstResponder];
       [self.ataNumber becomeFirstResponder];
       
   }
   else if(sender == self.ataNumber){
       
       [self.ataNumber resignFirstResponder];
       [self.handicap becomeFirstResponder];

       
   }
   else if(sender == self.handicap){
       
       [self.handicap resignFirstResponder];
       [self.ataClass becomeFirstResponder];
       
       
   }
   else if(sender == self.ataClass){
       
       [self.ataClass resignFirstResponder];
       [self.ataCategory becomeFirstResponder];
    
   }
   else{  //ATA Category
       
       [self.ataCategory resignFirstResponder];
       
   }
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

-(void) tapGesture {
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    [self.emergencyContact resignFirstResponder];
    [self.emergencyContactNum resignFirstResponder];
    [self.ataNumber resignFirstResponder];
    [self.handicap resignFirstResponder];
    [self.ataClass resignFirstResponder];
    [self.ataCategory resignFirstResponder];
}
- (IBAction)cancelAddShooter:(id)sender {
    
    //NSLog(@"Cancel Add Shooter");
    
    for(UIView *view in self.addShooterView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addShooterView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.addShooterView resignFirstResponder];
    self.addShooterOutlet.enabled = YES;
     self.navBar.prompt = @"Swipe left to edit shooter";
    self.cancelAddShooterOutlet.enabled = NO;
    [self.tableView setEditing:NO animated:YES];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView moreOptionButtonPressedInRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"EDIT CLICKED");
   // if([self isNetworkAvailable]){
       
        [self editShooter: [self.shootersArray objectAtIndex:indexPath.row] : (int)indexPath.row];
    //}
    //else{
        
      //  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to edit this shooter." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        //[alert show];
        
   // }
    
}


- (NSString *)tableView:(UITableView *)tableView titleForMoreOptionButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return @"Edit";
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    [self.tableView setEditing:NO animated:YES];
    
    if(buttonIndex == 1){
   
    NSString *objectId = self.shooterToDelete[@"appObjectId"];
        [self.shooterToDelete unpin];
        [self.shooterToDelete deleteEventually];
        
        
        
        PFQuery *query = [PFQuery queryWithClassName:@"ShootShooters"];
        [query fromLocalDatastore];
        [query whereKey:@"shooterId" containsString:objectId];
        NSMutableArray *shootShooters = [[query findObjects] mutableCopy];
     
        for(int x = 0; x < shootShooters.count; x++){
        
            [[shootShooters objectAtIndex:x] unpin];
            [[shootShooters objectAtIndex:x] deleteEventually];
        }
        

        PFQuery *shootQuery = [PFQuery queryWithClassName:@"Shoot"];
        [shootQuery whereKey:@"userID" containsString:[[PFUser currentUser] objectId]];
        [shootQuery fromLocalDatastore];
        NSMutableArray *shoots = [[shootQuery findObjects] mutableCopy];
        //Get all shoots to sort through
       
        for(int x = 0; x < shoots.count; x++){
            PFObject *tempStoredShoot = [PFObject objectWithClassName:@"Shoot"];
            tempStoredShoot = [shoots objectAtIndex:x];
            
            PFQuery *shootShooterQuery = [PFQuery queryWithClassName:@"ShootShooters"];
            [shootShooterQuery whereKey:@"shootID" containsString:tempStoredShoot[@"appObjectId"]];
            [shootShooterQuery fromLocalDatastore];
            NSMutableArray *tempShootShooters = [[shootShooterQuery findObjects] mutableCopy];
            NSLog(@"tempShooters: %@", tempShootShooters);
       
            if(tempShootShooters.count == 0){
                
                [tempStoredShoot unpin];
                [tempStoredShoot deleteEventually];
          
            }
        }
    
    
    [self.shootersArray removeObjectAtIndex:self.indexPathToDelete.row];
    [self.tableView reloadData];
}
    
    
}

-(void)editShooter : (PFObject *)shooter : (int) editIndex{
    
    
    for(UIView *view in self.addShooterView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    self.editShooterIndex = editIndex;
    self.editShooter = shooter;
    [self.addShooterView setBackgroundColor:[UIColor lightGrayColor]];
    self.navBar.prompt = nil;
    self.cancelAddShooterOutlet.enabled = YES;
    self.addShooterOutlet.enabled = NO;
    
    UIScrollView *scroll = [[UIScrollView alloc] init];
    scroll.frame = CGRectMake(0, 0, self.addShooterView.frame.size.width, self.addShooterView.frame.size.height - 200);
    [scroll setScrollEnabled:YES];
    [scroll setUserInteractionEnabled:YES];
    scroll.contentSize = self.addShooterView.frame.size;
    scroll.delegate = self;
    
    
    [self.addShooterView addSubview:scroll];
    
    //[scroll addSubview:self.addShooterView];
    
    UIImageView *backgroundImage;
    UIButton *editShooterSave;
    UILabel *shooterInfoLabel;
    UILabel *ATAInformation;
    
    if(self.view.frame.size.width > 320)  //iPAd
    {
        
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
        [backgroundImage setFrame:self.addShooterView.frame];
        [self.addShooterView addSubview:backgroundImage];
        [self.addShooterView sendSubviewToBack:backgroundImage];
        
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 43, 280, 50)];
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 9, 93, 276, 50)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 12, 143, 283, 52)];
        emailAddress = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 193, 280, 50)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 246, 280, 45)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 10, 298, 280, 45)];
        chokeType = [[UISegmentedControl alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3.12, 355, 280, 50)];
        
        ataNumber = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 + 10, 455, 240, 50)];
        handicap = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 + 10, 510, 240, 50)];
        ataClass = [[UITextField alloc] initWithFrame:CGRectMake(235, 565, 150, 50)];
        ataCategory = [[UITextField alloc] initWithFrame:CGRectMake(385, 565, 150, 50)];
        
        editShooterSave = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.5 + 5, 635, 150, 50)];
        shooterInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(310, 11, 200, 25)];
        ATAInformation = [[UILabel alloc] initWithFrame:CGRectMake(300, 425, 200, 25)];
    }
    else{  //Iphone
        
        backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
        [backgroundImage setFrame:CGRectMake(0, 0, 320, self.addShooterView.frame.size.height - 300)];
        [self.addShooterView addSubview:backgroundImage];
        [self.addShooterView sendSubviewToBack:backgroundImage];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(40, 27, 240, 50)];
        
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(42, 79, 236, 50)];
        
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(38, 132, 242, 52)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(40, 187, 240, 50)];
        emergencyContact = [[UITextField alloc] initWithFrame:CGRectMake(7, 247, 150, 50)];
        emergencyContactNum = [[UITextField alloc] initWithFrame:CGRectMake(164, 247, 150, 50)];
        
        ataNumber = [[UITextField alloc] initWithFrame:CGRectMake(40, 405, 240, 50)];
        handicap = [[UITextField alloc] initWithFrame:CGRectMake(40, 460, 240, 50)];
        ataClass = [[UITextField alloc] initWithFrame:CGRectMake(7, 515, 150, 50)];
        ataCategory = [[UITextField alloc] initWithFrame:CGRectMake(164, 515, 150, 50)];
        
        chokeType = [[UISegmentedControl alloc] initWithFrame: CGRectMake(20, 317, 280, 40)];
        editShooterSave = [[UIButton alloc] initWithFrame:CGRectMake(85, 600, 150, 50)];
        shooterInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 4, 200, 25)];
        ATAInformation = [[UILabel alloc] initWithFrame:CGRectMake(15, 375, 200, 25)];
    }
    
    [self.chokeType setBackgroundColor:[UIColor whiteColor]];
    [self.chokeType setTintColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f]];
    [self.chokeType.layer setBorderColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f].CGColor];
    [self.chokeType.layer setCornerRadius:5.0f];
    [self.chokeType.layer setBorderWidth:2.0f];

    
    [shooterInfoLabel setTextAlignment:NSTextAlignmentLeft];
    [shooterInfoLabel setText:@"Shooter Information"];
    [shooterInfoLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [shooterInfoLabel setTextColor:[UIColor colorWithRed:(105/255.f) green:(138/255.f) blue:(51/255.f) alpha:1.0f]];
    
    [ATAInformation setTextAlignment:NSTextAlignmentLeft];
    [ATAInformation setFont:[UIFont boldSystemFontOfSize:16]];
    [ATAInformation setText:@"ATA Information (Optional)"];
    [ATAInformation setTextColor: [UIColor colorWithRed:(105/255.f) green:(138/255.f) blue:(51/255.f) alpha:1.0f]];
    
    
    
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setFont:[UIFont systemFontOfSize:14]];
    [firstName setReturnKeyType:UIReturnKeyNext];
    [firstName setTextAlignment:NSTextAlignmentCenter];
    [firstName setBackground:[UIImage imageNamed:@"iTRAPPfirstName.png"]];
    //[firstName setBackgroundColor:[UIColor clearColor]];
    [firstName setBorderStyle:UITextBorderStyleNone];
    [firstName setPlaceholder:@"First Name"];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackgroundColor:[UIColor clearColor]];
    [lastName setFont:[UIFont systemFontOfSize:14]];
    [lastName setBorderStyle:UITextBorderStyleNone];
    [lastName setPlaceholder:@"Last Name"];
    [lastName setTextAlignment:NSTextAlignmentCenter];
    [lastName setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor clearColor]];
    [phoneNum setBorderStyle:UITextBorderStyleNone];
    [phoneNum setFont:[UIFont systemFontOfSize:14]];
    [phoneNum setPlaceholder:@"   Phone Number (Optional)"];
    [phoneNum setTextAlignment:NSTextAlignmentCenter];
    [phoneNum setBackground:[UIImage imageNamed:@"iTRAPPphone.png"]];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setReturnKeyType:UIReturnKeyNext];
    [emailAddress setBackgroundColor:[UIColor clearColor]];
    [emailAddress setBorderStyle:UITextBorderStyleNone];
    [emailAddress setFont:[UIFont systemFontOfSize:14]];
    [emailAddress setPlaceholder:@"   Email Address (Required)"];
    [emailAddress setTextAlignment:NSTextAlignmentCenter];
    [emailAddress setBackground:[UIImage imageNamed:@"iTRAPPemailAddressBox.png"]];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [emergencyContact setKeyboardType:UIKeyboardTypeDefault];
    [emergencyContact setReturnKeyType:UIReturnKeyNext];
    [emergencyContact setBackgroundColor:[UIColor clearColor]];
    [emergencyContact setBorderStyle:UITextBorderStyleNone];
    [emergencyContact setFont:[UIFont systemFontOfSize:14]];
    [emergencyContact setPlaceholder:@"E-Contact (Optional)"];
    [emergencyContact setTextAlignment:NSTextAlignmentCenter];
    [emergencyContact setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [emergencyContact addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [emergencyContactNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [emergencyContactNum setReturnKeyType:UIReturnKeyNext];
    [emergencyContactNum setBackgroundColor:[UIColor clearColor]];
    [emergencyContactNum setBorderStyle:UITextBorderStyleNone];
    [emergencyContactNum setFont:[UIFont systemFontOfSize:14]];
    [emergencyContactNum setPlaceholder:@"E-Phone # (Optional)"];
    [emergencyContactNum setTextAlignment:NSTextAlignmentCenter];
    [emergencyContactNum setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [emergencyContactNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [ataNumber setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [ataNumber setReturnKeyType:UIReturnKeyNext];
    [ataNumber setBackgroundColor:[UIColor clearColor]];
    [ataNumber setFont:[UIFont systemFontOfSize:14]];
    [ataNumber setBorderStyle:UITextBorderStyleNone];
    [ataNumber setPlaceholder:@"ATA Number"];
    [ataNumber setTextAlignment:NSTextAlignmentCenter];
    [ataNumber setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [ataNumber addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    handicap.inputView = self.pickerView;
    //[handicap setKeyboardType:UIKeyboardTyp];
    handicap.delegate = self;
    [handicap setReturnKeyType:UIReturnKeyNext];
    [handicap setBackgroundColor:[UIColor clearColor]];
    [handicap setFont:[UIFont systemFontOfSize:14]];
    [handicap setBorderStyle:UITextBorderStyleNone];
    [handicap setPlaceholder:@"ATA Handicap Yrds"];
    [handicap setTextAlignment:NSTextAlignmentCenter];
    [handicap setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [handicap addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    ataCategory.inputView = self.pickerView;
    [ataCategory setKeyboardType:UIKeyboardTypeDefault];
    ataCategory.delegate = self;
    [ataCategory setReturnKeyType:UIReturnKeyNext];
    [ataCategory setBackgroundColor:[UIColor clearColor]];
    [ataCategory setFont:[UIFont systemFontOfSize:14]];
    [ataCategory setBorderStyle:UITextBorderStyleNone];
    [ataCategory setPlaceholder:@"ATA Category"];
    [ataCategory setTextAlignment:NSTextAlignmentCenter];
    [ataCategory setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [ataCategory addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    ataClass.delegate = self;
    ataClass.inputView = self.pickerView;
    [ataClass setKeyboardType:UIKeyboardTypeDefault];
    [ataClass setReturnKeyType:UIReturnKeyDone];
    [ataClass setBackgroundColor:[UIColor clearColor]];
    [ataClass setFont:[UIFont systemFontOfSize:14]];
    [ataClass setBorderStyle:UITextBorderStyleNone];
    [ataClass setPlaceholder:@"ATA Class"];
    [ataClass setTextAlignment:NSTextAlignmentCenter];
    [ataClass setBackground:[UIImage imageNamed:@"BlackBoxSmall.png"]];
    [ataClass addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [chokeType insertSegmentWithTitle:@"Full" atIndex:0 animated:NO];
    [chokeType insertSegmentWithTitle:@"Modified" atIndex:1 animated:NO];
    [chokeType insertSegmentWithTitle:@"Improved" atIndex:2 animated:NO];
    [chokeType setSelectedSegmentIndex:1];
    
    
    [editShooterSave setBackgroundImage:[UIImage imageNamed:@"iTRAPPsaveshooter.png"] forState:UIControlStateNormal];
    [editShooterSave addTarget:self action:@selector(editShooterSave) forControlEvents:UIControlEventTouchUpInside];
    [editShooterSave setEnabled:true];
    
    
    self.firstName.text = shooter[@"fName"];
    self.lastName.text = shooter[@"lName"];
    self.phoneNum.text = shooter[@"phone"];
    self.emailAddress.text = shooter[@"email"];
    self.emergencyContact.text = shooter[@"emergencyContact"];
    self.emergencyContactNum.text = shooter[@"emergencyContactNum"];
    self.handicap.text = [NSString stringWithFormat:@"%d", [shooter[@"handicap"] intValue]];
    self.ataNumber.text = shooter[@"ataNumber"];
    self.ataClass.text = shooter[@"class"];
    self.ataCategory.text = shooter[@"category"];
    
    if([shooter[@"choke"] isEqualToString:@"Full"]){
        [self.chokeType setSelectedSegmentIndex:0];
        
    }else if([shooter[@"choke"] isEqualToString:@"Modified"]){
        
        [self.chokeType setSelectedSegmentIndex:1];
    }
    else{
        
        [self.chokeType setSelectedSegmentIndex:2];
    }
    

    
  
    [scroll addSubview:ATAInformation];
    [scroll addSubview:shooterInfoLabel];
    [scroll addSubview:ataNumber];
    [scroll addSubview:ataClass];
    [scroll addSubview:ataCategory];
    [scroll addSubview:handicap];
    [scroll addSubview:emergencyContact];
    [scroll addSubview:emergencyContactNum];
    [scroll addSubview:firstName];
    [scroll addSubview:lastName];
    [scroll addSubview:phoneNum];
    [scroll addSubview:emailAddress];
    
    [scroll addSubview:chokeType];
    [scroll addSubview:editShooterSave];

    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self.container addSubview: self.addShooterView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion:NULL];


}

-(void) editShooterSave {
    //NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
    NSString *objectId = [self.editShooter objectId];
    
    PFQuery *query =[PFQuery queryWithClassName:@"Shooter"];
    [query fromLocalDatastore];
    PFObject *shooter = [PFObject objectWithClassName:@"Shooter"];
    shooter = [query getObjectWithId: objectId];
    //Shooter *shooter = [Shooter MR_findFirstByAttribute:@"objectId" withValue:objectId];
    NSLog(@"Shooter To Edit: %@", shooter);
    
    
    
    if(firstName.text.length < 1 || lastName.text.length < 1 || self.emailAddress.text.length < 4)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
    
    NSString *choke= [self.chokeType titleForSegmentAtIndex:[self.chokeType selectedSegmentIndex]];
    
   // NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", self.firstName.text, self.lastName.text]];
    //shoo = objectId;
    shooter[@"fName"] = self.firstName.text;
    shooter[@"lName"] = self.lastName.text;
    shooter[@"phone"] = self.phoneNum.text;
    shooter[@"email"] = self.emailAddress.text;
    shooter[@"emergencyContact"] = self.emergencyContact.text;
    shooter[@"emergencyContactNum"] = self.emergencyContactNum.text;
    shooter[@"choke"] = choke;
        shooter[@"ataNumber"] = self.ataNumber.text;
        shooter[@"class"] = self.ataClass.text;
        shooter[@"category"] = self.ataCategory.text;
        shooter[@"handicap"] = [NSNumber numberWithInt: [self.handicap.text intValue]];
 
        
        [shooter saveEventually];
  
    
        [self.shootersArray replaceObjectAtIndex:self.editShooterIndex withObject:shooter];
       
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addShooterView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.addShooterView resignFirstResponder];
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:Nil waitUntilDone:NO ];
    self.addShooterOutlet.enabled = YES;
    self.navBar.prompt = @"Swipe left to edit shooter";
    self.cancelAddShooterOutlet.enabled = NO;
    
    
}
}

#pragma mark PICKERVIEW DELEGATE

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
        
        return 1;
}



// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(self.pickerView.tag == 1){
        
            return [self.handicapNumbers count];
    }
    else if(self.pickerView.tag == 2) //Trap house Picker
    {
        
        return [self.ataClassesArray count];
        
    }
    else{
        
        return [self.ataCategoriesArray count];
        
    }

    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(self.pickerView.tag == 1){
        
        self.handicap.text = [NSString stringWithFormat:@"%d", [[handicapNumbers objectAtIndex:row] intValue]];
        
    }
    else if(self.pickerView.tag == 2){
        
        self.ataClass.text = [ataClassesArray objectAtIndex:row];
        
    }
    else{
        
        
        self.ataCategory.text = [ataCategoriesArray objectAtIndex:row];
        
    }

}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    
    return 100;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	UILabel *pickerLabel = (UILabel *)view;
	// Reuse the label if possible, otherwise create and configure a new one
	if ((pickerLabel == nil) || ([pickerLabel class] != [UILabel class]))
    {  //newlabel
		
		pickerLabel = [[UILabel alloc] init];
		
		pickerLabel.backgroundColor = [UIColor clearColor];
        [pickerLabel sizeToFit];
    }
	pickerLabel.textColor = [UIColor blackColor];
    NSString*  text = @"";
    
    if(self.pickerView.tag == 1){ //HandicapNumbers
        
    
            [pickerLabel setFrame: CGRectMake(15.0, 0.0, 46, 32.0)];
            pickerLabel.textAlignment = NSTextAlignmentCenter;
            text = [NSString stringWithFormat:@"%d",[[self.handicapNumbers objectAtIndex:row] intValue]];
        
    }
    else if(self.pickerView.tag == 2){  //ATA Class
        
        text = [self.ataClassesArray objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    else{// ATA Category
        
        text = [self.ataCategoriesArray objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }

    pickerLabel.text = text;
    
	return pickerLabel;
}



#pragma mark TextField Delegate


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if([textField isEqual:self.handicap]){
        
        //NSLog(@"HANDICAP PICKER");
        self.pickerView.tag = 1;
        [self.pickerView reloadAllComponents];
        self.handicap.text = [NSString stringWithFormat:@"%d", [[handicapNumbers objectAtIndex:0] intValue]];
    }
    else if([textField isEqual:self.ataClass]){
    
        //NSLog(@"CLASS PICKER");
        self.pickerView.tag = 2;
        [self.pickerView reloadAllComponents];
        self.ataClass.text = [ataClassesArray objectAtIndex:0];
    }
    else{
        
        //NSLog(@"CATEGORY PICKER");
        self.pickerView.tag = 3;
        [self.pickerView reloadAllComponents];
        self.ataCategory.text = [ataCategoriesArray objectAtIndex:0];
        
    }
}


@end
