//
//  Shooter.h
//  ClayCounter
//
//  Created by Test on 1/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Shooter : NSManagedObject

@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * choke;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * emergencyContact;
@property (nonatomic, retain) NSString * emergencyContactNum;
@property (nonatomic, retain) NSString * fName;
@property (nonatomic, retain) NSString * lName;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString *objectId;
@property (nonatomic, retain) NSString *ataNumber;
@property (nonatomic, retain) NSNumber *handicap;
@property (nonatomic, retain) NSString *ataClass;
@property (nonatomic, retain) NSString *ataCategory;


-(Shooter *)initWithName : (NSString *) firstName : (NSString *) lastName;

@end
