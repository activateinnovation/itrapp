//
//  SignUpView.m
//  ClayCounter
//
//  Created by Test on 1/13/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SignUpView.h"

@interface SignUpView ()

@end

@implementation SignUpView
@synthesize usernameTextField;
@synthesize emailTextField;
@synthesize orgNameTextField;
@synthesize homeTrapRangeTextField;
@synthesize passwordTextField;
@synthesize repeatPasswordTextField;
@synthesize shooterPageView;
@synthesize passwordLabel;
@synthesize teamInfoLabel;
@synthesize headCoachLabel;
@synthesize headCoachFName;
@synthesize headCoachLName;
@synthesize headCoachPhoneNum;
@synthesize counter;
@synthesize NextSelected;
@synthesize tempSignUpData;
@synthesize user;
@synthesize coach;
@synthesize loginView;
@synthesize rightHandedLabel;
@synthesize leftRightSwitch;
@synthesize leftHandedLabel;
@synthesize decriptionTextView;
@synthesize updateData;
@synthesize pickerView;
@synthesize trapFieldsArray;
@synthesize trapFieldId;
@synthesize toolBar;
@synthesize menu;
@synthesize managedObjectContext;
@synthesize checkmarkImageView;
@synthesize spinner;
@synthesize checkmarkImageView2;
@synthesize trapFieldSelected;
@synthesize headCoachEmail;
@synthesize statesArray;


-(void) viewWillAppear:(BOOL)animated{
    
    
    [super viewWillAppear:YES];
    self.trapFieldId = [[NSString alloc] init];
    self.tempSignUpData = [[NSMutableArray alloc] init];
    self.updateData = [[UpdatedData alloc] init];
    self.updateData.delegate = self;
    
    [self.updateData getTrapFieldsNew:YES];
    // self.trapFieldsArray = [[NSMutableArray alloc] init];
    // self.trapFieldSelected = [[TrapField alloc] init];
    // self.trapFieldsArray = [self getTrapFields];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    self.statesArray = [[NSMutableArray alloc] initWithObjects: @"N/A", @"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH", @"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];
    
    
    self.view.layer.contents = (id)[UIImage imageNamed:@"iTrappShooterInfoBackground.jpg"].CGImage;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [tap setNumberOfTouchesRequired:1];
    
    [self.view addGestureRecognizer:tap];
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.shooterPageView =
    [storyboard instantiateViewControllerWithIdentifier:@"Team"];
    
    
    UIStoryboard* storyboard1;
    if(self.view.frame.size.width > 320)
    {
        storyboard1 = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard1 = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.loginView =
    [storyboard1 instantiateViewControllerWithIdentifier:@"LoginView"];
    
    // self.checkmarkImageView = [[UIImageView alloc] init];
    
    
    //NSLog(@"TrapFieldsArray: %@", self.trapFieldsArray);
    
    self.counter = 0;
    
    [self.usernameTextField becomeFirstResponder];
    //self.errorImageView.hidden = YES;
    //self.checkmarkImageView.hidden = YES;
    
    //Show the team information section
    self.teamInfoLabel.hidden = NO;
    self.usernameTextField.hidden = NO;
    self.orgNameTextField.hidden = NO;
    self.homeTrapRangeTextField.hidden = NO;
    self.emailTextField.hidden = NO;
    
    self.headCoachFName.hidden = YES;
    self.headCoachLabel.hidden = YES;
    self.headCoachLName.hidden = YES;
    self.headCoachPhoneNum.hidden = YES;
    self.headCoachEmail.hidden = YES;
    self.rightHandedLabel.hidden = YES;
    self.leftHandedLabel.hidden = YES;
    self.leftRightSwitch.hidden = YES;
    self.decriptionTextView.hidden = YES;
    
    self.passwordLabel.hidden = YES;
    self.passwordTextField.hidden = YES;
    self.repeatPasswordTextField.hidden = YES;
    
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 35, 320, 216)];
    [self.pickerView setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:0.4]];
    self.pickerView.delegate = self;
    self.pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
    
    [self.homeTrapRangeTextField setInputView: self.pickerView];
    [self.pickerView reloadAllComponents];
    
    // self.pickerView.hidden = YES;
    
}

-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewWillDisappear:YES];
    self.counter = 0;
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) gotFields:(NSArray *)fields{
    
    NSString *stateTemp = [self.statesArray objectAtIndex:0];
   // self.trapFieldsArray = [fields mutableCopy];
   
    PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
    [query whereKey:@"State" containsString:stateTemp];
    self.trapFieldsArray = [[query findObjects] mutableCopy];
    
    if(trapFieldsArray.count == 0)
    {
        if([stateTemp isEqualToString:@"N/A"]){
            
            [self.trapFieldsArray addObject:[NSString stringWithFormat:@"No Home Range"]];
        }
        else{
            [self.trapFieldsArray addObject:[NSString stringWithFormat:@"No Listings for %@", stateTemp]];
        }
    }
    [self.pickerView reloadAllComponents];

    
}

- (IBAction)cancelSignUp:(id)sender {
    
    [self presentViewController: self.loginView
                       animated:YES
                     completion:nil];
    
}

-(IBAction)keyboardNext:(id)sender{
    
    if(sender == usernameTextField){
        
        [self checkUsername];
        [usernameTextField resignFirstResponder];
        [emailTextField becomeFirstResponder];
        
    }
    else if (sender == emailTextField){
        [self checkEmail];
        [emailTextField resignFirstResponder];
        [orgNameTextField becomeFirstResponder];
        
    }
    else if(sender == orgNameTextField){
        
        [orgNameTextField resignFirstResponder];
        [homeTrapRangeTextField becomeFirstResponder];
        //[self.pickerView reloadAllComponents];
        //Put in the picker view here when homeTrapField becomes first responer
        
    }
    else if(sender == passwordTextField) {
        
        [passwordTextField resignFirstResponder];
        [repeatPasswordTextField becomeFirstResponder];
        
    }
    else if(sender == headCoachFName){
        
        [headCoachFName resignFirstResponder];
        [headCoachLName becomeFirstResponder];
        
    }
    else if (sender == headCoachLName){
        
        [headCoachLName resignFirstResponder];
        [headCoachPhoneNum becomeFirstResponder];
        
    }
    else if(sender == headCoachPhoneNum){
        [headCoachPhoneNum resignFirstResponder];
        [headCoachEmail becomeFirstResponder];
        
    }
    else{
        
    }
    
    
}

-(void) doneOnPasswordField{
    
    [self initiateSignUpProcess];
    
}


-(IBAction)keyboardDone:(id)sender{
    
    if(sender == homeTrapRangeTextField){
        [homeTrapRangeTextField resignFirstResponder];
    }
    else if (sender == repeatPasswordTextField){
        [repeatPasswordTextField resignFirstResponder];
        [self doneOnPasswordField];
        
    }
    else if(sender == self.headCoachEmail){
        
        [self.headCoachEmail resignFirstResponder];
    }
    else{
        
    }
    
}



-(void) tapGesture {
    
    [self.usernameTextField resignFirstResponder];
    [self.emailTextField resignFirstResponder];
    [self.orgNameTextField resignFirstResponder];
    [self.homeTrapRangeTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    [self.repeatPasswordTextField resignFirstResponder];
    [self.headCoachPhoneNum resignFirstResponder];
    [self.headCoachLName resignFirstResponder];
    [self.headCoachFName resignFirstResponder];
    [self.headCoachEmail resignFirstResponder];
}




- (IBAction)nextSelectedAction:(id)sender {
    
    if(self.counter == 0){
        
        //Set Parse User Data USERNAME, EMAIL, ORGANIZATION, and TRAP FIELD
        if(self.emailTextField.text.length == 0 || self.orgNameTextField.text.length == 0 || self.homeTrapRangeTextField.text.length == 0 || self.usernameTextField.text.length == 0 || self.checkmarkImageView.image == [UIImage imageNamed:@"error.png"] || checkmarkImageView2.image == [UIImage imageNamed:@"error.png"] ){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        else {
            
            
            [self.tempSignUpData addObject:usernameTextField.text];
            [self.tempSignUpData addObject:emailTextField.text];
            [self.tempSignUpData addObject:orgNameTextField.text];
            [self.tempSignUpData addObject: homeTrapRangeTextField.text];
            
            //Show head coach info
            self.teamInfoLabel.hidden = YES;
            self.usernameTextField.hidden = YES;
            self.orgNameTextField.hidden = YES;
            self.homeTrapRangeTextField.hidden = YES;
            self.emailTextField.hidden = YES;
            
            self.headCoachFName.hidden = NO;
            self.headCoachLabel.hidden = NO;
            self.headCoachLName.hidden = NO;
            self.headCoachPhoneNum.hidden = NO;
            self.headCoachEmail.hidden = NO;
            self.rightHandedLabel.hidden = NO;
            self.leftHandedLabel.hidden = NO;
            self.leftRightSwitch.hidden = NO;
            self.decriptionTextView.hidden = NO;
            
            
            self.passwordLabel.hidden = YES;
            self.passwordTextField.hidden = YES;
            self.repeatPasswordTextField.hidden = YES;
            
            self.checkmarkImageView.hidden = YES;
            self.checkmarkImageView2.hidden = YES;
            
            
            [self.homeTrapRangeTextField resignFirstResponder];
            [self.headCoachFName becomeFirstResponder];
            self.counter++;
        }
        
    }
    else if (self.counter == 1){
        
        
        //Add parse dacta to array COACH FNAME, COACH LNAME, COACH Email
        if((self.headCoachFName.text.length == 0) || (self.headCoachLName.text.length == 0) || (self.headCoachEmail.text.length == 0) ){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
        else{
            
            [self.tempSignUpData addObject:headCoachFName.text];
            [self.tempSignUpData addObject:headCoachLName.text];
            [self.tempSignUpData addObject:headCoachPhoneNum.text];
            [self.tempSignUpData addObject:headCoachEmail.text];
            
            if([self.leftRightSwitch isOn]){
                
                NSString *leftRight = @"Right";
                [self.tempSignUpData addObject:leftRight];
                
            }
            else{
                
                NSString *leftRight = @"Left";
                [self.tempSignUpData addObject:leftRight];
            }
            
            //Show the password fields
            
            [self.NextSelected setTitle:@"Done" forState:UIControlStateNormal];
            
            self.teamInfoLabel.hidden = YES;
            self.usernameTextField.hidden = YES;
            self.orgNameTextField.hidden = YES;
            self.homeTrapRangeTextField.hidden = YES;
            self.emailTextField.hidden = YES;
            
            self.headCoachFName.hidden = YES;
            self.headCoachLabel.hidden = YES;
            self.headCoachLName.hidden = YES;
            self.headCoachPhoneNum.hidden = YES;
            self.headCoachEmail.hidden = YES;
            self.rightHandedLabel.hidden = YES;
            self.leftHandedLabel.hidden = YES;
            self.leftRightSwitch.hidden = YES;
            self.decriptionTextView.hidden = YES;
            
            
            self.passwordLabel.hidden = NO;
            self.passwordTextField.hidden = NO;
            self.repeatPasswordTextField.hidden = NO;
            
            [self.headCoachEmail resignFirstResponder];
            [self.passwordTextField becomeFirstResponder];
            self.counter++;
        }
        //NO(false) means LEFT and YES(true) means RIGHT
        
    }
    else if (self.counter == 2){
        
        //Need to check passwords for matching here
        [self initiateSignUpProcess];
    }
}

-(void) initiateSignUpProcess{
    
    if (self.passwordTextField.text.length < 6 || self.repeatPasswordTextField.text.length < 6) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Password needs to be at least 6 characters long." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
    
    }else{
        
        if ([self.passwordTextField.text isEqualToString:self.repeatPasswordTextField.text] ) {
            [spinner startAnimating];
            //Add data to parse array PASSWORD
            [self.tempSignUpData addObject:passwordTextField.text];
            
            // NSLog(@"%@", self.tempSignUpData[7]);
            
            //NSLog(@"TrapField: %@, %@", self.trapFieldSelected.name, self.trapFieldSelected.objectId);
            //Add the user to parse, check internet connect
            user = [PFUser user];
            user.username = self.tempSignUpData[0];
            user.password = self.tempSignUpData[9];
            user.email = self.tempSignUpData[1];
            user[@"Organization"] = self.tempSignUpData[2];
            user[@"SubscriptionId"] = @"8I32FeQiGE";  // This is setting up the subscrition iD for the Trial
            
            if(self.trapFieldSelected == nil)
            {
                
                user[@"homeField"] = @"NA";
                user[@"homeFieldName"] = @"None Selected";
                
            }
            else{
                
                user[@"homeField"] = self.trapFieldSelected.objectId;
                user[@"homeFieldName"] = self.trapFieldSelected[@"Name"];
               
            }
            
            coach = [PFObject objectWithClassName:@"Coach"];
            coach[@"fName"] = self.tempSignUpData[4];
            coach[@"lName"] = self.tempSignUpData[5];
            coach[@"phone"] = self.tempSignUpData[6];
            coach[@"email"] = self.tempSignUpData[7];
            coach[@"leftRightHanded"] = self.tempSignUpData[8];

            
            [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (!error) {
                    
                    
                    [self setUpUser];
                    
                } else {
                 
                }
            }];
            
            
        }
        else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Password Error" message:@"Please make sure both passwords match." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
            
        }
    }
    
    
}






-(void) setUpUser{
    
   // self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
    
    NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", [self.tempSignUpData objectAtIndex:4], [self.tempSignUpData objectAtIndex:5]]];
    
    [PFUser logInWithUsernameInBackground: self.usernameTextField.text password: self.passwordTextField.text block:^(PFUser *userTemp, NSError *error) {
        if (userTemp) {
            
            
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"stayLoggedIn"];
            
            PFQuery *query = [PFQuery queryWithClassName:@"Subscription"];
            [query whereKey:@"objectId" equalTo:[PFUser currentUser][@"SubscriptionId"]];
            PFObject *subscription = [[PFObject alloc] initWithClassName:@"Subscription"];
            subscription = [query getFirstObject];
            if(subscription)
            {
                [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithInt: [subscription[@"shooterCap"] intValue]] forKey:@"shooterCap"];
            }

            userTemp = [PFUser currentUser];
            coach[@"userId"] = userTemp.objectId;
            coach[@"appObjectId"] = tempHashId;
            [coach pinInBackground];
            [coach saveInBackground];
            
         
            [self.spinner stopAnimating];
            [self presentWelcomeAlert];
            [self presentViewController:shooterPageView animated:YES completion:nil];
            
            
        } else {
            
            
        }
    }];
}



-(NSDate *) checkDate14DaysFromSignup : (NSDate *)startDate {
 
    NSDate *now = startDate;
    int daysToAdd = 14;  // or 60 :-)
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDate *newDate = [gregorian dateByAddingComponents:components toDate:now options:0];
    //NSLog(@"Start: %@, 14 days out: %@", startDate ,newDate);
    
    return newDate;
    
    
}


-(NSString*) getDate : (NSDate *)dateToConvert {
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    dateString = [formatter stringFromDate:dateToConvert];
    
    return dateString;
}

-(void) presentWelcomeAlert{
    
    NSDate *endDate = [self checkDate14DaysFromSignup:[PFUser currentUser].createdAt];
    NSString *expireString = [self getDate:endDate];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Welcome to iTrapp" message:[NSString stringWithFormat:@"Thank you for choosing iTrapp %@! By signing up you have started a 14 day unlimited free trial that lasts until %@. After this time you will be directed to our website to manage your account settings, but you will not automatically be charged at the end of the trial. Simply visit the \"Team\" page and start adding your shootrs today! Please enjoy iTrapp and visit our website at http://itrappshoot.com to provide us with any feedback you may have.", [PFUser currentUser][@"username"], expireString] delegate:self cancelButtonTitle:@"BEGIN" otherButtonTitles:nil, nil];
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
    
}

-(void) checkEmail {
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"email" equalTo:self.emailTextField.text]; // find all the women
    NSMutableArray *users = [[query findObjects] mutableCopy];
    
    // NSLog(@"%d", users.count);
    
    if (self.emailTextField.text.length == 0 ) {
        
        [self.checkmarkImageView2 setImage:[UIImage imageNamed:@"error.png"]];
        
    }
    else{
        if(users.count <= 0){
            
            [self.checkmarkImageView2 setImage:[UIImage imageNamed:@"checkmark.png"]];
        }
        else{
            self.emailTextField.text = @"";
            self.emailTextField.placeholder = @"Email already in use";
            [self.checkmarkImageView2 setImage:[UIImage imageNamed:@"error.png"]];
        }
    }
}


-(void) checkUsername {
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:self.usernameTextField.text]; // find all the women
    NSMutableArray *users = [[query findObjects] mutableCopy];
    
    // NSLog(@"%d", users.count);
    
    if (self.usernameTextField.text.length == 0 ) {
        
        [self.checkmarkImageView setImage:[UIImage imageNamed:@"error.png"]];
        
    }
    else{
        if(users.count <= 0){
            
            [self.checkmarkImageView setImage:[UIImage imageNamed:@"checkmark.png"]];
        }
        else{
            self.usernameTextField.text = @"";
            self.usernameTextField.placeholder = @"Username already taken";
            [self.checkmarkImageView setImage:[UIImage imageNamed:@"error.png"]];
        }
        
    }
    
}

#pragma mark PICKERVIEW DELEGATE

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 2;
}

// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0){
        return [self.statesArray count];
    }
    else{
        return [self.trapFieldsArray count];
    }
}


- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	UILabel *pickerLabel = (UILabel *)view;
	// Reuse the label if possible, otherwise create and configure a new one
	if ((pickerLabel == nil) || ([pickerLabel class] != [UILabel class]))
    {  //newlabel
		
		pickerLabel = [[UILabel alloc] init];
		
		pickerLabel.backgroundColor = [UIColor clearColor];
        [pickerLabel sizeToFit];
    }
	pickerLabel.textColor = [UIColor blackColor];
    NSString*  text = @"";
    
    if (component == 0)
    {
        [pickerLabel setFrame: CGRectMake(15.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        text = [self.statesArray objectAtIndex:row];
    }
    else
    {
        [pickerLabel setFrame: CGRectMake(50.0, 0.0, 270, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        if([[self.trapFieldsArray objectAtIndex:row] isKindOfClass:[PFObject class]] )
        {
            PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
            temp = [self.trapFieldsArray objectAtIndex:row];
            text = temp[@"Name"];
        }
        else{
            
            
            text = [self.trapFieldsArray objectAtIndex:0];
        }
        
        
    }
    
    
    
    pickerLabel.text = text;
    
	return pickerLabel;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    if(component == 0){
        
        //Need to check size and add an object that says there are no fields here so it doesnt crash
        NSString *stateTemp = [self.statesArray objectAtIndex:row];
        //self.stateSelected = row;
        [self.trapFieldsArray removeAllObjects];
        PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
        [query whereKey:@"State" containsString:stateTemp];
        [query orderByAscending:@"Name"];
        self.trapFieldsArray = [[query findObjects] mutableCopy];
       // self.trapFieldsArray = [[TrapField MR_findByAttribute:@"State" withValue:stateTemp andOrderBy:@"Name" ascending:YES] mutableCopy];
        if(self.trapFieldsArray.count == 0)
        {
            //[self.trapRanges removeAllObjects];
            if([stateTemp isEqualToString:@"N/A"]){
            
                [self.trapFieldsArray addObject:[NSString stringWithFormat:@"No Home Range"]];
                self.homeTrapRangeTextField.text = @"None Selected";
                self.trapFieldSelected = nil;
                [self.pickerView reloadAllComponents];
            }
            else{
                [self.trapFieldsArray addObject:[NSString stringWithFormat:@"No Listings for %@", stateTemp]];
                self.homeTrapRangeTextField.text = @"None Selected";
                self.trapFieldSelected = nil;
                [self.pickerView reloadAllComponents];
            }
        }
        else{
            PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
            temp = [self.trapFieldsArray objectAtIndex:0];
            self.homeTrapRangeTextField.text = temp[@"Name"];
            [self.pickerView reloadAllComponents];
        }
        
    }
    else{
        
        if([[self.trapFieldsArray objectAtIndex:row] isKindOfClass:[PFObject class]] )
        {
            PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
            temp = [self.trapFieldsArray objectAtIndex:row];
            self.homeTrapRangeTextField.text = temp[@"Name"];
            [self.pickerView reloadAllComponents];
            self.trapFieldSelected = temp;
        }
        else{
            
            self.homeTrapRangeTextField.text = @"None Selected";
            self.trapFieldSelected = nil;
        }
        
        
    }
    
}






- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    
    if(component == 0){
        
        return 60;
    }
    else{
        
        return 260;
        
    }
    
}

- (IBAction)usernameCheckDidEnd:(id)sender {
    
    
    [self checkUsername];
}
- (IBAction)checkEmailDone:(id)sender {
    
    [self checkEmail];
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}

- (IBAction)trapFieldBeginEditing:(id)sender {
    
    if([[self.trapFieldsArray objectAtIndex:0] isKindOfClass:[PFObject class]] )
    {
        //NSLog(@"HERE1");
        PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
        temp = [self.trapFieldsArray objectAtIndex:0];
        self.homeTrapRangeTextField.text = temp[@"Name"];
        self.trapFieldSelected = temp;
    }
    else{
        
        //NSLog(@"HERE2");
        self.homeTrapRangeTextField.text = @"None Selected";
        self.trapFieldSelected = nil;
    }

    
    
    
}
@end
