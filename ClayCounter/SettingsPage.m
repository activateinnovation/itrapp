//
//  SettingsPage.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SettingsPage.h"

@interface SettingsPage ()

@end

@implementation SettingsPage

@synthesize scoringPage;
@synthesize shootersPage;
@synthesize userDefaults;
@synthesize updatedData;
@synthesize container;
@synthesize addCoachView;
@synthesize firstName;
@synthesize lastName;
@synthesize phoneNum;
@synthesize emailAddress;
@synthesize managedObjectContext;
@synthesize leftRightSwitch;
@synthesize spinner;
@synthesize cancelButtonOutlet;
@synthesize coachToEdit;
@synthesize accountTypeLabel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.cancelButtonOutlet.enabled = NO;
    self.coachesDataArray = [[NSMutableArray alloc] init];
   self.updatedData = [[UpdatedData alloc] init];
    self.updatedData.delegate = self;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [refreshControl setTintColor:[UIColor colorWithRed:(91/255.f) green:(192/255.f) blue:(232/255.f) alpha:1.0f]];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
    
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height * 0.75)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    
    [spinner startAnimating];

    //[updatedData getCoaches];
   
   // self.coachesDataArray = [[Coach MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName" ascending:YES] mutableCopy];
 
    
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
    
    if(self.view.frame.size.width > 320)
    {
        [background setFrame: CGRectMake(0, 50, 780, 900)];
    }
    else
    {
        [background setFrame: CGRectMake(0, 50, 320, 510)];
    }
    [background setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];

    
    
    
    if(self.view.frame.size.width > 320){
        
        self.leftRightSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.15, 250, 40, 35)];
    }else{
        self.leftRightSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(135, 250, 40, 35)];
    }
    //This fills all of the labels in the settings page with user data
    self.container = [[UIView alloc] initWithFrame:CGRectMake(0, 60, self.view.frame.size.width, self.view.frame.size.height)];
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];
    self.addCoachView = [[UIView alloc] initWithFrame:self.container.bounds];
    self.addCoachView.clearsContextBeforeDrawing = YES;
    
    self.editCoachView = [[UIView alloc] initWithFrame:self.container.bounds];
    self.editCoachView.clearsContextBeforeDrawing = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGesture)];
    [tap setNumberOfTouchesRequired:1];
    
    [self.addCoachView addGestureRecognizer:tap];
    [self.editCoachView addGestureRecognizer:tap];
    
   // self.coachToEdit = [[Coach alloc] init];
    
    [self setUpView];
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
  self.cancelButtonOutlet.enabled = NO;
    
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.tabBar.delegate = self;
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.shootersPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Team"];
    
    
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
    

    self.loginView =
    [storyboard instantiateViewControllerWithIdentifier:@"LoginView"];
    
    [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex: 2]];
	// Do any additional setup after loading the view.
   
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) gotCoaches:(NSArray *)array{
    
    [self.coachesDataArray removeAllObjects];
    self.coachesDataArray = [array mutableCopy];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
        [spinner stopAnimating];
    });
}

- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    double delayInSeconds = 1.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    [self.updatedData getCoachesNew:NO];
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshControl endRefreshing];
    });
}


-(void) setUpView{
    
    [self.updatedData getCoachesNew:NO];
    
    self.usernameLabel.text = [PFUser currentUser][@"username"];
    if([[PFUser currentUser][@"Organization"] length] > 0){
        self.organizationLabel.text = [PFUser currentUser][@"Organization"];
    }
    else{
        self.organizationLabel.text = @"None";
    }
    self.trapFieldLabel.text = [PFUser currentUser][@"homeFieldName"];
    PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
    [query fromLocalDatastore];
    [query whereKey:@"userId" containsString:[PFUser currentUser].objectId];
    self.numOfShootersLabel.text = [NSString stringWithFormat:@"%lu Team Shooters", (unsigned long)[[query findObjects] count]];
   // Subscription *subscription = [Subscription MR_findFirstByAttribute:@"objectId" withValue:[PFUser currentUser][@"subscriptionId"]];
    PFQuery *subQuery = [PFQuery queryWithClassName:@"Subscription"];
    [subQuery whereKey:@"objectId" containsString:[PFUser currentUser][@"SubscriptionId"]];
    [subQuery fromLocalDatastore];
    PFObject *subscription = [PFObject objectWithClassName:@"Subscription"];
    subscription = [subQuery getFirstObject];
 
    self.accountTypeLabel.text = [NSString stringWithFormat:@"%@ - Max of %d Shooters", subscription[@"name"], [subscription[@"shooterCap"] intValue]];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.coachesDataArray count];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
   // UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    MSCMoreOptionTableViewCell *cell = (MSCMoreOptionTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:nil];

    
    if (cell == nil) {
        
       // cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell = [[MSCMoreOptionTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.delegate = self;
    }
    
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
 
    
    PFObject *tempCoach = [PFObject objectWithClassName:@"Coach"];
    tempCoach = [self.coachesDataArray objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", tempCoach[@"fName"], tempCoach[@"lName"]];
   
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iTrappLittleBlueRect.png"]];
    [background setFrame:CGRectMake(0, 0, 265, 44)];
    [background setContentMode:UIViewContentModeScaleAspectFill];
    
    [cell addSubview:background];
    [cell sendSubviewToBack:background];
    
    UILabel *leftOrRight = [[UILabel alloc] initWithFrame:CGRectMake(167, 10, 95, 25)];
    [cell addSubview:leftOrRight];
    [leftOrRight setText:[NSString stringWithFormat:@"%@ Handed", tempCoach[@"leftRightHanded"]]];
    [leftOrRight setFont:[UIFont systemFontOfSize:14]];
    [leftOrRight setTextColor:[UIColor darkGrayColor]];
    
    return cell;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    
    if(item.tag == 1){
        
        [self presentViewController:shootersPage animated:YES completion:nil];
        
    }
    else if(item.tag == 2){
        
         [self presentViewController:scoringPage animated:YES completion:nil];
        
    }
    else{
        
        
        
    }
    
}


-(void) setUpAddCoachView{
    
   // [self.addCoachView setBackgroundColor:[UIColor clearColor]];
  //self.addCoachView.layer.contents = (id)[UIImage imageNamed:@"SmallerBackground.jpg"].CGImage;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"SmallerBackground.jpg"]];
    [imageView setFrame:CGRectMake(0, 0, self.addCoachView.frame.size.width, self.addCoachView.frame.size.height)];
    [self.addCoachView addSubview:imageView];
    [self.addCoachView sendSubviewToBack:imageView];
    
    [self.leftRightSwitch setOn:YES];
    [self.leftRightSwitch setOnTintColor:[UIColor colorWithRed:(87/255.f) green:(196/255.f) blue:(234/255.f) alpha:1.0]];
    
     self.cancelButtonOutlet.enabled = YES;
    
    UITextView *infoText;
    UILabel *leftHanded;
    UILabel *rightHanded;
    UIButton *addCoach;
    if(self.view.frame.size.width > 320){
        
         infoText = [[UITextView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.7, 280, 200, 50)];
        leftHanded = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3.15, 254, 100, 25)];
        rightHanded = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/1.8, 254, 110, 25)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 20, 280, 50)];
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 75, 280, 50)];
        phoneNum= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 130, 280, 50)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 185, 280, 50)];
        addCoach = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4, 360, 120, 50)];

    }
    else{
        
         infoText = [[UITextView alloc] initWithFrame:CGRectMake(60, 280, 200, 50)];
        leftHanded = [[UILabel alloc] initWithFrame:CGRectMake(30, 254, 100, 25)];
        rightHanded = [[UILabel alloc] initWithFrame:CGRectMake(200, 254, 110, 25)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(10, 20, 300, 50)];
         lastName = [[UITextField alloc] initWithFrame:CGRectMake(12, 75, 296, 50)];
        phoneNum= [[UITextField alloc] initWithFrame:CGRectMake(10, 130, 300, 50)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(12, 185, 296, 50)];
        addCoach = [[UIButton alloc] initWithFrame:CGRectMake(85, 360, 150, 50)];
        
        
        
    }
   
    infoText.backgroundColor = [UIColor clearColor];
    infoText.text = @"This information is used to adjust the app to best fit you!";
    [infoText setFont:[UIFont systemFontOfSize:14]];
    [infoText setTextAlignment:NSTextAlignmentCenter];
    self.logOutOutlet.enabled = NO;
    //self.editAccountOutlet.enabled = NO;
    self.navBar.topItem.title = @"Add Coach";
    // [self.addShooterView setAlpha:0.9];
    
    leftHanded.text = @"Left Handed";
    [leftHanded setFont:[UIFont boldSystemFontOfSize:16]];
    
   
    rightHanded.text = @"Right Handed";
    [rightHanded setFont:[UIFont boldSystemFontOfSize:16]];
    
    
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setReturnKeyType:UIReturnKeyNext];
    //[firstName setBackgroundColor:[UIColor clearColor]];
    //[firstName setBorderStyle:UITextBorderStyleRoundedRect];
    [firstName setPlaceholder:@"First Name"];
    [firstName setTextAlignment:NSTextAlignmentCenter];
    [firstName setBackground:[UIImage imageNamed:@"iTRAPPfirstName.png"]];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    //[lastName setBackgroundColor:[UIColor whiteColor]];
    //[lastName setBorderStyle:UITextBorderStyleRoundedRect];
    [lastName setTextAlignment:NSTextAlignmentCenter];
    [lastName setPlaceholder:@"Last Name"];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
   
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
   // [phoneNum setBackgroundColor:[UIColor whiteColor]];
   // [phoneNum setBorderStyle:UITextBorderStyleRoundedRect];
    [phoneNum setTextAlignment:NSTextAlignmentCenter];
    [phoneNum setBackground:[UIImage imageNamed:@"iTRAPPphone.png"]];
    [phoneNum setPlaceholder:@"Phone # (Optional)"];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
   
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [emailAddress setReturnKeyType:UIReturnKeyDone];
   // [emailAddress setBackgroundColor:[UIColor whiteColor]];
  //  [emailAddress setBorderStyle:UITextBorderStyleRoundedRect];
    [emailAddress setBackground:[UIImage imageNamed:@"iTRAPPemailAddressBox.png"]];
    [emailAddress setTextAlignment:NSTextAlignmentCenter];
    [emailAddress setPlaceholder:@"Email (Required)"];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    

 
    
    
    [addCoach setImage:[UIImage imageNamed:@"iTRAPPsaveCoach.png"] forState:UIControlStateNormal];
    //[addCoach setTitleColor:[UIColor colorWithRed:(226/255.f) green:(58/255.f) blue:(50/255.f) alpha:1.0] forState:UIControlStateNormal];
    //[addCoach.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    //[addCoach.titleLabel sizeToFit];
    [addCoach addTarget:self action:@selector(addCoachToData) forControlEvents:UIControlEventTouchUpInside];
    [addCoach setEnabled:true];
    
    /*UIButton *cancelAddCoach = [[UIButton alloc] initWithFrame:CGRectMake(30, 400, 100, 50)];
    [cancelAddCoach setTitle:@"Cancel" forState:UIControlStateNormal];
    [cancelAddCoach setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [cancelAddCoach.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    [cancelAddCoach.titleLabel sizeToFit];
    [cancelAddCoach addTarget:self action:@selector(cancelAddCoach) forControlEvents:UIControlEventTouchUpInside];
    [cancelAddCoach setEnabled:true];*/
    
    

    [self.addCoachView addSubview:infoText];
    [self.addCoachView addSubview:firstName];
    [self.addCoachView addSubview:lastName];
    [self.addCoachView addSubview:phoneNum];
    [self.addCoachView addSubview:emailAddress];
    [self.addCoachView addSubview:addCoach];
    //[self.addCoachView addSubview:cancelAddCoach];
    [self.addCoachView addSubview:leftRightSwitch];
    [self.addCoachView addSubview:leftHanded];
    [self.addCoachView addSubview:rightHanded];
    
}

-(void) nextOnKeyboard : (id) sender {
    
    if(sender == self.firstName)
    {
        [self.firstName resignFirstResponder];
        [self.lastName becomeFirstResponder];
    }
    else if(sender == self.lastName){
        
        [self.lastName resignFirstResponder];
        [self.phoneNum becomeFirstResponder];
    }
    else if (sender == self.phoneNum){
        
        [self.phoneNum resignFirstResponder];
        [self.emailAddress becomeFirstResponder];
    }
    else if(sender == self.emailAddress){
        [self.emailAddress resignFirstResponder];
        
    }

    
}


-(void) addCoachToData {
   // //NSLog(@"Add Coach");
    
    
    if(self.firstName.text.length < 2 || self.lastName.text.length < 2 || self.emailAddress.text.length < 4){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled in." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
        NSString *tempHashId = [self sha1:[NSString stringWithFormat:@"%@%@", self.firstName.text, self.lastName.text]];
        NSString *leftRight = [[NSString alloc] init];
        if([self.leftRightSwitch isOn]){
            leftRight = @"Right";
            
        }else{
            
            leftRight = @"Left";
        }
        
        /*   self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
         
         Coach *coach = [Coach MR_createInContext:self.managedObjectContext];
         coach.fName = self.firstName.text;
         coach.lName = self.lastName.text;
         coach.phone = self.phoneNum.text;
         coach.email = self.emailAddress.text;
         coach.leftRightHandefd = leftRight;
         coach.userId = [PFUser currentUser].objectId;
         //int x = [Shooter MR_countOfEntities];
         coach.objectId = tempHashId;//[NSNumber numberWithInt:(x + 1)];
         
         [self.managedObjectContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         if (success) {*/
        
        PFObject *coachObject = [PFObject objectWithClassName:@"Coach"];
        coachObject[@"appObjectId"] = tempHashId;
        coachObject[@"fName"] = self.firstName.text;
        coachObject[@"lName"] = self.lastName.text;
        coachObject[@"phone"] = self.phoneNum.text;
        coachObject[@"leftRightHanded"] = leftRight;
        coachObject[@"email"] = self.emailAddress.text;
        coachObject[@"userId"] = [PFUser currentUser].objectId;
        
        [coachObject pin];
        [coachObject saveEventually];
        
        
    //}];
    
    [self.coachesDataArray removeAllObjects];
    
    //self.coachesDataArray = [[Coach MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName" ascending:YES] mutableCopy];
      [self.updatedData getCoachesNew:NO];
    
    //[self.tableView reloadData];
    
    for(UIView *view in self.addCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addCoachView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.addCoachView removeFromSuperview];
    self.logOutOutlet.enabled = YES;
   // self.editAccountOutlet.enabled = YES;
    self.navBar.topItem.title = @"Settings";
        
       // [self.tableView reloadData];
    }//else
    
    
    
    
}

//called on add coachview to dismiss keybord from all view
-(void) tapGesture{
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    
}

- (IBAction)logOut:(id)sender {
    
    [PFUser logOut];
    
    [userDefaults setBool:NO forKey:@"stayLoggedIn"];
    [userDefaults setObject:@"" forKey:@"name"];
    [userDefaults setObject:[NSNumber numberWithInt:0] forKey:@"shooterCap"];
    [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
    
    dispatch_async(dispatch_get_main_queue(), ^{
         [self presentViewController: self.loginView animated:YES completion:nil];
    });
   
}

- (IBAction)addCoach:(id)sender {
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpAddCoachView];
                        [self.container addSubview: self.addCoachView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion:NULL];
    
}


-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

- (IBAction)cancelAddCoach:(id)sender {
    
   // //NSLog(@"Cancel Add Coach");
    
    for(UIView *view in self.addCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.addCoachView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.addCoachView removeFromSuperview];
    self.logOutOutlet.enabled = YES;
    self.cancelButtonOutlet.enabled = NO;
    self.navBar.topItem.title = @"Settings";
    
    
}



- (NSString *)tableView:(UITableView *)tableView titleForMoreOptionButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return @"Edit";
    
}

- (void)tableView:(UITableView *)tableView moreOptionButtonPressedInRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //NSLog(@"EDIT CLICKED");
    if([self isNetworkAvailable]){
        
        [self editCoach: [self.coachesDataArray objectAtIndex:indexPath.row] : (int)indexPath.row];
        [self.tableView setEditing:NO animated:YES];
         self.navBar.topItem.title = @"Edit Coach";
        

        //[self editShooter: [self.shootersArray objectAtIndex:indexPath.row]];
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to edit this coach." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alert show];
        
    }
    
}


//THIS SECTION OF CODE SETS UP THE NECESSARY METHODS FOR DELETEING A COACH

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
            
            self.coachToDelete = [self.coachesDataArray objectAtIndex:indexPath.row];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Coach" message:@"This will delete all of the data for this shoot. Are you sure you want to delete it?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"DELETE", nil];
            [alert show];
        alert.tag = 1;
        
    }
}


-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        //NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        //NSLog(@"-> connection established!\n");
        return YES;
    }
}



//ALERTVIEW DELEGATE

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    [self.tableView setEditing:NO animated:YES];
    
    if(alertView.tag == 1){  //DELETE A COACH
        
        if(buttonIndex == 0){
            
            //CANCEL
        }
        else {
            
            [self.coachToDelete unpin];
            [self.coachToDelete deleteEventually];
            
            [self.coachesDataArray removeObjectAtIndex:self.deleteIndex];
            [self.tableView reloadData];
        
        }
    }
    
}


- (void)editCoach: (PFObject *)coach : (int) indexToEdit {
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpEditCoachView: coach : (int)indexToEdit];
                        [self.container addSubview: self.editCoachView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion:NULL];
    
}

-(void) setUpEditCoachView: (PFObject *)coach : (int)indexToEdit{
    
    self.editIndex = indexToEdit;
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed: @"SmallerBackground.jpg"]];
    [imageView setFrame:CGRectMake(0, 0, self.addCoachView.frame.size.width, self.addCoachView.frame.size.height)];
    [self.editCoachView addSubview:imageView];
    [self.editCoachView sendSubviewToBack:imageView];
    
    [self.leftRightSwitch setOn:YES];
    [self.leftRightSwitch setOnTintColor:[UIColor colorWithRed:(87/255.f) green:(196/255.f) blue:(234/255.f) alpha:1.0]];
    
    self.cancelButtonOutlet.enabled = YES;
    
    UITextView *infoText;
    UILabel *leftHanded;
    UILabel *rightHanded;
    UIButton *saveCoach;
    if(self.view.frame.size.width > 320){
        
        infoText = [[UITextView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.7, 280, 200, 50)];
        leftHanded = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3.15, 254, 100, 25)];
        rightHanded = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/1.8, 254, 110, 25)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 20, 280, 50)];
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 75, 280, 50)];
        phoneNum= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 130, 280, 50)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3, 185, 280, 50)];
        saveCoach = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4, 360, 120, 50)];
        
    }
    else{
        
        infoText = [[UITextView alloc] initWithFrame:CGRectMake(60, 280, 200, 50)];
        leftHanded = [[UILabel alloc] initWithFrame:CGRectMake(30, 254, 100, 25)];
        rightHanded = [[UILabel alloc] initWithFrame:CGRectMake(200, 254, 110, 25)];
        firstName = [[UITextField alloc] initWithFrame:CGRectMake(10, 20, 300, 50)];
        lastName = [[UITextField alloc] initWithFrame:CGRectMake(12, 75, 296, 50)];
        phoneNum= [[UITextField alloc] initWithFrame:CGRectMake(10, 130, 300, 50)];
        emailAddress= [[UITextField alloc] initWithFrame:CGRectMake(12, 185, 296, 50)];
        saveCoach = [[UIButton alloc] initWithFrame:CGRectMake(85, 360, 150, 50)];
        
        
        
    }
    
    infoText.backgroundColor = [UIColor clearColor];
    infoText.text = @"This information is used to adjust the app to best fit you!";
    [infoText setFont:[UIFont systemFontOfSize:14]];
    [infoText setTextAlignment:NSTextAlignmentCenter];
    self.logOutOutlet.enabled = NO;
    //self.editAccountOutlet.enabled = NO;
    self.navBar.topItem.title = @"Add Coach";
    // [self.addShooterView setAlpha:0.9];
    
    leftHanded.text = @"Left Handed";
    [leftHanded setFont:[UIFont boldSystemFontOfSize:16]];
    
    
    rightHanded.text = @"Right Handed";
    [rightHanded setFont:[UIFont boldSystemFontOfSize:16]];
    
    
    [firstName setKeyboardType:UIKeyboardTypeDefault];
    [firstName setReturnKeyType:UIReturnKeyNext];
    //[firstName setBackgroundColor:[UIColor clearColor]];
    //[firstName setBorderStyle:UITextBorderStyleRoundedRect];
    [firstName setPlaceholder:@"First Name"];
    [firstName setTextAlignment:NSTextAlignmentCenter];
    [firstName setBackground:[UIImage imageNamed:@"iTRAPPfirstName.png"]];
    [firstName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [lastName setKeyboardType:UIKeyboardTypeDefault];
    [lastName setReturnKeyType:UIReturnKeyNext];
    [lastName setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    //[lastName setBackgroundColor:[UIColor whiteColor]];
    //[lastName setBorderStyle:UITextBorderStyleRoundedRect];
    [lastName setTextAlignment:NSTextAlignmentCenter];
    [lastName setPlaceholder:@"Last Name"];
    [lastName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    // [phoneNum setBackgroundColor:[UIColor whiteColor]];
    // [phoneNum setBorderStyle:UITextBorderStyleRoundedRect];
    [phoneNum setTextAlignment:NSTextAlignmentCenter];
    [phoneNum setBackground:[UIImage imageNamed:@"iTRAPPphone.png"]];
    [phoneNum setPlaceholder:@"Phone # (Optional)"];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    
    [emailAddress setKeyboardType:UIKeyboardTypeEmailAddress];
    [emailAddress setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [emailAddress setReturnKeyType:UIReturnKeyDone];
    // [emailAddress setBackgroundColor:[UIColor whiteColor]];
    //  [emailAddress setBorderStyle:UITextBorderStyleRoundedRect];
    [emailAddress setBackground:[UIImage imageNamed:@"iTRAPPemailAddressBox.png"]];
    [emailAddress setTextAlignment:NSTextAlignmentCenter];
    [emailAddress setPlaceholder:@"Email (Required)"];
    [emailAddress addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];

    
    [saveCoach setImage:[UIImage imageNamed:@"iTRAPPsaveCoach.png"] forState:UIControlStateNormal];
    //[addCoach setTitleColor:[UIColor colorWithRed:(226/255.f) green:(58/255.f) blue:(50/255.f) alpha:1.0] forState:UIControlStateNormal];
    //[addCoach.titleLabel setFont:[UIFont boldSystemFontOfSize:20]];
    //[addCoach.titleLabel sizeToFit];
    [saveCoach addTarget:self action:@selector(updateCoachToData) forControlEvents:UIControlEventTouchUpInside];
    [saveCoach setEnabled:true];

    
    self.firstName.text = coach[@"fName"];
    self.lastName.text = coach[@"lName"];
    self.phoneNum.text = coach[@"phone"];
    self.emailAddress.text = coach[@"email"];
    
    if([coach[@"leftRightHanded"] isEqualToString:@"Right"]){
    
        [self.leftRightSwitch setOn:YES];
    }
    else{
        
        [self.leftRightSwitch setOn:NO];
        
    }
    
 
    self.coachToEdit = coach;
    
    [self.editCoachView addSubview:infoText];
    [self.editCoachView addSubview:firstName];
    [self.editCoachView addSubview:lastName];
    [self.editCoachView addSubview:phoneNum];
    [self.editCoachView addSubview:emailAddress];
    [self.editCoachView addSubview:saveCoach];
    //[self.addCoachView addSubview:cancelAddCoach];
    [self.editCoachView addSubview:leftRightSwitch];
    [self.editCoachView addSubview:leftHanded];
    [self.editCoachView addSubview:rightHanded];
    
}

-(void) updateCoachToData {
    
    if(firstName.text.length <= 1 || lastName.text.length <= 1 || self.emailAddress.text.length <= 4)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure all of the required fields are filled." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else{
       
        self.coachToEdit[@"fName"] = self.firstName.text;
        self.coachToEdit[@"lName"] = self.lastName.text;
        self.coachToEdit[@"phone"] = self.phoneNum.text;
        self.coachToEdit[@"email"] = self.emailAddress.text;
        if(self.leftRightSwitch.isOn){
            
            self.coachToEdit[@"leftRightHanded"] = @"Right";
        }
        else{
            
            self.coachToEdit[@"leftRightHanded"] = @"Left";
            
        }
        
        [self.coachToEdit pin];
        [self.coachToEdit saveEventually];
        
        
        [self.coachesDataArray replaceObjectAtIndex:self.editIndex withObject:self.coachToEdit];
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView reloadData];
            [self savedEditCoachDismiss];
        });
    }
    
}



-(void) savedEditCoachDismiss{
    
    for(UIView *view in self.editCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [self.firstName resignFirstResponder];
    [self.lastName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.emailAddress resignFirstResponder];
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.view sendSubviewToBack:self.editCoachView];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    [self.editCoachView removeFromSuperview];
    self.logOutOutlet.enabled = YES;
    self.cancelButtonOutlet.enabled = NO;
    self.navBar.topItem.title = @"Settings";
    
}


@end
