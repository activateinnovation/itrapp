//
//  LoginView.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "LoginView.h"

@interface LoginView ()

@end

@implementation LoginView

@synthesize usernameTextField;
@synthesize passwordTextField;
@synthesize shooterPage;
@synthesize spinner;
@synthesize signUp;
@synthesize userDefaults;
@synthesize updatedData;
@synthesize scoringPage;
@synthesize counter;
@synthesize userNameImage;
@synthesize passwordImage;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


//Sets up the loading spinner and centers it on the page.
-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    self.stayLoggedInSwitch.on = YES;
    self.updatedData = [[UpdatedData alloc] init];
    userDefaults = [NSUserDefaults standardUserDefaults];
    
    self.counter = 0;
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    self.view.layer.contents = (id)[UIImage imageNamed:@"iTrappShooterInfoBackground.jpg"].CGImage;
    
    [self.view sendSubviewToBack:self.userNameImage];
    [self.view sendSubviewToBack:self.passwordImage];
    
    self.pageLabel = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 100, self.view.frame.size.height/2 - 30, 200, 60)];
    [self.pageLabel setHidden:YES];
    [self.pageLabel setBackgroundColor:[UIColor colorWithWhite:0.869 alpha:0.900]];
    [self.pageLabel.layer setCornerRadius:8.0f];
    [self.pageLabel.layer setBorderColor:[UIColor colorWithWhite:0.500 alpha:0.900].CGColor];
    [self.pageLabel.layer setBorderWidth:1.0];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, self.pageLabel.frame.size.height/2 - 15, self.pageLabel.frame.size.width, 30)];
    [label setText:@"Logging In..."];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:self.pageLabel];
    [self.view bringSubviewToFront:self.pageLabel];
    [self.pageLabel addSubview:label];
    
    self.loadingspinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingspinner setCenter:CGPointMake(25, self.pageLabel.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.pageLabel addSubview:self.loadingspinner];
    [self.pageLabel bringSubviewToFront: self.loadingspinner];
    [self.loadingspinner startAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}


-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
    
    BOOL loggedIn = [userDefaults boolForKey:@"stayLoggedIn"];
    
  /*  if(loggedIn == YES){
        
        //This logs out the user if they are past their trial date
        NSDate *date14FromCreation = [self checkDate14DaysFromSignup:[PFUser currentUser].createdAt];
        NSString *subscriptionId = [PFUser currentUser][@"SubscriptionId"];
        
        if([subscriptionId isEqualToString:@"8I32FeQiGE"] && ([[NSDate date] compare: date14FromCreation] == NSOrderedDescending)){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Trial Expired" message: @"Your 14 day unlimited trial of iTrapp has expired. Please head to our website at http://itrappshoot.com to manage you account settings. The data from your trial is stored safely until you upgrade your account status. Thank you." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [PFUser logOut];
            [userDefaults setBool:NO forKey:@"stayLoggedIn"];
            [alert show];
            [spinner stopAnimating];
        }
        else{
            
            
            if([PFUser currentUser]){
                
                
                [self.pageLabel setHidden:NO];
                //[spinner startAnimating];
                UIStoryboard* storyboard;
                if(self.view.frame.size.width > 320)
                {
                    storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
                    
                }
                else{
                    
                    storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.scoringPage =
                    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
                    
                    [self presentViewController:scoringPage animated: NO completion:^{
                        
                        //[spinner stopAnimating];
                        [self.loadingspinner stopAnimating];
                        [self.pageLabel setHidden:YES];
                
                        
                    }];

                });
            }
        
            
            else{
                
                
            }
            
        }
    }
    else{
    }*/

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginAction:(id)sender {
    
    
    [self login];
}


-(void)login{
    
    //NEED TO CHECK THIS OUT WITH LOGGING IN WITHOUT INTERNET, MAKE STAY LOGGED IN SWITCH 1/17/14
    
    
    [spinner startAnimating];
    [PFUser logInWithUsernameInBackground: self.usernameTextField.text password: self.passwordTextField.text block:^(PFUser *user, NSError *error) {
        
        if (user) {
            
            NSDate *date14FromCreation = [self checkDate14DaysFromSignup:[PFUser currentUser].createdAt];
            NSString *subscriptionId = [PFUser currentUser][@"SubscriptionId"];
            //THIS IS THE END OF THE TRIAL PERIOD
           if([subscriptionId isEqualToString:@"8I32FeQiGE"] && [[NSDate date] compare: date14FromCreation] == NSOrderedDescending){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Trial Expired" message: @"Your 14 day unlimited trial of iTrapp has expired. Please head to our website at http://itrappshoot.com to manage you account settings. The data from your trial is stored safely until you upgrade your account status. Thank you." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [spinner stopAnimating];
            }
            //REGULAR PAID ACCOUNT
            else{
        
                //WARNING USERS OF THEIR TRIAL EXPIRATION DATE
                if([subscriptionId isEqualToString:@"8I32FeQiGE"] && ([[NSDate date] compare: date14FromCreation] == NSOrderedAscending || [[NSDate date] compare: date14FromCreation] == NSOrderedSame)){
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Trial Period" message: [NSString stringWithFormat: @"You are still in your 14 day unlimited trial of iTrapp. Please keep in mind that you trial expires on %@. The data from your trial is stored safely after your trial until you upgrade your account status. Thank you.", [self getDate:date14FromCreation]] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [spinner stopAnimating];
                }

                
                if ([self.stayLoggedInSwitch isOn]) {
                    
                    [userDefaults setBool:YES forKey:@"stayLoggedIn"];
                }
                else{
                    
                    [userDefaults setBool:NO forKey:@"stayLoggedIn"];
                }
                
                
                UIStoryboard* storyboard;
                if(self.view.frame.size.width > 320)
                {
                    storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
                    
                }
                else{
                    
                    storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
                }
                self.scoringPage =
                [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
                
              
                self.updatedData = [[UpdatedData alloc] init];
                self.updatedData.delegate = self;
                
                
                if([self isNetworkAvailable]){
                    
                    PFQuery *query = [PFQuery queryWithClassName:@"Subscription"];
                    [query whereKey:@"objectId" equalTo:[PFUser currentUser][@"SubscriptionId"]];
                    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                        
                        if(objects){
                            
                            
                            NSMutableArray *subscription = [[NSMutableArray alloc] init];
                            [subscription addObjectsFromArray:objects];
                            
                            if(subscription.count > 0)
                            {
                                
                                PFObject *subObject = [PFObject objectWithClassName:@"Subscription"];
                                subObject = [subscription objectAtIndex:0];
                                
                                [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithInt: [subObject[@"shooterCap"] intValue]] forKey:@"shooterCap"];
                                [[NSUserDefaults standardUserDefaults] setObject:  subObject[@"name"] forKey:@"name"];
                                // NSLog(@"ShooterCap : %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"shooterCap"]);
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    self.scoringPage =
                                    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
                                    
                                    [self presentViewController:scoringPage animated: NO completion:^{
                                        
                                        [spinner stopAnimating];
                                        
                                        
                                    }];
                                    
                                });

                            }
                        }
                        else{
                            
                            //NSLog(@"ERROR");
                            
                        }
                        
                        
                        //[self.updatedData performSelector:@selector(getSubscriptions) withObject:nil afterDelay:0.2];
                        //[self.updatedData getShooters];
                        //[self.updatedData getCoaches];
                        //[self getDataArrays];
                        
                        
                    }];

                   
                    
                
                }
            }
            
                
            } else {
                
                [spinner stopAnimating];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Account" message:@"Username or password is incorrect." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                
            }
            
        }];
        
}


-(NSDate *) checkDate14DaysFromSignup : (NSDate *)startDate {
    
    NSDate *now = startDate;
    int daysToAdd = 14;  // or 60 :-)
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *newDate = [gregorian dateByAddingComponents:components toDate:now options:0];
    //NSLog(@"Start: %@, 14 days out: %@", startDate ,newDate);
    
    return newDate;
    
    
}

-(NSString*) getDate : (NSDate *)dateToConvert {
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    dateString = [formatter stringFromDate:dateToConvert];
    
    return dateString;
}



-(void)retrieveData {
    
    counter++;
    //NSLog(@"Counter: %d", counter);

    if(counter == 2){
        
        //NSLog(@"Too many");
           
        
        [self presentScoringPage];
        
        
    }
    
    
}

-(void) presentScoringPage {
    
    [spinner stopAnimating];
    [self presentViewController:self.scoringPage animated:YES completion:nil];
}


-(void) viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    self.usernameTextField.text = @"";
    self.passwordTextField.text = @"";
    counter = 0;
    
}

- (IBAction)nextOnKeyboard:(id)sender {
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField becomeFirstResponder];
}

- (IBAction)dismissKeyboard:(id)sender {
    
    [self resignFirstResponder];
    [self login];
}

- (IBAction)tapDismiss:(id)sender {
    
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}


- (IBAction)signUp:(id)sender {
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    self.signUp =
    [storyboard instantiateViewControllerWithIdentifier:@"SignUp"];
    
    [self presentViewController:signUp animated:YES completion:nil];

}

-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        //NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        //NSLog(@"-> connection established!APPDelegate\n");
        return YES;
    }
}

- (IBAction)forgotPassword:(id)sender {
    
    UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Reset Password" message:@"Please enter the email address asscoicated with your account. An email will be sent with a link to reset your password." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    av.tag=2;
    av.alertViewStyle = UIAlertViewStylePlainTextInput;
    [av show];
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 2){
        
        if(buttonIndex == 0){
            
            
            
        }
        else{
            
            [PFUser requestPasswordResetForEmailInBackground: [alertView textFieldAtIndex:0].text];
            
            
            UIAlertView *av = [[UIAlertView alloc]initWithTitle:@"Check Email" message:@"The request to change your password has been sent to the email associated with your account. Please check you email to reset your password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
            [av show];

            
        }
        
    }
    
    
    
    
    
}
@end
