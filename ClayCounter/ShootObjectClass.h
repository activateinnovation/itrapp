//
//  ShootObjectClass.h
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "Coach.h"

@interface ShootObjectClass : NSObject

@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) PFObject *coach;
@property int shotsTotal;
@property int shotsAtEachPost;
@property (strong, nonatomic) NSString *pracOrComp;
-(ShootObjectClass *) initWithItems: (NSMutableArray *) shooters : (PFObject *) coachTemp : (int) totalShots : (int) shotsPerPost;

           
@end
