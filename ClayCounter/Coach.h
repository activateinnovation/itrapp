//
//  Coach.h
//  ClayCounter
//
//  Created by Test on 1/25/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Coach : NSManagedObject

@property (nonatomic, retain) NSString * fName;
@property (nonatomic, retain) NSString * lName;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSString * leftRightHanded;
@property (nonatomic, retain) NSString * objectId;
@property (nonatomic, retain) NSString * email;

@end
