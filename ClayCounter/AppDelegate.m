//
//  AppDelegate.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//
#import <Parse/Parse.h>
#import <ParseCrashReporting/ParseCrashReporting.h>
#import "AppDelegate.h"
#include<unistd.h>
#include<netdb.h>

@implementation AppDelegate
//@synthesize managedObjectContext;
//@synthesize managedObjectModel;
@synthesize shooterPage;
@synthesize updatedData;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    [Parse enableLocalDatastore];
    [ParseCrashReporting enable];
    
  
    //self.updatedShootersArray = [[NSMutableArray alloc] init];
   //shooterPage.managedObjectContext = self.managedObjectContext
    [Parse setApplicationId:@"7USes2fLXpcHoov71GeapFfg5mgag3FpLtHOzO4i"
                  clientKey:@"3EEgAsd1myQFuPsZXLTPH1yctG7YxcfTTHY5ru9q"];
    
    // Enable Crash Reporting

    [MagicalRecord setupCoreDataStackWithStoreNamed:@"<Project Name>.sqlite"];
   
    //updatedData = [[UpdatedData alloc] init];

    if([self isNetworkAvailable]){
   
        
       [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
       
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"reload"];
    
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:(87/255.f) green:(196/255.f) blue:(234/255.f) alpha:1.0f]];
    
    [self pushToMainPage];
    
    // Override point for customization after application launch.
    return YES;
}

-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        NSLog(@"-> connection established!APPDelegate\n");
        return YES;
    }
}

-(void) pushToMainPage {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL loggedIn = [userDefaults boolForKey:@"stayLoggedIn"];
    
    if(loggedIn == YES){
        
        //This logs out the user if they are past their trial date
        NSDate *date14FromCreation = [self checkDate14DaysFromSignup:[PFUser currentUser].createdAt];
        NSString *subscriptionId = [PFUser currentUser][@"SubscriptionId"];
        
        if([subscriptionId isEqualToString:@"8I32FeQiGE"] && ([[NSDate date] compare: date14FromCreation] == NSOrderedDescending)){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Trial Expired" message: @"Your 14 day unlimited trial of iTrapp has expired. Please head to our website at http://itrappshoot.com to manage you account settings. The data from your trial is stored safely until you upgrade your account status. Thank you." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [PFUser logOut];
            [userDefaults setBool:NO forKey:@"stayLoggedIn"];
            [alert show];
            //[spinner stopAnimating];
        }
        else{
            
            
            if([PFUser currentUser]){
                
                
               // [self.pageLabel setHidden:NO];
                //[spinner startAnimating];
                UIStoryboard* storyboard;
                if(self.window.frame.size.width > 320)
                {
                    storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
                    
                }
                else{
                    
                    storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
                }
                
                ScoringPage *scoringPage =
                [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
                
                
                self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
                self.window.rootViewController = scoringPage;
                [self.window makeKeyAndVisible];

                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                                   });
            }
            
            
            else{
                
                
            }
            
        }
    }
    else{
    }

    
}

-(NSDate *) checkDate14DaysFromSignup : (NSDate *)startDate {
    
    NSDate *now = startDate;
    int daysToAdd = 14;  // or 60 :-)
    
    // set up date components
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setDay:daysToAdd];
    
    // create a calendar
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *newDate = [gregorian dateByAddingComponents:components toDate:now options:0];
    //NSLog(@"Start: %@, 14 days out: %@", startDate ,newDate);
    
    return newDate;
    
    
}



/*- (NSManagedObjectContext *) managedObjectContext {
    if (managedObjectContext != nil) {
        return managedObjectContext;
    }
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (managedObjectModel != nil) {
        return managedObjectModel;
    }
    managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    return managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationDocumentsDirectory]stringByAppendingPathComponent: @"<Project Name>.sqlite"]];
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc]
                                  initWithManagedObjectModel:[self managedObjectModel]];
    if(![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                 configuration:nil URL:storeUrl options:nil error:&error]) {
        Error for store creation should be handled in here
    }
    
    return persistentStoreCoordinator;
}

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}*/


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    //[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"reload"];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
    [MagicalRecord cleanUp];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
