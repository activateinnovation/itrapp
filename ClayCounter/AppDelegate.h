//
//  AppDelegate.h
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <CoreData/CoreData.h>
#import <UIKit/UIKit.h>
#import "ShootersPage.h"
#import "UpdatedData.h"
#import "ScoringPage.h"

@class ShootersPage;
@class UpdatedData;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
    

@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property ShootersPage *shooterPage;
@property UpdatedData *updatedData;
@property (strong, nonatomic) UIWindow *window;

@end
