//
//  TrapField.m
//  ClayCounter
//
//  Created by ActivateInnovation on 5/29/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "TrapField.h"


@implementation TrapField

@dynamic objectId;
@dynamic name;
@dynamic phone;
@dynamic street;
@dynamic state;
@dynamic city;
@dynamic zipCode;

@end
