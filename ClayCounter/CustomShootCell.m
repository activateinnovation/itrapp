//
//  CustomShootCell.m
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "CustomShootCell.h"

@implementation CustomShootCell
@synthesize shotsCounter;
@synthesize shotsInShootCounter;
@synthesize totalShotsInShoot;
@synthesize totalShotsInShootLabel;
@synthesize hitsOverall;
@synthesize hitsOverallLabel;
@synthesize textField0, textField1,textField2,textField3,textField4,textField5,textField6,textField7,textField8,textField9;
@synthesize shooterName;
@synthesize shotsFired;
@synthesize shotsFiredInRound;
@synthesize shotsAtEachPost;
@synthesize tempScores;
@synthesize roundLabel;
@synthesize postNumber;
@synthesize scoringShootPage;
@synthesize viewAlpha;
@synthesize isIndex;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier :(NSString *) totalShots :(NSString *)shotsFiredTemp :(NSString *)post :(NSString *)shootername :(NSNumber *)shotsAtEachPostTemp :(int)hitsOverallTemp : (NSMutableArray *)shooterScoreArray : (int)roundNumberTemp : (int) shotsFiredInRoundTemp : (NSMutableArray *)shootShooter : (BOOL) isIndexLocal
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.shootShooterArray = [[NSMutableArray alloc] init];
        self.shootShooterArray = shootShooter;
        self.textFieldArray = [[NSMutableArray alloc] init];
        self.shotsAtEachPost = [shotsAtEachPostTemp intValue];
        self.roundNumber = roundNumberTemp;
        self.shotsFiredInRound = shotsFiredInRoundTemp;
        self.scoreArray = [[NSMutableArray alloc] init];
        self.scoreArray = shooterScoreArray;
        tempScores = [[NSMutableArray alloc] init];
        self.postNumber = [post intValue];
        self.shotsFired = [shotsFiredTemp intValue];
        self.scoringShootPage = [[ScoringShootPage alloc] init];
        self.frame = CGRectMake(0, 0, 260, 170);
        self.isIndex = isIndexLocal;
      
        [self setBackgroundColor:[UIColor clearColor]];
        
        self.roundLabel = [[UILabel alloc] initWithFrame:CGRectMake(26, 26, 90, 30)];
        self.roundLabel.text = [NSString stringWithFormat:@"Round: #%d", (self.roundNumber + 1)];
        [self.roundLabel setFont:[UIFont systemFontOfSize:15]];
        
        self.totalShotsInShootLabel = [[UILabel alloc] initWithFrame:CGRectMake(200, 6 , 56, 45)];
        self.totalShotsInShootLabel.text = [NSString stringWithFormat:@"%d/%@ Fired", shotsFired , totalShots];
        self.totalShotsInShootLabel.numberOfLines = 2;
        [self.totalShotsInShootLabel setBackgroundColor:[UIColor clearColor]];
        /*[self.totalShotsInShootLabel setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
        [self.totalShotsInShootLabel.layer setBorderColor:[UIColor blackColor].CGColor];
        [self.totalShotsInShootLabel.layer setBorderWidth:1.0f];*/
        [self.totalShotsInShootLabel setFont:[UIFont boldSystemFontOfSize:17]];
        [self.totalShotsInShootLabel setTextAlignment:NSTextAlignmentCenter];
        self.totalShotsInShootLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.totalShotsInShootLabel setTextColor:[UIColor darkTextColor]];
        
        UIView *labelBackground = [[UIView alloc] initWithFrame:CGRectMake(144, 6, 112, 45)];
        [labelBackground setBackgroundColor: [UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
        [labelBackground.layer setBorderColor:[UIColor blackColor].CGColor];
        [labelBackground.layer setBorderWidth:1.0f];
        
        
        self.hitsOverallLabel = [[UILabel alloc] initWithFrame:CGRectMake(144, 6 , 56, 45)];
        self.hitsOverall = hitsOverallTemp;
        self.hitsOverallLabel.text = [NSString stringWithFormat:@"%d/%@ Hits", hitsOverall, totalShots];
        //[self.hitsOverallLabel setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
        [self.hitsOverallLabel setBackgroundColor:[UIColor clearColor]];
        [self.hitsOverallLabel setTextAlignment:NSTextAlignmentCenter];
        [self.hitsOverallLabel setFont:[UIFont boldSystemFontOfSize:17]];
        self.hitsOverallLabel.numberOfLines = 2;
        self.hitsOverallLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self.hitsOverallLabel setTextColor:[UIColor darkTextColor]];
    
        self.postCounter = [[UILabel alloc] initWithFrame:CGRectMake(100, 56, 80, 25)];
        self.postCounter.text = [NSString stringWithFormat:@"Post: #%d", self.postNumber];
        [self.postCounter setTextColor:[UIColor colorWithRed:(105/255.f) green: (138/255.f) blue: (51/255.f) alpha:1.0f]];
        [self.postCounter setFont:[UIFont boldSystemFontOfSize:16]];
    
        
        
        self.shooterName = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 180, 28)];
        [self.shooterName setFont:[UIFont systemFontOfSize:18]];
        [self.shooterName setTextColor:[UIColor colorWithRed:(226/255.f) green:(58/255.f) blue:(50/255.f) alpha:1.0f]];
        self.shooterName.text = shootername;
        
        
        self.viewAlpha = [[UIView alloc] init];
        [self.viewAlpha setBackgroundColor:[UIColor whiteColor]];
        [self.viewAlpha setAlpha:0.8];
        [self.viewAlpha.layer setBorderColor:[UIColor darkGrayColor].CGColor];
        [self.viewAlpha.layer setBorderWidth:2.8f];
        
       
        if([shotsAtEachPostTemp intValue] == 2){
            
            self.textField0 = [[UITextField alloc] initWithFrame:CGRectMake(88.5, 80, 40, 40)];
            [self.textField0 setTextAlignment:NSTextAlignmentCenter];
            [self.textField0 setBorderStyle:UITextBorderStyleLine];
            self.textField0.delegate = self;
            [self.textField0 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
            //[self.textField0 setAlpha:0.7];
            self.textField0.tag = 0;
            
            self.textField1 = [[UITextField alloc] initWithFrame:CGRectMake(132, 80, 40, 40)];
            [self.textField1 setBorderStyle:UITextBorderStyleLine];
            [self.textField1 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
            [self.textField1 setTextAlignment:NSTextAlignmentCenter];
            // [self.textField1 setAlpha:0.7];
            self.textField1.tag = 1;
            self.textField1.delegate = self;
            
            [self addSubview:self.textField0];
            [self addSubview:self.textField1];

            [self.textFieldArray addObject:self.textField0];
            [self.textFieldArray addObject:self.textField1];
            
            if(self.shotsFiredInRound > 0){
                
                [self updateScoresFor5];
                //NSLog(@"Above O");
            }


            
            
            
        }
        else if([shotsAtEachPostTemp intValue] == 5){

            //NSLog(@"HEREINSIDE");
            self.textField0 = [[UITextField alloc] initWithFrame:CGRectMake(24, 80, 40, 40)];
            [self.textField0 setTextAlignment:NSTextAlignmentCenter];
            [self.textField0 setBorderStyle:UITextBorderStyleLine];
            self.textField0.delegate = self;
            [self.textField0 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
            //[self.textField0 setAlpha:0.7];
            self.textField0.tag = 0;
           
            self.textField1 = [[UITextField alloc] initWithFrame:CGRectMake(67, 80, 40, 40)];
            [self.textField1 setBorderStyle:UITextBorderStyleLine];
            [self.textField1 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
            [self.textField1 setTextAlignment:NSTextAlignmentCenter];
           // [self.textField1 setAlpha:0.7];
            self.textField1.tag = 1;
            self.textField1.delegate = self;
            
            self.textField2 = [[UITextField alloc] initWithFrame:CGRectMake(110, 80, 40, 40)];
            [self.textField2 setBorderStyle:UITextBorderStyleLine];
            [self.textField2 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
            [self.textField2 setTextAlignment:NSTextAlignmentCenter];
           // [self.textField2 setAlpha:0.7];
             self.textField2.tag = 2;
            self.textField2.delegate = self;
            
            self.textField3 = [[UITextField alloc] initWithFrame:CGRectMake(153, 80, 40, 40)];
            [self.textField3 setBorderStyle:UITextBorderStyleLine];
            [self.textField3 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
            [self.textField3 setTextAlignment:NSTextAlignmentCenter];
           // [self.textField3 setAlpha:0.7];
             self.textField3.tag = 3;
            self.textField3.delegate = self;
           
            self.textField4 = [[UITextField alloc] initWithFrame:CGRectMake(196, 80, 40, 40)];
            [self.textField4 setBorderStyle:UITextBorderStyleLine];
            [self.textField4 setTextAlignment:NSTextAlignmentCenter];
            [self.textField4 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
           // [self.textField4 setAlpha:0.7];
             self.textField4.tag = 4;
            self.textField4.delegate = self;

            
            
            
            [self addSubview:self.textField0];
            [self addSubview:self.textField1];
            [self addSubview:self.textField2];
            [self addSubview:self.textField3];
            [self addSubview:self.textField4];
 
            
            [self.textFieldArray addObject:self.textField0];
            [self.textFieldArray addObject:self.textField1];
            [self.textFieldArray addObject:self.textField2];
            [self.textFieldArray addObject:self.textField3];
            [self.textFieldArray addObject:self.textField4];

            if(self.shotsFiredInRound > 0){
                
                [self updateScoresFor5];
                //NSLog(@"Above O");
            }
           
            
        }
        else{
            //10
            self.textField0 = [[UITextField alloc] initWithFrame:CGRectMake(24, 80, 40, 40)];
            [self.textField0 setBorderStyle:UITextBorderStyleLine];
            [self.textField0 setBackgroundColor: [UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];
;
            [self.textField0 setTextAlignment:NSTextAlignmentCenter];
            self.textField0.tag = 0;
            //[self.textField0 setAlpha:0.7];
            self.textField0.delegate = self;
            self.textField1 = [[UITextField alloc] initWithFrame:CGRectMake(67, 80, 40, 40)];
            [self.textField1 setBorderStyle:UITextBorderStyleLine];
            [self.textField1 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField1.tag = 1;
            [self.textField1 setTextAlignment:NSTextAlignmentCenter];
            //[self.textField1 setAlpha:0.7];
            self.textField1.delegate = self;
            self.textField2 = [[UITextField alloc] initWithFrame:CGRectMake(110, 80, 40, 40)];
            [self.textField2 setTextAlignment:NSTextAlignmentCenter];
            [self.textField2 setBorderStyle:UITextBorderStyleLine];
            [self.textField2 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField2.tag = 2;
            //[self.textField2 setAlpha:0.7];
            self.textField2.delegate = self;
            self.textField3 = [[UITextField alloc] initWithFrame:CGRectMake(153, 80, 40, 40)];
            [self.textField3 setBorderStyle:UITextBorderStyleLine];
            [self.textField3 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField3.tag = 3;
            [self.textField3 setTextAlignment:NSTextAlignmentCenter];
            //[self.textField3 setAlpha:0.7];
              self.textField3.delegate = self;
            self.textField4 = [[UITextField alloc] initWithFrame:CGRectMake(196, 80, 40, 40)];
            [self.textField4 setTextAlignment:NSTextAlignmentCenter];
            [self.textField4 setBorderStyle:UITextBorderStyleLine];
            [self.textField4 setBackgroundColor: [UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField4.tag = 4;
            //[self.textField4 setAlpha:0.7];
              self.textField4.delegate = self;
            self.textField5 = [[UITextField alloc] initWithFrame:CGRectMake(24, 121, 40, 40)];
            [self.textField5 setBorderStyle:UITextBorderStyleLine];
            [self.textField5 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField5.tag = 5;
            [self.textField5 setTextAlignment:NSTextAlignmentCenter];
            //[self.textField5 setAlpha:0.7];
              self.textField5.delegate = self;
            self.textField6 = [[UITextField alloc] initWithFrame:CGRectMake(67, 121, 40, 40)];
            [self.textField6 setBorderStyle:UITextBorderStyleLine];
            [self.textField6 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField6.tag = 6;
            [self.textField6 setTextAlignment:NSTextAlignmentCenter];
            //[self.textField6 setAlpha:0.7];
              self.textField6.delegate = self;
            self.textField7 = [[UITextField alloc] initWithFrame:CGRectMake(110, 121, 40, 40)];
              self.textField7.tag = 7;
            [self.textField7 setTextAlignment:NSTextAlignmentCenter];
            [self.textField7 setBorderStyle:UITextBorderStyleLine];
            [self.textField7 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

           // [self.textField7 setAlpha:0.7];
              self.textField7.delegate = self;
            self.textField8 = [[UITextField alloc] initWithFrame:CGRectMake(153, 121, 40, 40)];
            [self.textField8 setTextAlignment:NSTextAlignmentCenter];
            [self.textField8 setBorderStyle:UITextBorderStyleLine];
            [self.textField8 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

              self.textField8.tag = 8;
            //[self.textField8 setAlpha:0.7];
              self.textField8.delegate = self;
            self.textField9 = [[UITextField alloc] initWithFrame:CGRectMake(196, 121, 40, 40)];
            [self.textField9 setTextAlignment:NSTextAlignmentCenter];
            [self.textField9 setBorderStyle:UITextBorderStyleLine];
              self.textField9.tag = 9;
            [self.textField9 setBackgroundColor:[UIColor colorWithRed:(230/255.f) green:(231/255.f) blue:(231/255.f) alpha:1.0f]];

            //[self.textField9 setAlpha:0.7];
              self.textField9.delegate = self;
            [self addSubview:self.textField0];
            [self addSubview:self.textField1];
            [self addSubview:self.textField2];
            [self addSubview:self.textField3];
            [self addSubview:self.textField4];
            [self addSubview:self.textField5];
            [self addSubview:self.textField6];
            [self addSubview:self.textField7];
            [self addSubview:self.textField8];
            [self addSubview:self.textField9];
            
            [self.textFieldArray addObject:self.textField0];
            [self.textFieldArray addObject:self.textField1];
            [self.textFieldArray addObject:self.textField2];
            [self.textFieldArray addObject:self.textField3];
            [self.textFieldArray addObject:self.textField4];
            [self.textFieldArray addObject:self.textField5];
            [self.textFieldArray addObject:self.textField6];
            [self.textFieldArray addObject:self.textField7];
            [self.textFieldArray addObject:self.textField8];
            [self.textFieldArray addObject:self.textField9];
            
            UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(9, 91, 15, 20)];
            label1.text = @"1";
            
            UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(9, 132, 15, 20)];
            label2.text = @"2";
            
            
            [self addSubview:label1];
            [self addSubview:label2];
            
            if(self.scoreArray.count > 0){
                
                [self updateScoresFor10];
            }
            
        }
        
        //NSLog(@"ShotsFiredInRound: %d, Shotsperpost: %d", shotsFiredInRound, shotsAtEachPost);
      //NEED to Mess With This In Order to get The Undo with Clicking on TextField Figured out, may need to set up and editing mode with different variables.
        if(shotsFiredInRound < shotsAtEachPost)
        {
            [self.viewAlpha setFrame:[[self.textFieldArray objectAtIndex:self.shotsFiredInRound] frame]];
            
        }

        [self addSubview:labelBackground];
        [self sendSubviewToBack:labelBackground];
        [self addSubview:self.roundLabel];
        [self addSubview:self.hitsOverallLabel];
        [self addSubview:self.postCounter];
        [self addSubview:shooterName];
        [self addSubview: self.totalShotsInShootLabel];
        
        if(self.isIndex){
            
            
            [self addSubview:viewAlpha];
            [self bringSubviewToFront:viewAlpha];
        }
        
    }

    
           return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateScoresFor5{
   
    int x;
    int shotsTemp;
    //NSLog(@"RoundNumber: %d", self.roundNumber);
    if(self.roundNumber == 0){
        
        //x = (self.roundNumber * shotsAtEachPost);
        //NSLog(@"X: %d", x);
        //shotsTemp = (x + shotsFiredInRound);
        //NSLog(@"ShotsFiredInRound#1: %d", shotsFiredInRound);
        x = 0;
        shotsTemp = self.shotsFiredInRound;
        
    }else{
        
         x = (self.roundNumber * shotsAtEachPost);
        //NSLog(@"X: %d", x);
        shotsTemp = (x + shotsFiredInRound);
        //shotsTemp = 5;
   
    }
    
   
    
    
    ////NSLog(@"ShotsFiredInRound: %d", shotsFiredInRound);
    //NSLog(@"shotsTemp: %d", shotsTemp);
    for(int i = x; i < shotsTemp; i++){
        
        //NSLog(@"i: %d", i);
        [tempScores addObject:[self.scoreArray objectAtIndex:i]];
    }
    //NSLog(@"tempScores: %@", tempScores);
    //NSLog(@"CellScore: %@", self.scoreArray);


    for(int i = 0; i < tempScores.count; i++){
        
        [[self.textFieldArray objectAtIndex:i]setText:[tempScores objectAtIndex:i]];
        if([[tempScores objectAtIndex:i] isEqualToString: @"L+"] || [[tempScores objectAtIndex:i] isEqualToString: @"L"] || [[tempScores objectAtIndex:i] isEqualToString:@"S"] || [[tempScores objectAtIndex:i] isEqualToString: @"R"] || [[tempScores objectAtIndex:i] isEqualToString: @"R+"] || [[tempScores objectAtIndex:i] isEqualToString: @"O"] ){
            
            [[self.textFieldArray objectAtIndex:i] setTextColor:[UIColor colorWithRed:(226/255.f) green:(58/255.f) blue:(50/255.f) alpha:1.0f]];
            [[self.textFieldArray objectAtIndex:i] setFont:[UIFont boldSystemFontOfSize:20]];
            
            
        }
        else{
            
            [[self.textFieldArray objectAtIndex:i] setTextColor:[UIColor colorWithRed:(105/255.f) green: (138/255.f) blue: (51/255.f) alpha:1.0f]];
            [[self.textFieldArray objectAtIndex:i] setFont:[UIFont boldSystemFontOfSize:21]];
        }
    }
}

-(void)updateScoresFor10 {
    

    int x;
    int shotsTemp;
    if(self.roundNumber == 0){
        
        x = 0;
        shotsTemp = self.shotsFiredInRound;
        
    }else{
        
        x = (self.roundNumber * shotsAtEachPost);
        //NSLog(@"X: %d", x);
        shotsTemp = (x + shotsFiredInRound);
        
    }
    
    
    
    //NSLog(@"ShotsFiredInRound: %d", shotsFiredInRound);
    for(int i = x; i < shotsTemp; i++){
        
        [tempScores addObject:[self.scoreArray objectAtIndex:i]];
    }
    //NSLog(@"tempScores: %@", tempScores);
    //NSLog(@"CellScore: %@", self.scoreArray);
    
    
    for(int i = 0; i < tempScores.count; i++){
        
        [[self.textFieldArray objectAtIndex:i]setText:[tempScores objectAtIndex:i]];
        
    }
    
    for(int i = 0; i < tempScores.count; i++){
        
        [[self.textFieldArray objectAtIndex:i] setText:[tempScores objectAtIndex:i]];
        if([[tempScores objectAtIndex:i] isEqualToString: @"L+"] || [[tempScores objectAtIndex:i] isEqualToString: @"L"] || [[tempScores objectAtIndex:i] isEqualToString:@"S"] || [[tempScores objectAtIndex:i] isEqualToString: @"R"] || [[tempScores objectAtIndex:i] isEqualToString: @"R+"] || [[tempScores objectAtIndex:i] isEqualToString: @"O"]){
            
            
            [[self.textFieldArray objectAtIndex:i] setTextColor:[UIColor colorWithRed:(226/255.f) green:(58/255.f) blue:(50/255.f) alpha:1.0f]];
            [[self.textFieldArray objectAtIndex:i] setFont:[UIFont boldSystemFontOfSize:20]];
            
            
        }
        else{
            
            [[self.textFieldArray objectAtIndex:i] setTextColor:[UIColor colorWithRed:(105/255.f) green: (138/255.f) blue: (51/255.f) alpha:1.0f]];
            [[self.textFieldArray objectAtIndex:i] setFont:[UIFont boldSystemFontOfSize:21]];
            
        }
        
    }

    
}


-(void) clearTextFields{
    
    for(int i = 0; i < tempScores.count; i++){
        
        [[self.textFieldArray objectAtIndex:i] setText: nil];
        
    }
    
    
}


-(void) textFieldDidBeginEditing:(UITextField *)textField{
    //NSLog(@"TextField%d", textField.tag);
    //NSLog(@"Did end editing");
    
    [textField resignFirstResponder];
    
   
    NSString *tempScoreString = textField.text;
    if([tempScoreString isEqualToString:@""]){

        
    }
    else{
    
       
        
       [_delegate scoreFieldClicked: (int)self.tag : textField :self.shootShooterArray];
        
    }

}


-(void) highlightTextField: (UITextField *)textField{
    NSLog(@"HighLight Field");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.viewAlpha setFrame: textField.frame];
        
    });
}




@end
