//
//  ShooterShootObject.h
//  ClayCounter
//
//  Created by ActivateInnovation on 2/9/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Shooter.h"
#import <Parse/Parse.h>

@class Shooter;

@interface ShooterShootObject : NSObject

@property (strong, nonatomic) PFObject *shooter;
@property int postNumber;
@property int roundNumber;
@property int shotCounter;
@property int hitNumber;
@property int hitsInRound;
@property (strong, nonatomic) NSNumber *handicap;
@property (strong, nonatomic) NSMutableArray *scoreArray;
@property int shotsFiredInRound;
-(ShooterShootObject *) initWithItems: (PFObject *)shooterTemp : (int)postNumberTemp : (int) shotCounterTemp : (int) hitNumberTemp :(NSMutableArray *)scoreArrayTemp : (int)shotsFiredInRoundTemp : (int) hitsInShootTemp : (NSNumber *)handicapTemp;


@end
