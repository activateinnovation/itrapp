//
//  LoginView.h
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "ShootersPage.h"
#import "SignUpView.h"
#import "UpdatedData.h"
#import "ScoringPage.h"



@class ShootersPage;
@class SignUpView;
@class UpdatedData;
@class ScoringPage;

@interface LoginView : UIViewController <UpdateDataDelegate, UIAlertViewDelegate>

- (IBAction)forgotPassword:(id)sender;

@property UpdatedData *updatedData;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
- (IBAction)signUp:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UISwitch *stayLoggedInSwitch;
@property (strong, nonatomic) ScoringPage *scoringPage;
@property SignUpView *signUp;
@property ShootersPage *shooterPage;
- (IBAction)loginAction:(id)sender;
- (IBAction)nextOnKeyboard:(id)sender;
- (IBAction)dismissKeyboard:(id)sender;
@property int counter;
- (IBAction)tapDismiss:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *userNameImage;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) UIActivityIndicatorView *loadingspinner;
@property (strong, nonatomic) IBOutlet UIImageView *passwordImage;
@property (strong, nonatomic) UIView *pageLabel;
@end
