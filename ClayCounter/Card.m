//
//  Card.m
//  ClayCounter
//
//  Created by Test on 4/1/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "Card.h"

@interface Card ()

@end

@implementation Card
@synthesize scoringPage;
@synthesize webView;
@synthesize shootersArray;
@synthesize shooterShootObjectsArray;
@synthesize index1;
@synthesize spinner;


@synthesize storedShoot;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    //NSLog(@"HERE");
  
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];


}

-(BOOL) prefersStatusBarHidden
{
    return YES;
}

-(void) viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
   
    
}

-(BOOL)shouldAutorotate{
    
    return NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return  interfaceOrientation == UIInterfaceOrientationLandscapeRight;
}

- (void)viewDidLoad
{
    
    self.shootersArray = [[NSMutableArray alloc] init];
    self.shooterShootObjectsArray = [[NSMutableArray alloc] init];
    
    self.reorederShootData = [[NSMutableArray alloc] init];

    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];

    
    if(self.view.frame.size.width > 600){
        
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 40, 1024, 700)];
        
        
    }
    else{
        
        self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 40, 575, 280)];
    }
    
    [self.webView setBackgroundColor:[UIColor clearColor]];
    self.webView.delegate = self;
    [self.webView setUserInteractionEnabled:YES];
    webView.opaque = NO;
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"index" withExtension:@"html"];
    [self.view addSubview: self.webView];
    
    [spinner startAnimating];
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    
   
    
    
    [super viewDidLoad];
     [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
   
    // Dispose of any resources that can be recreated.
}

//Rotates the view to landscape
-(void)showScoreCard : (int) index : (NSMutableArray *)shootShooters : (NSMutableArray *)shooterShootObjects : (PFObject *)storedShootTemp {
    
    //[self.view setTransform:CGAffineTransformMakeRotation(-1.57)];
    //[self.view setFrame:CGRectMake(0, 0, self.view.frame.size.height, self.view.frame.size.width)];
    CGAffineTransform transform = CGAffineTransformMakeRotation(-M_PI/2);
    self.view.transform = transform;
    
    // Repositions and resizes the view.
    // If you need NavigationBar on top then set height appropriately
    CGRect contentRect = CGRectMake(0, 0, 480, 320);
    self.view.bounds = contentRect;

 
    
    self.index1 = index;
    self.shootersArray = shootShooters;
    self.shooterShootObjectsArray = shooterShootObjects;
    self.storedShoot = [PFObject objectWithClassName:@"Shoot"];
    self.storedShoot = storedShootTemp;
    
    
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    dateString = [formatter stringFromDate: self.storedShoot.createdAt];
    
    
    NSString *trapFieldString;
    if([self.storedShoot[@"trapFieldId"] isEqualToString:@"NA"]){
        
        trapFieldString = @"No Range Recorded";
    }
    else{
        PFObject *trapField = [PFObject objectWithClassName:@"TrapField"];
        PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
        [query fromLocalDatastore];
        trapField = [query getObjectWithId:storedShoot[@"trapFieldId"]];
        trapFieldString = trapField[@"Name"];
    }
    

    self.titleInfoLabel.text = [NSString stringWithFormat:@"%@- %@- %@ Shots- Cat: %@-\nHouse #%@- %@",dateString , self.storedShoot[@"typeShoot"], self.storedShoot[@"totalShots"], self.storedShoot[@"handicapType"], self.storedShoot[@"trapHouseNumber"], trapFieldString];

    [self reorderData];

}

- (IBAction)cancel:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
    //NSLog(@"page is loading");
    ////NSLog(@"reorderedData: %@", self.reorederShootData);
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    //NSLog(@"finished loading");
    

    [self sendShooter];
    [spinner stopAnimating];
   
}

-(void) sendShooter{
    
    
    for(int x = 0; x < shooterShootObjectsArray.count; x++){
        
   // ShootShooters *tempShootShooter = [shooterShootObjectsArray objectAtIndex:x];
   // Shooter *tempShooter = self.shootersArray[x];
        PFObject *tempShooter = [PFObject objectWithClassName:@"Shooter"];
        tempShooter = self.shootersArray[x];
    NSString *shooterName = [NSString stringWithFormat:@"%@ %@", tempShooter[@"fName"], tempShooter[@"lName"]];
    NSString *scoreData = [self.reorederShootData[x] componentsJoinedByString:@","];
        PFObject *shootShooter = [PFObject objectWithClassName:@"ShootShooters"];
    shootShooter = [self.shooterShootObjectsArray objectAtIndex: x];
    
       
   // NSString *temp = [self.reorederShootData componentsJoinedByString:@","];
        NSString *function = [NSString stringWithFormat: @"getData('%@', '%@', %d, %d);", scoreData, shooterName, [self.storedShoot[@"shotsPerRotation"] intValue], [shootShooter[@"totalHits"] intValue]];
    NSString *result = [self.webView stringByEvaluatingJavaScriptFromString: function];
    NSLog(@"Result: %@", result);

    }
    
}

-(void) reorderData {
    
     //NSLog(@"here");
    
    for(int x = 0; x < shooterShootObjectsArray.count; x++)
    {
        PFObject *temp = [PFObject objectWithClassName:@"ShootShooters"];
        temp = [shooterShootObjectsArray objectAtIndex:x];
        
       // NSLog(@"SCORE DATA As String:%@  %@", temp,  temp[@"shootData"]);
       // NSString *stringData = temp[@"shootData"];
        NSMutableArray *scoreData = temp[@"shootData"];//[[stringData componentsSeparatedByString:@","] mutableCopy];
    
        
       // NSMutableArray *scoreData  = [NSMutableArray arrayWithArray: tempList];
        
      // NSLog(@"SCORE DATA BEFORE: %@", scoreData);
        
      
        NSMutableArray *tempArray = [NSMutableArray arrayWithArray:scoreData];
     

     //   //NSLog(@"ShooterData: %@ Starting Post: %@", scoreData, temp.startingPost);
        
        for(int y = 0; y < scoreData.count; y++){
            
            
            if(scoreData.count == 25){
                
                int pos = y + (([temp[@"startingPost"] intValue] - 1) * [self.storedShoot[@"shotsPerRotation"] intValue]);
                
                if(pos >= scoreData.count){
                    
                    pos = pos % scoreData.count;
                    // //NSLog(@"pos: %d", pos);
                    
                }
                
                [tempArray replaceObjectAtIndex: pos withObject: scoreData[y]];
                
            }

            else{
             
                int pos = y + (([temp[@"startingPost"] intValue] - 1) * [self.storedShoot[@"shotsPerRotation"] intValue]);
                if(pos >= scoreData.count){
                    
                    pos = pos % scoreData.count;
                    ////NSLog(@"pos: %d", pos);
                    
                }
                [tempArray replaceObjectAtIndex: pos withObject: scoreData[y]];
                
            }
            
        }
        
       // //NSLog(@"TempArray: %@", tempArray);
        
        [self.reorederShootData addObject:tempArray];
        
       // //NSLog(@"REORDERED DATA: %@", self.reorederShootData);
        
    }
    
   // }
    
}


- (IBAction)sendEmail:(id)sender {
    
    PFObject *scoringCoach = [PFObject objectWithClassName:@"Coach"];
    PFQuery *query = [PFQuery queryWithClassName:@"Coach"];
    [query fromLocalDatastore];
    [query whereKey:@"appObjectId" equalTo:self.storedShoot[@"coachID"]];
    scoringCoach = [query getFirstObject];
    // Coach *scoringCoach = [Coach MR_findFirstByAttribute:@"objectId" withValue:self.storedShoot.coachId];
    
    PFQuery *query2 = [PFQuery queryWithClassName:@"Coach"];
    [query2 whereKey:@"userId" equalTo:[PFUser currentUser].objectId];
    [query2 fromLocalDatastore];
    [query2 orderByAscending:@"fName"];
    NSArray *coachesArray = [query2 findObjects];//[Coach MR_findByAttribute:@"userId" withValue:[PFUser currentUser].objectId andOrderBy: @"fName"  ascending:YES];
    
    NSMutableArray *tempEmailArray = [[NSMutableArray alloc] init];
    
    PFObject *checkEmailShooter = [PFObject objectWithClassName:@"Shooter"];
    checkEmailShooter = [shootersArray objectAtIndex:0];

    //NSLog(@"EMAIL: %@", checkEmailShooter.email);
    
    
    if([checkEmailShooter[@"email"] isEqualToString:@""] || [scoringCoach[@"email"] isEqualToString:@""]){
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Email Addresses" message:@"Each shooter and coach must have an email address saved in their profile to send socre cards by email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else{
        
        for(int x = 0; x < shootersArray.count; x++)
        {
            PFObject *temp = [PFObject objectWithClassName:@"Shooter"];
            temp = [shootersArray objectAtIndex:0];
            [tempEmailArray addObject:temp[@"email"]];
        }
        
        for(int x = 0; x < coachesArray.count; x++){
            
            PFObject *temp = [PFObject objectWithClassName:@"Coach"];
            temp = [coachesArray objectAtIndex:x];
            if(temp != nil && ![temp isEqual:[NSNull null]]){
               
                [tempEmailArray addObject:temp[@"email"]];
            }
        }
        
        NSArray *finalEmails = [[NSArray alloc] initWithArray:tempEmailArray];
        NSLog(@"Final Emails: %@", finalEmails);
        
        int squadTotal = 0;
        int squadTotalShots = 0;
        NSString *scoresString = @"";
        for(int x = 0; x < self.shooterShootObjectsArray.count; x++)
        {
            
            PFObject *temp = [PFObject objectWithClassName:@"ShootShooters"];
            temp = self.shooterShootObjectsArray[x];
            PFObject *tempShooter = [PFObject objectWithClassName:@"Shooter"];
            PFQuery *query3 = [PFQuery queryWithClassName:@"Shooter"];
            [query3 fromLocalDatastore];
            [query3 whereKey:@"appObjectId" equalTo:temp[@"shooterId"]];
            tempShooter = [query3 getFirstObject];
            squadTotalShots += [temp[@"totalShots"] intValue];
            squadTotal += [temp[@"totalHits"] intValue];
            
            if([self.storedShoot[@"handicapTypes"] isEqualToString:@"Handicaps"]){
                scoresString = [NSString stringWithFormat:@"%@%@ %@ Hit: %@/%@ - %@ yrds</br>", scoresString, tempShooter[@"fName"], tempShooter[@"lName"], temp[@"totalHits"], temp[@"totalShots"], temp[@"handicap"]];
            }
            else{
                
               scoresString = [NSString stringWithFormat:@"%@%@ %@ Hit: %@/%@</br>", scoresString, tempShooter[@"fName"], tempShooter[@"lName"], temp[@"totalHits"], temp[@"totalShots"]];
                
            }
            
        }
        
        //NSLog(@"Squad Total: %d", squadTotal);
        //NSLog(@"scoreString: %@", scoresString);
        
        MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
        picker.mailComposeDelegate = self;
        NSString *html = [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML"];
        [picker setSubject:self.titleInfoLabel.text];
        [picker setToRecipients: finalEmails];
        [picker setMessageBody:[NSString stringWithFormat:@"<h4><b>%@ %@ scored this shoot.</b></h4><p><b>Category: %@</b></p><h4><b>Squad Total: %d/%d Shots</b></h4><p><b>%@</b></p></br> %@", scoringCoach[@"fName"], scoringCoach[@"lName"], self.storedShoot[@"handicapType"], squadTotal, squadTotalShots, scoresString, html] isHTML:YES];
        
        // [self presentViewController:picker animated:YES completion:nil];
        
        if([MFMailComposeViewController canSendMail] == NO)
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"Your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if([self isNetworkAvailable] == NO){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"There is no network connection available to send emails currently." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else if([self isNetworkAvailable] == NO && [MFMailComposeViewController canSendMail]){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Error" message:@"These is no network connection and your email is not properly configured on your device." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
            [self presentViewController:picker animated:YES completion:nil];
        }
    }
}


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    [self dismissViewControllerAnimated:NO completion: ^{
        
        if(result == MFMailComposeResultSent){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Scores Sent" message:@"These scores were successfully sent." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
        else if(result == MFMailComposeResultFailed)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"These scores failed to send." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            
          //  NSLog(@"Canceled");
            
        }
    }];

}

            
-(BOOL) isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        //NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        //NSLog(@"-> connection established!\n");
        return YES;
    }
}


@end
