//
//  ScoringPage.m
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ScoringPage.h"

@interface ScoringPage ()

@end

@implementation ScoringPage
@synthesize tabBar;
@synthesize tableView;
@synthesize shooterPage;
@synthesize settingsPage;
@synthesize setupShootView;
@synthesize storedShootArray;
@synthesize shootShootersObjects;
@synthesize updatedData;
@synthesize spinner;
@synthesize shootToDelete;
@synthesize indexPathToDelete;
@synthesize card;
@synthesize managedContext;
@synthesize shootShootersToDelete;
@synthesize initialSelector;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:YES];
    
    
    self.tabBar.delegate = self;
    self.storedShootArray = [[NSMutableArray alloc] init];
    self.shootCoachesArray = [[NSMutableArray alloc] init];
    self.shootShootersArray = [[NSMutableArray alloc] init];
    self.shootShootersObjects = [[NSMutableArray alloc] init];
    self.shootShootersToDelete = [[NSMutableArray alloc] init];
    self.managedContext = [NSManagedObjectContext MR_contextForCurrentThread];

    //////NSLog(@"StoredShoots%@", self.storedShootArray);
    
    self.updatedData = [[UpdatedData alloc] init];
    self.updatedData.delegate = self;

    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if([userDefaults boolForKey:@"reload"] == YES){
        
        if([self isNetworkAvailable]){
        [[PFUser currentUser] fetch];
        [self.updatedData getShootsNew : YES : 0];
        [self.updatedData getShootersNew : YES];
        [self.updatedData getCoachesNew : YES];
        [self.updatedData getShootShootersNew : YES];
        [self.updatedData getTrapFieldsNew : YES];
        [self.updatedData getSubscriptionIdNew: YES];
        [userDefaults setBool:NO forKey:@"reload"];
        }
        else{
            
            [userDefaults setBool:NO forKey:@"reload"];
            [self.updatedData getShootsNew: NO : 0];
            
        }
        
    }
    else{
  
        [userDefaults setBool:NO forKey:@"reload"];
        [self.updatedData getShootsNew: NO : 0];
   
    
    }
 
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [userDefaults synchronize];
        
    });
  
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
    
    if(self.view.frame.size.width > 320)
    {
        [background setFrame: CGRectMake(0, 75, 768, 940)];
    }
    else
    {
         [background setFrame: CGRectMake(0, 50, 320, 510)];
    }
    [background setContentMode:UIViewContentModeScaleToFill];
    
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.shooterPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Team"];
    
    self.settingsPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Settings"];
    
    self.initialSelector =
    [storyboard instantiateViewControllerWithIdentifier:@"InitialSetupShoot"];

    self.setupShootView =
    [storyboard instantiateViewControllerWithIdentifier:@"setupShoot"];
    self.card =
    [storyboard instantiateViewControllerWithIdentifier:@"Card"];
    
    [self.tabBar setSelectedItem:[self.tabBar.items objectAtIndex: 1]];
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(self.view.frame.size.width/2.0, self.view.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    [self.view bringSubviewToFront:spinner];
    [spinner performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:YES];
    
    self.pageLabel = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 - 100, self.view.frame.size.height/2 - 30, 200, 60)];
    [self.pageLabel setHidden:YES];
    [self.pageLabel setBackgroundColor:[UIColor colorWithWhite:0.869 alpha:0.800]];
    [self.pageLabel.layer setCornerRadius:8.0f];
    [self.pageLabel.layer setBorderColor:[UIColor colorWithWhite:0.500 alpha:0.800].CGColor];
    [self.pageLabel.layer setBorderWidth:1.0];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, self.pageLabel.frame.size.height/2 - 15, self.pageLabel.frame.size.width, 30)];
    [label setText:@"Loading..."];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor blackColor]];
    [label setFont:[UIFont boldSystemFontOfSize:20]];
    [self.view addSubview:self.pageLabel];
    [self.view bringSubviewToFront:self.pageLabel];
    [self.pageLabel addSubview:label];
    
    self.loadingspinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.loadingspinner setCenter:CGPointMake(25, self.pageLabel.frame.size.height/2.0)]; // I do this because I'm in landscape mode
    [self.pageLabel addSubview:self.loadingspinner];
    [self.pageLabel bringSubviewToFront: self.loadingspinner];
    [self.loadingspinner startAnimating];
    
    
    
   
	// Do any additional setup after loading the view.
}

-(void) viewDidLoad {
   NSLog(@"ViewDidLoad");
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
   
    ODRefreshControl *refreshControl = [[ODRefreshControl alloc] initInScrollView:self.tableView];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [refreshControl setTintColor:[UIColor colorWithRed:(91/255.f) green:(192/255.f) blue:(232/255.f) alpha:1.0f]];
    [refreshControl setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];
    [refreshControl addTarget:self action:@selector(dropViewDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];

}

-(void) viewDidAppear:(BOOL)animated{
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dropViewDidBeginRefreshing:(ODRefreshControl *)refreshControl
{
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
   
    [self.tableView setUserInteractionEnabled:NO];
    if([self isNetworkAvailable]){
        
         [self.storedShootArray removeAllObjects];
        [self.updatedData getShootsNew: YES : 0];
    }
    else{
        
         [self.storedShootArray removeAllObjects];
          [self.updatedData getShootsNew: NO : 0];
    }

    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"reload"];
   
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [refreshControl endRefreshing];
    });
}

-(void) retrieveData{
    
    if (updatedData.delegateTag == 3) {
  
        [spinner stopAnimating];
    }

}


-(void) getMoreShoots: (int) ref{
    
    if(self.storedShootArray.count > 9){
        [self.pageLabel setHidden:NO];
        [self.updatedData getShootsNew:NO :ref];
    }
}


//Delegate for Parse Local
-(void) gotShoots: (NSArray *)shoots : (int) startingPoint{

    NSLog(@"Delegate Got Shoots: %@", shoots);
   
    [self.storedShootArray addObjectsFromArray:shoots];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"reload"] == NO){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getDataArrays : startingPoint];
        });
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [spinner stopAnimating];
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
        });        
    }
    
    if(self.storedShootArray.count < 1)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Shoots" message:@"It looks like you have not any scored shoots with your account. Please make sure that you have shooters added to your team and then click the \"Start New Shoot\" button." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
     [self.tableView setUserInteractionEnabled:YES];

}

-(void) gotShooters: (NSArray *)shoots{
    
   // NSLog(@"Delegate Got Shooters: %@", shoots);
}

-(void) gotFields: (NSArray *)fields{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getDataArrays: 0];
    });
   

    dispatch_async(dispatch_get_main_queue(), ^{
        [spinner stopAnimating];
           [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    });
    
    
}
-(void) gotShootShooters:(NSArray *) shootShooters{

   // NSLog(@"Delegate Got ShootShooters: %@", shootShooters);
}

-(void) gotSubscription: (NSArray *) subscription {
    
    // NSLog(@"Delegate Got Subscription: %@", subscription);
}

-(void) gotCoaches: (NSArray *) coaches {
    
     NSLog(@"Delegate Got Coaches: %@", coaches);
}

-(void) getDataArrays : (int) startingPoint {
    
    NSMutableArray *finalShooters;
    //Get Shooters for each shoot
    
    for(int x = startingPoint; x < self.storedShootArray.count; x++){
        
        PFObject *storedShootTemp = [PFObject objectWithClassName:@"Shoot"];
        storedShootTemp = [self.storedShootArray objectAtIndex:x];
        
        PFObject *coachTemp = [PFObject objectWithClassName:@"Coach"];
        PFQuery *query1 = [PFQuery queryWithClassName:@"Coach"];
        [query1 fromLocalDatastore];
        [query1 whereKey:@"appObjectId" equalTo:storedShootTemp[@"coachID"]];
        coachTemp = [query1 getFirstObject];
        if(coachTemp){
        [self.shootCoachesArray addObject:coachTemp];
        }
        
        NSMutableArray *shootShootersTempArray =[[NSMutableArray alloc] init];
        PFQuery *query = [PFQuery queryWithClassName:@"ShootShooters"];
        [query fromLocalDatastore];
        [query whereKey:@"shootID" equalTo: storedShootTemp[@"appObjectId"]];
        [query orderByAscending:@"startingPost"];
    
        shootShootersTempArray = [[query findObjects] mutableCopy];
       // NSLog(@"Shoot Shooters Temp: %@", shootShootersTempArray);
        [self.shootShootersObjects addObject:shootShootersTempArray];
        finalShooters = [[NSMutableArray alloc] init];
        
        for(int i = 0; i < shootShootersTempArray.count; i++){
            PFObject *shootShooter = [PFObject objectWithClassName:@"ShootShooters"];
            shootShooter = [shootShootersTempArray objectAtIndex:i];
       
            PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
            [query fromLocalDatastore];
            [query whereKey:@"appObjectId" equalTo: shootShooter[@"shooterId"]];
            PFObject *shooter = [PFObject objectWithClassName:@"Shooter"];
            shooter = [query getFirstObject];
            
            if(shooter)
            {
                [finalShooters addObject:shooter];
            }
        
        }
        
       [self.shootShootersArray addObject:finalShooters];

    }

   [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    [self.pageLabel setHidden:YES];


}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    
    return [storedShootArray count];
}


- (void)tableView:(UITableView *)tableView1 commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [self.tableView setEditing:NO animated:YES];
        
        if([self isNetworkAvailable]){
        
            self.shootToDelete = [self.storedShootArray objectAtIndex:indexPath.row];
            
            PFQuery *query = [PFQuery queryWithClassName:@"ShootShooters"];
            [query fromLocalDatastore];
            [query whereKey:@"shootID" containsString:self.shootToDelete[@"appObjectId"]];
            self.shootShootersToDelete = [[query findObjects] mutableCopy];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Delete Shoot" message:@"This will delete all of the data for this shoot. Are you sure you want to delete it?" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Cancel", @"DELETE", nil];
            [alert show];
            alert.tag = 1;
            self.indexPathToDelete = [[NSIndexPath alloc] init];
            self.indexPathToDelete = indexPath;
            ////NSLog(@"IndexPathToDelete: %d", indexPathToDelete.row);
        
        }
        else{
            
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Issue" message:@"You must be connected to the internet to delete a shoot." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert show];
            
        }
    
    }
}



- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView
{
    NSLog(@"HERE");
    NSArray *visibleRows = [self.tableView visibleCells];
    UITableViewCell *lastVisibleCell = [visibleRows lastObject];
    NSIndexPath *path = [self.tableView indexPathForCell:lastVisibleCell];
    if(path.row == self.storedShootArray.count - 1)
    {
        // Do something here
        NSLog(@"end of the table");
        
        if([lastVisibleCell editingStyle] != UITableViewCellEditingStyleDelete){
        
        [self getMoreShoots: (int)self.storedShootArray.count];
        
        }
        //[self.getDataObject getLifeStreamUpdates: (int)(self.lifestreamArray.count - 1)];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
   UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }

    
    [cell setBackgroundColor:[UIColor clearColor]];
   /////////////////////
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 9, 118, 25)];
    [dateLabel setFont:[UIFont boldSystemFontOfSize:20]];
    [dateLabel setTextColor:[UIColor colorWithRed:(105/255.f) green: (138/255.f) blue: (51/255.f) alpha:1.0f]];
    [dateLabel setTextAlignment:NSTextAlignmentCenter];
    PFObject *temp = [PFObject objectWithClassName:@"Shoot"];
    temp = [self.storedShootArray objectAtIndex:indexPath.row];
    
    PFObject *tempCoach = [PFObject objectWithClassName:@"Coach"];
    PFQuery *query = [PFQuery queryWithClassName:@"Coach"];
    [query whereKey:@"appObjectId" containsString: temp[@"coachID"]];
    [query fromLocalDatastore];
    tempCoach = [query getFirstObject];

    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    dateString = [formatter stringFromDate:temp.createdAt];
    dateLabel.text = dateString;
    
    /////////////////////
    UIImageView *backgroundTop = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"BlackBox.png"]];
    [backgroundTop setFrame: CGRectMake(120, 2, 201, 92)];
    [backgroundTop setContentMode:UIViewContentModeScaleToFill];

    /////////////////////
    UILabel *pracOrComp = [[UILabel alloc] initWithFrame:CGRectMake(130 , 6, 50, 25)];
    [pracOrComp setFont:[UIFont boldSystemFontOfSize:16]];
    [pracOrComp setTextColor:[UIColor darkGrayColor]];
    pracOrComp.text = @"Type: ";
   
    UILabel *pracOrCompText = [[UILabel alloc] initWithFrame:CGRectMake(173 , 6, 100, 25)];
    [pracOrCompText setFont:[UIFont systemFontOfSize:16]];
    [pracOrCompText setTextColor:[UIColor darkGrayColor]];
    pracOrCompText.text = [NSString stringWithFormat:@"%@", temp[@"typeShoot"]];

    /////////////////////
    UILabel *trapRange = [[UILabel alloc] initWithFrame:CGRectMake(12, 34, 115, 50)];
    [trapRange setTextColor:[UIColor darkGrayColor]];
    [trapRange setFont:[UIFont boldSystemFontOfSize:16]];
    [trapRange setLineBreakMode:NSLineBreakByWordWrapping];
    [trapRange setTextColor:[UIColor colorWithRed:(87/255.f) green:(170/255.f) blue:(244/255.f) alpha:1.0f]];
    
    trapRange.numberOfLines = 2;
    NSString *trapFieldString;
    if([temp[@"trapFieldId"] isEqualToString:@"NA"]){
        
        trapFieldString = @"No Range Recorded";
    }
    else{
        PFObject *trapField = [PFObject objectWithClassName:@"TrapField"];
        PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
        [query fromLocalDatastore];
        trapField = [query getObjectWithId:temp[@"trapFieldId"]];
        trapFieldString = trapField[@"Name"];
    }
    [trapRange setText:trapFieldString];
    

    //////////////////
    UILabel *houseNum = [[UILabel alloc] initWithFrame:CGRectMake(130, 44, 60, 25)];
    [houseNum setFont:[UIFont boldSystemFontOfSize:16]];
    [houseNum setTextColor:[UIColor darkGrayColor]];
    houseNum.text = @"House:";
    
    UILabel *houseNumText = [[UILabel alloc] initWithFrame:CGRectMake(184, 44, 170, 25)];
    [houseNumText setFont:[UIFont systemFontOfSize:16]];
    [houseNumText setTextColor:[UIColor darkGrayColor]];
    houseNumText.text = [NSString stringWithFormat:@"#%@", temp[@"trapHouseNumber"]];
    
    //////////////////
    UILabel *category = [[UILabel alloc] initWithFrame:CGRectMake(130, 64, 76, 25)];
    [category setFont:[UIFont boldSystemFontOfSize:16]];
    [category setTextColor:[UIColor darkGrayColor]];
    category.text = @"Category:";
    
    UILabel *handicapText = [[UILabel alloc] initWithFrame:CGRectMake(206, 64, 170, 25)];
    [handicapText setFont:[UIFont systemFontOfSize:16]];
    [handicapText setTextColor:[UIColor darkGrayColor]];
    if([temp[@"handicapType"] isEqualToString:@"Handicaps"]){
        
        handicapText.text = [NSString stringWithFormat:@"%@", temp[@"handicapType"]];
        
    }
    else{
        
        handicapText.text = @"Singles";
    }

    //////////////////////
  
    
   /* if(self.shootCoachesArray.count > 0){
        if([self.shootCoachesArray objectAtIndex:indexPath.row]){
            
            tempCoach = [self.shootCoachesArray objectAtIndex:indexPath.row];
        }
    }*/
    
      NSLog(@"Coache: %@", tempCoach);
    
    
    UILabel *coachLabel = [[UILabel alloc] initWithFrame:CGRectMake(130, 25, 60, 25)];
    [coachLabel setFont:[UIFont boldSystemFontOfSize:16]];
    [coachLabel setTextColor:[UIColor darkGrayColor]];
    [coachLabel setText:@"Coach: "];
    
    UILabel *coachLabelText = [[UILabel alloc] initWithFrame:CGRectMake(184, 25, 160, 25)];
    [coachLabelText setFont:[UIFont systemFontOfSize:16]];
    [coachLabelText setTextColor:[UIColor darkGrayColor]];

    if([tempCoach[@"fName"] length] > 0){
        [coachLabelText setText:[NSString stringWithFormat:@"%@ %@", tempCoach[@"fName"], tempCoach[@"lName"]]];
    }
    else{
        
        [coachLabelText setText:[NSString stringWithFormat:@"%@", @"Coach"]];
    }
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iTrappRoundedRectangleBig.png"]];
    [background setFrame: CGRectMake(5, 90, 310, 135)];
    [background setContentMode:UIViewContentModeScaleToFill];

    UITextView *shootersTextView = [[UITextView alloc] initWithFrame:CGRectMake(6, 80, 308, 145)];
    [shootersTextView setTextAlignment:NSTextAlignmentCenter];
    [shootersTextView setFont:[UIFont systemFontOfSize:16]];
    [shootersTextView setUserInteractionEnabled:NO];
    [shootersTextView.layer setMasksToBounds:YES];
    [shootersTextView setBackgroundColor:[UIColor clearColor]];
    
    NSMutableArray *tempShootersArray;
    NSMutableArray *tempShootShooter ;
    if(self.shootShootersArray.count > 0){
        tempShootersArray = [self.shootShootersArray objectAtIndex: indexPath.row];
    }
    
    if(self.shootShootersObjects.count >  0){
        
          tempShootShooter = [self.shootShootersObjects objectAtIndex:indexPath.row];
    }
    
     NSString *tempShooters = [[NSString alloc] init];
    for(int x = 0; x < tempShootersArray.count; x++){
     
       // NSLog(@"TEMP SHOOTER SET UP: %d", x);
        
        PFObject *shootShooter = [PFObject objectWithClassName:@"ShootShooters"];
        shootShooter = [tempShootShooter objectAtIndex:x];
        
        PFObject *tempShooter = [PFObject objectWithClassName:@"Shooter"];
        PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
        [query fromLocalDatastore];
        [query whereKey:@"appObjectId" equalTo:shootShooter[@"shooterId"]];
        tempShooter = [query getFirstObject];
       
        if([temp[@"handicapType"] isEqualToString:@"Handicaps"])
        {
           
            tempShooters = [NSString stringWithFormat:@"%@\n%@ %@: %@/%@ Hit - %d yrds", tempShooters, tempShooter[@"fName"], tempShooter[@"lName"], shootShooter[@"totalHits"], shootShooter[@"totalShots"], [shootShooter[@"handicap"] intValue]];
            // NSLog(@"TempShooters: %@", tempShooters);
            [shootersTextView setText:tempShooters];
        }
        else{
            
            tempShooters = [NSString stringWithFormat:@"%@\n%@ %@: %@/%@ Hits", tempShooters, tempShooter[@"fName"], tempShooter[@"lName"], shootShooter[@"totalHits"], shootShooter[@"totalShots"]];
            [shootersTextView setText:tempShooters];
            
        }
        
    }
    
    [cell addSubview:houseNumText];
    [cell addSubview:handicapText];
    [cell addSubview:coachLabelText];
    [cell addSubview:pracOrCompText];
    [cell addSubview:backgroundTop];
    [cell sendSubviewToBack:backgroundTop];
    [cell addSubview:background];
    [cell sendSubviewToBack:background];
    [cell addSubview:pracOrComp];
    [cell addSubview:shootersTextView];
    [cell addSubview:category];
    [cell addSubview:coachLabel];
    [cell addSubview:trapRange];
    [cell addSubview:houseNum];
    [cell addSubview:dateLabel];

    return cell;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    
    
    if(item.tag == 1){
        
        [self presentViewController:shooterPage animated:YES completion:nil];
        
    }
    else if(item.tag == 3){
        
        [self presentViewController:settingsPage animated:YES completion:nil];
        
    }
    else{
        
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // ////NSLog(@"IndexSelected: %d", indexPath.row);
    [self presentViewController:self.card animated:YES completion:nil];
   
    NSLog(@"ShootShooters: %@", self.shootShootersObjects[indexPath.row]);
    [self.card showScoreCard: (int)indexPath.row : self.shootShootersArray[indexPath.row] : self.shootShootersObjects[indexPath.row] : storedShootArray[indexPath.row] ];
    


}

-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        ////NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        ////NSLog(@"-> connection established!\n");
        return YES;
    }
}

- (IBAction)setUpNewShoot:(id)sender {
    
    [self presentViewController: initialSelector animated:YES completion:nil];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag > -1) {
        
        if(buttonIndex == 1)
        {
            NSArray *deleteRowsArray = [[NSArray alloc] initWithObjects: self.indexPathToDelete, nil];
    
            PFQuery *shooterQuery = [PFQuery queryWithClassName:@"ShootShooters"];
            [shooterQuery whereKey:@"shootID" equalTo:self.shootToDelete[@"appObjectId"]];
            [shooterQuery fromLocalDatastore];
            [shooterQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                if(objects){
                    
                    for(int x = 0; x < objects.count; x ++){
                        
                        [[objects objectAtIndex:x] unpin];
                        [[objects objectAtIndex:x] deleteEventually];
                        
                    }
                }
            }];
            
            [self.shootToDelete unpin];
            [self.shootToDelete deleteEventually];
                    
                    [self.storedShootArray removeObjectAtIndex:self.indexPathToDelete.row];
                    [self.shootShootersArray removeObjectAtIndex:self.indexPathToDelete.row];
                    [self.shootShootersObjects removeObjectAtIndex:self.indexPathToDelete.row];
                    [self.tableView deleteRowsAtIndexPaths:deleteRowsArray withRowAnimation:UITableViewRowAnimationRight];
        
            
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        }
      
       
    }
    else{
        
        [self.tableView setEditing:NO animated:YES];
        
    }
    
    
    
}

@end
