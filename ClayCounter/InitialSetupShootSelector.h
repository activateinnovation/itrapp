//
//  InitialSetupShootSelector.h
//  ClayCounter
//
//  Created by Test on 7/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetupShootView.h"
#import "ScoringPage.h"


@class SetupShootView;
@class ScoringPage;
@interface InitialSetupShootSelector : UIViewController

- (IBAction)singlesClicked:(id)sender;
- (IBAction)handicapsClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)doublesClicked:(id)sender;
@property (strong, nonatomic) SetupShootView *setupShootView;
@property (strong, nonatomic) ScoringPage *scoringPage;

@end
