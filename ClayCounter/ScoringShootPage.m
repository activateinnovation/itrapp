//
//  ScoringShootPage.m
//  ClayCounter
//
//  Created by Test on 2/3/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "ScoringShootPage.h"

@interface ScoringShootPage ()

@end

@implementation ScoringShootPage
@synthesize shootObject;
@synthesize tableView;
@synthesize leftPlusButton;
@synthesize leftButton;
@synthesize straightButton;
@synthesize rightButton;
@synthesize rightPlusButton;
@synthesize hitButton;
@synthesize counter;
@synthesize setUpShoot;
@synthesize currentShoot;
@synthesize managedObjectContext;
@synthesize coach;
@synthesize testButton;
@synthesize shotsCounter;
@synthesize shooterIndex;
@synthesize shooterShootArray;
@synthesize roundNumber;
@synthesize endAlert;
@synthesize tempShootId;
@synthesize previousShooterIndex;
@synthesize modifyScoreTag;
@synthesize undoButton;
@synthesize startingPostNumbers;
@synthesize slashButton;
@synthesize oButton;
@synthesize handicapValues;
@synthesize initialSetUpShoot;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) viewWillAppear:(BOOL)animated{

    
    self.coachId = [[NSString alloc] init];
    self.shooterShootArray = [[NSMutableArray alloc] init];
    self.shootersArray = [[NSMutableArray alloc] init];
    self.currentShoot = [PFObject objectWithClassName:@"CurrentShoot"];
    PFQuery *query = [PFQuery queryWithClassName:@"CurrentShoot"];
    [query fromLocalDatastore];
    [query orderByAscending:@"createdAt"];
    self.currentShoot = [query getFirstObject];
    NSLog(@"Current Shoot: %@", self.currentShoot);
    
    self.handicapValues = [[currentShoot[@"handicapString"] componentsSeparatedByString:@","] mutableCopy];
    
    if([currentShoot[@"typeShoot"] isEqualToString:@"Handicaps"]){
        
        for(int x = 0; x < handicapValues.count; x++){
            
            if([self.handicapValues[x] intValue] > 0){
            }
            else{
                
                //NSLog(@"O");
                [self.handicapValues removeObject:[self.handicapValues objectAtIndex:x]];
            }
            
        }
    }
    
    self.shotsCounter = 0;
    self.shooterIndex = 0;
    self.previousShooterIndex = 0;
    self.roundNumber = 0;
    self.modifyScoreTag = -1;
    self.tempShootId = [[NSString alloc] init];
    
    self.slashButton.hidden = YES;
    self.oButton.hidden = YES;

    self.coach = [PFObject objectWithClassName:@"Coach"];
    PFQuery *coachQuery = [PFQuery queryWithClassName:@"Coach"];
    [coachQuery fromLocalDatastore];
    [coachQuery whereKey:@"appObjectId" containsString:self.currentShoot[@"coach"]];
    self.coach = [coachQuery getFirstObject];
    

    self.coachId = self.coach[@"appObjectId"];
 // NSLog(@"CoachObjectId: %@", self.coach@"app);
    self.startingPostNumbers = [[NSMutableArray alloc] init];
    NSArray *temp = [self.currentShoot[@"startingPost"] componentsSeparatedByString:@","];
    self.startingPostNumbers = [NSMutableArray arrayWithArray:temp];
    

    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
    //If IPAD SCreen
    if(self.view.frame.size.width > 320)
    {
        [background setFrame: CGRectMake(0, 50, 780, 900)];
    }
    else //IF IPOD SCREEN
    {
        [background setFrame: CGRectMake(0, 60, 320, 550)];
    }
    [background setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];

    
    if([self.coach[@"leftRightHanded"] isEqualToString:@"Left"]){
        
       // //NSLog(@"Left");
        if(self.view.frame.size.width > 320){
            
            self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(60, 61, (self.view.frame.size.width - 60), self.view.frame.size.height)];
          
            if([self.currentShoot[@"scoringType"] isEqualToString:@"Advanced"]){
                
                self.slashButton.hidden = YES;
                self.oButton.hidden = YES;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = NO;
                self.leftPlusButton.hidden = NO;
                self.hitButton.hidden = NO;
                self.straightButton.hidden = NO;
                self.rightButton.hidden = NO;
                self.rightPlusButton.hidden = NO;
            
            [self.undoButton setFrame:CGRectMake(0, 246, 65, 60)];
            [self.leftPlusButton setFrame:CGRectMake(0, 309, 65, 60)];
            [self.leftButton setFrame:CGRectMake(0, 372, 65, 60)];
            [self.hitButton setFrame:CGRectMake(0, 440, 65, 115)];
            [self.straightButton setFrame:CGRectMake(0, 563, 65, 60)];
            [self.rightButton setFrame:CGRectMake(0, 626, 65, 60)];
                [self.rightPlusButton setFrame:CGRectMake(0, 689, 65, 60)];
            }
            else{
                
                self.slashButton.hidden = NO;
                self.oButton.hidden = NO;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = YES;
                self.leftPlusButton.hidden = YES;
                self.hitButton.hidden = YES;
                self.straightButton.hidden = YES;
                self.rightButton.hidden = YES;
                self.rightPlusButton.hidden = YES;
                
                [self.undoButton setFrame:CGRectMake(0, 246, 65, 60)];
                [self.slashButton setFrame:CGRectMake(0, 400, 65, 115)];
                [self.oButton setFrame:CGRectMake(0, 523, 65, 115)];
            }
            
        }
        else{
       
            self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(60, 61, 260, 504)];
      
            if([self.currentShoot[@"scoringType"] isEqualToString:@"Advanced"]){
                
                self.slashButton.hidden = YES;
                self.oButton.hidden = YES;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = NO;
                self.leftPlusButton.hidden = NO;
                self.hitButton.hidden = NO;
                self.straightButton.hidden = NO;
                self.rightButton.hidden = NO;
                self.rightPlusButton.hidden = NO;
                
            [self.undoButton setFrame:CGRectMake(0, 60, 65, 70)];
            [self.leftPlusButton setFrame:CGRectMake(0, 123, 65, 70)];
            [self.leftButton setFrame:CGRectMake(0, 186, 65, 70)];
            [self.hitButton setFrame:CGRectMake(0, 253, 65, 125)];
            [self.straightButton setFrame:CGRectMake(0, 377, 65, 70)];
            [self.rightButton setFrame:CGRectMake(0, 440, 65, 70)];
            [self.rightPlusButton setFrame:CGRectMake(0, 503, 65, 70)];
            }
            else{
                
                
                self.slashButton.hidden = NO;
                self.oButton.hidden = NO;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = YES;
                self.leftPlusButton.hidden = YES;
                self.hitButton.hidden = YES;
                self.straightButton.hidden = YES;
                self.rightButton.hidden = YES;
                self.rightPlusButton.hidden = YES;
                
                [self.undoButton setFrame:CGRectMake(0, 60, 65, 70)];
                
                [self.slashButton setFrame:CGRectMake(0, 213, 65, 125)];
                [self.oButton setFrame:CGRectMake(0, 337, 65, 115)];
            }
        }

    }
    else if([self.coach[@"leftRightHanded"] isEqualToString:@"Right"]){
        
        
       // //NSLog(@"Right");
        if(self.view.frame.size.width > 320){
            
            self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 61, (self.view.frame.size.width - 60), self.view.frame.size.height)];
            
            if([self.currentShoot[@"scoringType"] isEqualToString:@"Advanced"]){
                
                self.slashButton.hidden = YES;
                self.oButton.hidden = YES;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = NO;
                self.leftPlusButton.hidden = NO;
                self.hitButton.hidden = NO;
                self.straightButton.hidden = NO;
                self.rightButton.hidden = NO;
                self.rightPlusButton.hidden = NO;
     
            [self.undoButton setFrame:CGRectMake(self.tableView.frame.size.width, 246, 65, 70)];
            [self.leftPlusButton setFrame:CGRectMake(self.tableView.frame.size.width, 309, 65, 70)];
            [self.leftButton setFrame:CGRectMake(self.tableView.frame.size.width, 372, 65, 70)];
            [self.hitButton setFrame:CGRectMake(self.tableView.frame.size.width, 440, 65, 115)];
            [self.straightButton setFrame:CGRectMake(self.tableView.frame.size.width, 563, 65, 70)];
            [self.rightButton setFrame:CGRectMake(self.tableView.frame.size.width, 626, 65, 70)];
            [self.rightPlusButton setFrame:CGRectMake(self.tableView.frame.size.width, 689, 65, 70)];
                
            }
            else{
                
                self.slashButton.hidden = NO;
                self.oButton.hidden = NO;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = YES;
                self.leftPlusButton.hidden = YES;
                self.hitButton.hidden = YES;
                self.straightButton.hidden = YES;
                self.rightButton.hidden = YES;
                self.rightPlusButton.hidden = YES;

                
                [self.undoButton setFrame:CGRectMake(self.tableView.frame.size.width, 246, 65, 70)];
                
                [self.slashButton setFrame:CGRectMake(self.tableView.frame.size.width, 400, 65, 115)];
                [self.oButton setFrame:CGRectMake(self.tableView.frame.size.width, 523, 65, 115)];
                
                
            }
            
        }
        else{
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 61, 260, 504)];
            
            
            if([self.currentShoot[@"scoringType"] isEqualToString:@"Advanced"]){
                
                self.slashButton.hidden = YES;
                self.oButton.hidden = YES;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = NO;
                self.leftPlusButton.hidden = NO;
                self.hitButton.hidden = NO;
                self.straightButton.hidden = NO;
                self.rightButton.hidden = NO;
                self.rightPlusButton.hidden = NO;
        
            [self.undoButton setFrame:CGRectMake(258, 60, 65, 70)];
            [self.leftPlusButton setFrame:CGRectMake(258, 123, 65, 70)];
            [self.leftButton setFrame:CGRectMake(258, 186, 65, 70)];
            [self.hitButton setFrame:CGRectMake(258, 253, 65, 125)];
            [self.straightButton setFrame:CGRectMake(258, 377, 65, 70)];
            [self.rightButton setFrame:CGRectMake(258, 440, 65, 70)];
            [self.rightPlusButton setFrame:CGRectMake(258, 503, 65, 70)];
                
            }
            else{
                
                self.slashButton.hidden = NO;
                self.oButton.hidden = NO;
                self.undoButton.hidden = NO;
                self.leftButton.hidden = YES;
                self.leftPlusButton.hidden = YES;
                self.hitButton.hidden = YES;
                self.straightButton.hidden = YES;
                self.rightButton.hidden = YES;
                self.rightPlusButton.hidden = YES;

                [self.undoButton setFrame:CGRectMake(258, 60, 65, 70)];
                [self.slashButton setFrame:CGRectMake(258, 213, 65, 125)];
                [self.oButton setFrame:CGRectMake(258, 337, 65, 115)];
                
                
            }
        }

    }
    
    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
     tempArray = [[self.currentShoot[@"shooters"] componentsSeparatedByString:@","] mutableCopy];
    ////NSLog(@"tempArray%@", tempArray);

    
    //this is where the starting post number gets set in the view for ecah shooter
    for(int x = 0; x < tempArray.count; x++){
        
        PFObject *shooter = [PFObject objectWithClassName:@"Shooter"];
        PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
        [query fromLocalDatastore];
        [query whereKey:@"appObjectId" containsString:[tempArray objectAtIndex:x]];
        shooter = [query getFirstObject];//Shooter MR_findFirstByAttribute:@"objectId" withValue:[tempArray objectAtIndex:x]];
        NSMutableArray *tempArray = [[NSMutableArray alloc] init];
        
        ShooterShootObject *shooterShootObject = [[ShooterShootObject alloc] initWithItems:shooter : [[self.startingPostNumbers objectAtIndex:x] intValue] : 0 : 0 : tempArray : 0 : 0 : [self.handicapValues objectAtIndex: x]];
        
        [self.shooterShootArray addObject:shooterShootObject];
        
    }
    
    
    [self.tableView setBackgroundColor:[UIColor clearColor]];
    
    [self.view addSubview:self.undoButton];
    [self.view addSubview:self.leftPlusButton];
    [self.view addSubview:self.leftButton];
    [self.view addSubview:self.straightButton];
    [self.view addSubview:self.rightPlusButton];
    [self.view addSubview:self.rightButton];
    [self.view addSubview:self.hitButton];
    [self.view addSubview:self.slashButton];
    [self.view addSubview:self.oButton];
    
    
    [self.view addSubview:self.tableView];
   

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     //[self.view addSubview:self.tableView];

    //[self.tableView reloadData];
   
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    self.setUpShoot =
    [storyboard instantiateViewControllerWithIdentifier:@"setupShoot"];
    
    self.initialSetUpShoot =
    [storyboard instantiateViewControllerWithIdentifier:@"InitialSetupShoot"];
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
   // //NSLog(@"NumberOfRows");
    return [self.shooterShootArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 170.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    CustomShootCell *cell = [self.tableView dequeueReusableCellWithIdentifier:nil];
    ShooterShootObject *shooterShootObject = [self.shooterShootArray objectAtIndex:indexPath.row];
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    BOOL isIndex;
    if(indexPath.row == self.shooterIndex){
        
        isIndex = YES;
        
    }
    else{
        
        isIndex = NO;
    }
    
    //NSLog(@"Shooter Index: %hhd", isIndex);
    
    if (cell == nil) {
        
        
        ////NSLog(@"TotatlShots: %@", self.currentShoot.totalShots);
        cell = [[CustomShootCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier : [NSString stringWithFormat:@"%@", self.currentShoot[@"totalShots"]] : [NSString stringWithFormat:@"%d", shooterShootObject.shotCounter] : [NSString stringWithFormat:@"%d", shooterShootObject.postNumber] : [NSString stringWithFormat:@"%@ %@", shooterShootObject.shooter[@"fName"], shooterShootObject.shooter[@"lName"]]: self.currentShoot[@"shotsAtPost"] :  shooterShootObject.hitNumber : shooterShootObject.scoreArray: shooterShootObject.roundNumber: shooterShootObject.shotsFiredInRound : self.shooterShootArray: isIndex];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
       
    }
    
  
    cell.delegate = self;
    cell.tag = indexPath.row;
   
   // //NSLog(@"ShooterIndex: %d", shooterIndex);
    
    if(indexPath.row == self.shooterIndex){
        
        [cell setBackgroundColor:[UIColor colorWithRed:(87/255.f) green:(196/255.f) blue:(234/255.f) alpha:0.25f]];
        //[cell setBackgroundColor: [self colorWithHexString:@"D0D0D0"]];
        
              // method to set highlighted Cell
    }
    
    
    if(self.shotsCounter == 1)
    {
        self.undoButton.enabled = NO;
    
    }else if(shooterIndex == 0 && (cell.shotsFiredInRound == cell.shotsAtEachPost || cell.shotsFiredInRound == 0)){
        ////NSLog(@"HEREYeah");
            self.undoButton.enabled = NO;
            
    }else{
        
          self.undoButton.enabled = YES;
    }
    
    return cell;
    
}

-(void) viewDidDisappear:(BOOL)animated{
    
    [self.leftPlusButton removeFromSuperview];
    [self.leftButton removeFromSuperview];
    [self.straightButton removeFromSuperview];
    [self.rightButton removeFromSuperview];
    [self.rightPlusButton removeFromSuperview];
    [self.leftPlusButton removeFromSuperview];
    [self.hitButton removeFromSuperview];
    self.coach = nil;
    
    
}


- (IBAction)cancelShoot:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel Shoot" message:@"Are you sure you want to cancel your current shoot?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"Resume", nil];
    alert.tag = 3;
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
}


-(IBAction)scoringButtonCLicked:(id)sender{
    
    
    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:NO];
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:self.shooterIndex inSection:0];
    CustomShootCell *cell = (CustomShootCell *)[self.tableView cellForRowAtIndexPath: index];

    ShooterShootObject *shooterShootTemp = [self.shooterShootArray objectAtIndex:self.shooterIndex];
   
    
    if(modifyScoreTag > -1){
        
        
    
    }else{
        
        if((shooterShootTemp.shotsFiredInRound -1) == cell.shotsAtEachPost ){
      
        if(shooterShootTemp.postNumber == 5){
            
            shooterShootTemp.postNumber = 1;
            
        }
        else{

            shooterShootTemp.postNumber++;
        
        }
        //shooterShootTemp.roundNumber++;
        shooterShootTemp.shotsFiredInRound = 0;
        shooterShootTemp.hitsInRound = 0;
        for (int x = 0; x < cell.tempScores.count; x++){
                
                [cell.tempScores replaceObjectAtIndex:x withObject:@""];
                
        }
    }
        
        
    }

    
    if(sender == self.leftPlusButton){
    
        if(self.modifyScoreTag > -1){
            //NEED TO CHECK FOR ROUND NUMBER
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"L+"];
             self.shotsCounter++;
            
        }else{
        
            [shooterShootTemp.scoreArray addObject:@"L+"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
       
        }
     //   [self.tableView reloadData];
        
    }else if (sender == self.leftButton){
    
        if(self.modifyScoreTag > -1){
            
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"L"];
             self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"L"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
      // [self.tableView reloadData];
    
    }else if(sender == self.straightButton){
   
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"S"];
             self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"S"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
    //   [self.tableView reloadData];
        
    }else if(sender == self.rightButton){
       
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"R"];
             self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"R"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
     //   [self.tableView reloadData];
        
    }else if(sender == self.rightPlusButton){

        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"R+"];
             self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"R+"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
     //  [self.tableView reloadData];
        
    }else if(sender == self.oButton){
     
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex:self.modifyScoreTag withObject:@"O"];
             self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"O"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
        //  [self.tableView reloadData];
        
    }
    else if(sender == self.hitButton){
  
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex: self.modifyScoreTag withObject:@"H"];
             self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"H"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
        shooterShootTemp.hitNumber++;
        shooterShootTemp.hitsInRound++;
       
    //    [self.tableView reloadData];
        
    }else if(sender == self.slashButton){
      
        if(self.modifyScoreTag > -1){
            
            [shooterShootTemp.scoreArray replaceObjectAtIndex: self.modifyScoreTag withObject:@"/"];
               self.shotsCounter++;
            
        }else{
            
            [shooterShootTemp.scoreArray addObject:@"/"];
            shooterShootTemp.shotCounter++;
            self.shotsCounter++;
            cell.shotsFired++;
            shooterShootTemp.shotsFiredInRound++;
            
        }
        shooterShootTemp.hitNumber++;
        shooterShootTemp.hitsInRound++;
        
        //    [self.tableView reloadData];
        
    }

    else{
        
        
    }
    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:NO];
 
 
   // //NSLog(@"Shooter index: %d", self.shooterIndex);
    
    
    int temp = (int)[self.shooterShootArray count];
    if(cell.roundNumber == 0){
        NSLog(@"Shots Counter: %d", self.shotsCounter);
        //Ends round Shoot is only 10 total shots with 10 shots at each post
        if(self.shotsCounter == [self.shooterShootArray count]  * [self.currentShoot[@"totalShots"] intValue]){
            
            //ENDS THE ROUND
            // if((self.shotsCounter - 1) == [self.shooterShootArray count]  * [self.currentShoot.totalShots intValue]) {
            
            NSString *tempHits = [[NSString alloc] init];
            int squadTotal = 0;
            
            
            for (int x = 0; x < shooterShootArray.count; x++) {
                ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                //  //NSLog(@"Hit Number: %d", shootTemp.hitNumber);
                tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits/ %d Total", tempHits ,shootTemp.shooter[@"fName"], shootTemp.hitNumber, [currentShoot[@"totalShots"] intValue]];
                
                squadTotal += shootTemp.hitNumber;
            }
            
            tempHits = [NSString stringWithFormat:@"%@\nSquad Total: %d", tempHits, squadTotal];
            
            self.endAlert = [[UIAlertView alloc] initWithTitle:@"End of Shoot Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"MODIFY" otherButtonTitles: @"APPROVE", nil];
            [self.endAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            
            
            
            
            
        }  //Ends round 1 normal
        else if(self.shotsCounter == (cell.shotsAtEachPost * temp)){
            
            NSString *tempHits = [[NSString alloc] init];
            
            for (int x = 0; x < shooterShootArray.count; x++) {
                ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits",tempHits, shootTemp.shooter[@"fName"], shootTemp.hitsInRound];
                
            }
         
            
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"MODIFY" otherButtonTitles:@"APPROVE", nil];
           
            [self enableOrDisableShootButtons:NO];
            
            alert.tag = 1;
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            
            
        }
        else{
            
            
            
        }
        
    }
    else if(self.shotsCounter == [self.shooterShootArray count]  * [self.currentShoot[@"totalShots"] intValue]){
        
    
        //ENDS THE ROUND
       // if((self.shotsCounter - 1) == [self.shooterShootArray count]  * [self.currentShoot.totalShots intValue]) {
            
            NSString *tempHits = [[NSString alloc] init];
            int squadTotal = 0;
            
            
            for (int x = 0; x < shooterShootArray.count; x++) {
                ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                //  //NSLog(@"Hit Number: %d", shootTemp.hitNumber);
                tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits/ %d Total", tempHits ,shootTemp.shooter[@"fName"], shootTemp.hitNumber, [currentShoot[@"totalShots"] intValue]];
                
                squadTotal += shootTemp.hitNumber;
            }
            
            tempHits = [NSString stringWithFormat:@"%@\nSquad Total: %d", tempHits, squadTotal];
            
            self.endAlert = [[UIAlertView alloc] initWithTitle:@"End of Shoot Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"MODIFY" otherButtonTitles: @"APPROVE", nil];
         [self enableOrDisableShootButtons:NO];
            [self.endAlert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            
            
       // }
    }

    else if(cell.roundNumber > 0){
        
     /*   //NSLog(@"ROUND 2 OR HIGHER END");
        if(modifyScoreTag > -1 && (cell.tempScores.count == cell.shotsAtEachPost)){
            
            //NSLog(@"FINAL MODIFY: Temp SCores: %d", cell.tempScores.count);
            
        }*/
        NSLog(@"Shots Counter: %d %d", self.shotsCounter, (int)(self.shooterShootArray.count * [self.currentShoot[@"shotsAtPost"] intValue] * (cell.roundNumber + 1)));
        
        
        if(self.shotsCounter == (int)((int)self.shooterShootArray.count * [self.currentShoot[@"shotsAtPost"] intValue] * (cell.roundNumber + 1)))
        {
        
        NSString *tempHits = [[NSString alloc] init];
        
        for (int x = 0; x < shooterShootArray.count; x++)
        {
            ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
            tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits",tempHits, shootTemp.shooter[@"fName"], shootTemp.hitsInRound];
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hits" message:[NSString stringWithFormat:@"%@", tempHits] delegate:self cancelButtonTitle:@"MODIFY" otherButtonTitles: @"APPROVE", nil];
        alert.tag =2;
             [self enableOrDisableShootButtons:NO];
        [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];

        
        }
    }
    else{
        
        
    }
    
    
    [self performSelectorOnMainThread:@selector(enableOrDisableShootButtons:) withObject:NO waitUntilDone:YES];
        
    
    
    if(modifyScoreTag > -1){
        
        self.shooterIndex = previousShooterIndex;
    
    }
    
    
    //[self enableOrDisableShootButtons: NO];
    [self performSelector:@selector(goToCurrentCell) withObject:nil afterDelay:0.05];
   
    
}




-(void) goToCurrentCell{
    
  //  //NSLog(@"HERE");
    if(modifyScoreTag > -1){
        
        
    }else{
    
        if(shooterIndex == ([self.shooterShootArray count] - 1)){
       
            shooterIndex = 0;
        
        }
        else{
    
            
            shooterIndex++;
    
        }
    }

    //THIS IS WHERE THE CELL NEEDS TO BE HIGHLIGHTED

     NSIndexPath *index1 = [NSIndexPath indexPathForRow:self.shooterIndex inSection:0];

    
    [tableView scrollToRowAtIndexPath:index1 atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
   
    self.previousShooterIndex = 0;
    self.modifyScoreTag = -1;
    [self.tableView setUserInteractionEnabled:YES];
     [self.tableView reloadData];
    [self enableOrDisableShootButtons:YES];
    
}

//This sections saves the shoot data to parse and to core data
-(void) endAndSaveShoot{
    
    //Creates a string of date time to hash and create a unique id for each shoot object
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"ddMMyyyyHHmmss"];
    dateString = [formatter stringFromDate:[NSDate date]];
   // //NSLog(@"DateTimeString: %@", dateString);
    dateString = [NSString stringWithFormat:@"%@%@", dateString, [PFUser currentUser][@"Organization"]];
  
    self.tempShootId = [self sha1:dateString];
    

            PFObject *shoot = [PFObject objectWithClassName:@"Shoot"];
            shoot[@"userID"] = [PFUser currentUser].objectId;
            shoot[@"coachID"] = self.coachId;
            shoot[@"appObjectId"] = tempShootId;
            shoot[@"trapFieldId"] = self.currentShoot[@"trapFieldId"];
            shoot[@"handicapType"] = self.currentShoot[@"typeShoot"];
            shoot[@"trapHouseNumber"] = self.currentShoot[@"trapHouseNumber"];
            shoot[@"typeShoot"] = self.currentShoot[@"pracOrComp"];
            shoot[@"shotsPerRotation"] = self.currentShoot[@"shotsAtPost"];
            shoot[@"totalShots"] = self.currentShoot[@"totalShots"];
    
            [shoot pin];
            [shoot saveEventually];
            
    
            
            for(int x = 0; x < shooterShootArray.count; x++){
                
                ShooterShootObject *shooterShootTemp = [self.shooterShootArray objectAtIndex:x];
                PFObject *shooterTemp = [PFObject objectWithClassName:@"Shooter"];
                shooterTemp = shooterShootTemp.shooter;
                 PFObject *shooterShoot = [PFObject objectWithClassName:@"ShootShooters"];
            
                int hitCounter = 0;
                for(int j = 0; j < shooterShootTemp.scoreArray.count; j++){
                    if([[shooterShootTemp.scoreArray objectAtIndex:j] isEqualToString: @"H"] || ([[shooterShootTemp.scoreArray objectAtIndex:j] isEqualToString: @"/"])){
                        
                        hitCounter++;
                    }
                    shooterShoot[@"totalHits"] = [NSNumber numberWithInt:hitCounter];
                }
               
                shooterShoot[@"shootID"] = tempShootId;
                shooterShoot[@"totalShots"] = self.currentShoot[@"totalShots"];
                shooterShoot[@"handicap"] = [NSNumber numberWithInt:[ shooterShootTemp.handicap intValue]];
                shooterShoot[@"shooterId"] = [NSString stringWithFormat:@"%@", shooterTemp[@"appObjectId"]];
                shooterShoot[@"startingPost"] = [NSNumber numberWithInt:[[self.startingPostNumbers objectAtIndex:x] intValue]];
                
                shooterShoot[@"shootData"] = shooterShootTemp.scoreArray;
                shooterShoot[@"totalHits"] = [NSNumber numberWithInt: hitCounter];
                shooterShoot[@"userID"] = [PFUser currentUser].objectId;
                [shooterShoot pin];
                [shooterShoot saveEventually];
                
            }
    
     [self presentViewController:self.scoringPage animated:YES completion:nil];
   
}

-(void) enableOrDisableShootButtons: (BOOL) enableDisable{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
      //  [tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
        
        if(enableDisable){
            [self.leftPlusButton setEnabled:YES];
            [self.leftButton setEnabled:YES];
            [self.straightButton setEnabled:YES];
            [self.rightButton setEnabled:YES];
            [self.rightPlusButton setEnabled:YES];
            [self.hitButton setEnabled:YES];
            [self.slashButton setEnabled:YES];
            [self.oButton setEnabled:YES];
            
        }
        else{ //Buttons are disabled with NO
            [self.leftPlusButton setEnabled:NO];
            [self.leftButton setEnabled:NO];
            [self.straightButton setEnabled:NO];
            [self.rightButton setEnabled:NO];
            [self.rightPlusButton setEnabled:NO];
            [self.hitButton setEnabled:NO];
            [self.slashButton setEnabled:NO];
            [self.oButton setEnabled:NO];
            
        }

    });
    ////NSLog(@"GOTHERE");
    //Buttons are enabled with YES
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView == self.endAlert){
        
        if(buttonIndex == 0){
            
           //MODIFY LAST ROUND IN SHOOT
             [self enableOrDisableShootButtons:NO];
            
        }
        else{
            //END AND SAVE THE SHOOT ON APPROVAL
             [self endAndSaveShoot];
        }
        
        
    }
    else if(alertView.tag == 1){  // END OF ROUND 1
        
        //self.shooterIndex = 0;
        //[self.tableView reloadData];
        if(buttonIndex == 0)  //Modify
        {
            
             [self enableOrDisableShootButtons:NO];
            
        }
        else{
            
            //NSLog(@"Approve");
            
            for (int x = 0; x < shooterShootArray.count; x++) {
                ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                //tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits",tempHits, shootTemp.shooter.fName, shootTemp.hitsInRound];
                 shootTemp.hitsInRound = 0;
                  shootTemp.shotsFiredInRound = 0;
                  shootTemp.roundNumber++;
                   if(shootTemp.postNumber == 5){
                
                      shootTemp.postNumber = 1;
                
                   }
                  else{
                
                      shootTemp.postNumber++;
                
                    }

                }
            
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
        }
        
        
        
    }
    else if(alertView.tag == 2){  //END OF OTHER ROUNDS
        
        if(buttonIndex == 0)  //Modify
        {
        
             [self enableOrDisableShootButtons:NO];
            
        }
        else{
            
            //NSLog(@"Approve");
            
            for (int x = 0; x < shooterShootArray.count; x++) {
                ShooterShootObject *shootTemp = [self.shooterShootArray objectAtIndex:x ];
                //tempHits = [NSString stringWithFormat:@"%@\n%@: %d Hits",tempHits, shootTemp.shooter.fName, shootTemp.hitsInRound];
                shootTemp.hitsInRound = 0;
                shootTemp.shotsFiredInRound = 0;
                shootTemp.roundNumber++;
                if(shootTemp.postNumber == 5){
                    
                    shootTemp.postNumber = 1;
                    
                }
                else{
                    
                    shootTemp.postNumber++;
                    
                }
                
            }
            
            [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
            
        }
       
    }
    else if(alertView.tag == 3)
    {
        
        if(buttonIndex == 0)
        {
            
            [self presentViewController:self.initialSetUpShoot animated:YES completion:nil];
        }
        else
        {
            
            
        }
    }

}


//This allows the user to go back and rerecord a shoot score
- (IBAction)undoLastAction:(id)sender {

    
   // //NSLog(@"ShooterIndex: %d", self.shooterIndex);
    
  
    if(self.shooterIndex == 0){
        
        self.shooterIndex = (int)([self.shooterShootArray count] - 1);
       // //NSLog(@"UNDO");
        
    }else{
        
        self.shooterIndex--;
        
    }
    
   // //NSLog(@"ShooterIndexv2: %d", self.shooterIndex);
    NSIndexPath *indexCell = [NSIndexPath indexPathForRow: self.shooterIndex inSection:0];
    CustomShootCell *cellChange = (CustomShootCell *)[self.tableView cellForRowAtIndexPath: indexCell];
     ShooterShootObject *shooterShootTemp = [self.shooterShootArray objectAtIndex:self.shooterIndex];
    
     if(cellChange.shotsFiredInRound == cellChange.shotsAtEachPost){
        
       // //NSLog(@"ROUNDDOWN");
         self.undoButton.enabled = NO;
        //Set Undo button to disable so that you cant undo past a round
        
    }
    
    /*[self.tableView reloadData];*/
    cellChange.shotsFiredInRound--;
    cellChange.shotsFired--;
    
    if([[cellChange.scoreArray objectAtIndex:([cellChange.scoreArray count] - 1)] isEqualToString:@"H"] ||[[cellChange.scoreArray objectAtIndex:([cellChange.scoreArray count] - 1)] isEqualToString:@"/"]){
        
        shooterShootTemp.hitNumber--;
        shooterShootTemp.hitsInRound--;
        cellChange.hitsOverall--;
        
    }
  
    [cellChange.scoreArray removeLastObject];
    self.shotsCounter--;

   
    shooterShootTemp.shotsFiredInRound--;
    shooterShootTemp.shotCounter--;
  

    [tableView scrollToRowAtIndexPath:indexCell atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self.tableView reloadData];
    [self enableOrDisableShootButtons:YES];
    
}

-(NSString*) sha1:(NSString*)input
{
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
    
    CC_SHA1(data.bytes, data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
    
}


-(void)scoreFieldClicked : (int)cellTag :(UITextField *)textField :(NSMutableArray *)shootShooter{
    
    [self.tableView setUserInteractionEnabled:NO];
    self.previousShooterIndex = self.shooterIndex;
    
    NSIndexPath *cellIndex = [NSIndexPath indexPathForRow:cellTag inSection:0];
    NSArray *array = [[NSArray alloc] initWithObjects:cellIndex, nil];
    CustomShootCell *shootCell = (CustomShootCell *)[tableView cellForRowAtIndexPath:cellIndex];

    self.shooterIndex = cellTag;
    ShooterShootObject *shooterShootObject = [shootShooter objectAtIndex:self.shooterIndex];

    
    [tableView scrollToRowAtIndexPath:cellIndex atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    
      NSLog(@"Delegate called properly: Cell Index %d, ShooterShoot %@ textFieldTag: %d", (int)cellIndex.row, shooterShootObject.scoreArray, (int)textField.tag);
    self.shotsCounter--;
   
    int x = 0;
    if(shootCell.roundNumber == 0){
        
        if([textField.text isEqualToString:@"H"] || [textField.text isEqualToString:@"/"]){
            
            shooterShootObject.hitNumber--;
            shooterShootObject.hitsInRound--;
        }
        
          self.modifyScoreTag = (int)textField.tag;
       [shooterShootObject.scoreArray replaceObjectAtIndex:textField.tag withObject:@""];
        
       [shootCell.scoreArray replaceObjectAtIndex:textField.tag withObject:@""];
        shootCell.isIndex = YES;
       //  [self.tableView reloadData];
        [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
        
        CustomShootCell *shootCell2 = (CustomShootCell *)[tableView cellForRowAtIndexPath:cellIndex];

        dispatch_async(dispatch_get_main_queue(), ^{
            
            [shootCell2 highlightTextField:textField];
        
        });
        
    }
    else{
        
        if([textField.text isEqualToString:@"H"] || [textField.text isEqualToString:@"/"]){
            
            shooterShootObject.hitNumber--;
            shooterShootObject.hitsInRound--;
        }
        
        x = ((shootCell.roundNumber *shootCell.shotsAtEachPost) + (int)textField.tag);
        NSLog(@"number for round: %d", x);
        
        self.modifyScoreTag = x;
       [shooterShootObject.scoreArray replaceObjectAtIndex:x withObject:@""];
       [shootCell.scoreArray replaceObjectAtIndex:x withObject:@""];
        shootCell.isIndex = YES;
     //   [self.tableView reloadData];
        [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationFade];
        
        CustomShootCell *shootCell2 = (CustomShootCell *)[tableView cellForRowAtIndexPath:cellIndex];

        dispatch_async(dispatch_get_main_queue(), ^{
            
         [shootCell2 highlightTextField:textField];
     
        });
      //  //NSLog(@"ShOTS: %@", shootCell.shotsFiredInRound);f
        
    }
    
     [self enableOrDisableShootButtons:YES];
}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

@end
