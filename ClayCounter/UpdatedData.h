//
//  UpdatedData.h
//  ClayCounter
//
//  Created by Test on 1/18/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

@protocol UpdateDataDelegate

-(void)retrieveData;
-(void) gotShoots: (NSArray *)shoots :(int)startingPoint;
-(void) gotShooters: (NSArray *) shooters;
-(void) gotFields: (NSArray *) fields;
-(void) gotShootShooters: (NSArray *) shootShooters;
-(void) gotSubscription :(NSArray *) subscription;
-(void) gotCoaches: (NSArray *) array;

@end

#import "SettingsPage.h"
#import "ScoringPage.h"
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "ShootersPage.h"
#import <CoreData/CoreData.h>
#import "MagicalRecord.h"
#import "Shooter.h"
#import "Coach.h"
#import "ShootShooters.h"
#import <Bolts/Bolts.h>
#import "TrapField.h"


@interface UpdatedData : NSObject

-(void) getShootsNew: (BOOL) online : (int)startingPoint;
-(void) getShootersNew: (BOOL) online;
-(void) getTrapFieldsNew: (BOOL) online;
-(void) getShootShootersNew: (BOOL) online;
-(void) getSubscriptionIdNew: (BOOL) online;
-(void) getCoachesNew: (BOOL) online;

-(void) triggerRetrieveDataDelegate;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
-(BOOL) isNetworkAvailable;
@property (strong, nonatomic) id <UpdateDataDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) NSMutableArray *coachesArray;
@property (strong, nonatomic) NSMutableArray *shootArray;
@property (strong, nonatomic) NSMutableArray *shootShooterArray;
@property (strong, nonatomic) NSMutableArray *trapFieldsArray;
@property (strong, nonatomic) NSMutableArray *subscriptionArray;
@property int numOfSavedShooters;
@property int numOfParseShooters;
@property int numeOfSavedCoaches;
@property int numOfParseCoaches;
@property int numOfSavedShoots;
@property int numOfParseShoots;
@property int numOfSavedShootShooters;
@property int numOfParseShootShooters;
@property int numOfSavedSubscriptions;
@property int numOfParseSubscriptions;
@property int delegateTag;
-(void)getShooters;
-(void)getCoaches;
-(void) getShoots;
-(void) getShootShooters;
-(void) getTrapFields;

@end
