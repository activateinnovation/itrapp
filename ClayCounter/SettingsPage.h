//
//  SettingsPage.h
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//
#import "ScoringPage.h"
#import "ShootersPage.h"
#import <UIKit/UIKit.h>
#import "LoginView.h"
#import "UpdatedData.h"
#import "Shooter.h"
#import "Coach.h"
#import "CurrentShoot.h"
#import "ShootShooters.h"
#import "StoredShoot.h"
#import <MSCMoreOptionTableViewCell/MSCMoreOptionTableViewCell.h>


@class MSCMoreOptionTableViewCell;
@class ScoringPage;
@class ShootersPage;
@class LoginView;
@class UpdatedData;
@class Shooter;
@class Coach;

@interface SettingsPage : UIViewController <UITabBarDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, UpdateDataDelegate, MSCMoreOptionTableViewCellDelegate>
@property (strong, nonatomic) IBOutlet UIBarButtonItem *logOutOutlet;
@property (strong, nonatomic) UIView *container;
@property (strong, nonatomic) IBOutlet UITextField *accountTypeLabel;
@property (strong, nonatomic) UIView *addCoachView;
@property (strong, nonatomic) UIView *editCoachView;
- (IBAction)cancelAddCoach:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelButtonOutlet;
@property (strong, nonatomic) IBOutlet UILabel *usernameLabel;
@property (strong, nonatomic) PFObject *coachToDelete;
@property int deleteIndex;

//@property (strong, nonatomic) IBOutlet UIBarButtonItem *editAccountOutlet;
@property (strong, nonatomic) IBOutlet UILabel *organizationLabel;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) UISwitch *leftRightSwitch;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UINavigationBar *navBar;
- (IBAction)addCoach:(id)sender;
@property (strong, nonatomic) PFObject *coachToEdit;
@property (strong, nonatomic) UITextField *firstName;
@property (strong, nonatomic) UITextField *lastName;
@property (strong, nonatomic) UITextField *phoneNum;
@property (strong, nonatomic) UITextField *emailAddress;
@property LoginView *loginView;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) IBOutlet UILabel *trapFieldLabel;
@property (strong, nonatomic) IBOutlet UILabel *numOfShootersLabel;
- (IBAction)logOut:(id)sender;
@property int editIndex;
@property ScoringPage* scoringPage;
@property ShootersPage *shootersPage;
@property UpdatedData *updatedData;
@property (strong, nonatomic) NSMutableArray *coachesDataArray;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;


@end
