//
//  InitialSetupShootSelector.m
//  ClayCounter
//
//  Created by Test on 7/17/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "InitialSetupShootSelector.h"

@interface InitialSetupShootSelector ()

@end

@implementation InitialSetupShootSelector
@synthesize setupShootView;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    self.scoringPage = [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];
    self.setupShootView =
    [storyboard instantiateViewControllerWithIdentifier:@"setupShoot"];

    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iTrappShooterInfoBackground.jpg"]];
    
    if(self.view.frame.size.width > 320)
    {
        [background setFrame: CGRectMake(0, 55, 780, 900)];
    }
    else
    {
        [background setFrame: CGRectMake(0, 55, 320, 520)];
    }
    [background setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)singlesClicked:(id)sender {
    
    [setupShootView typeOfShootSelected: @"Singles"];
    [self presentViewController:setupShootView animated:YES completion: nil];
    
    
    
}

- (IBAction)handicapsClicked:(id)sender {
    
    [setupShootView typeOfShootSelected: @"Handicaps"];
    [self presentViewController:setupShootView animated:YES completion: nil];
    
    
}

- (IBAction)cancelClicked:(id)sender {
    
    
    [self presentViewController:self.scoringPage animated:YES completion: nil];
}

- (IBAction)doublesClicked:(id)sender {
    
   
    [self presentViewController:setupShootView animated:YES completion: nil];
     [setupShootView typeOfShootSelected: @"Doubles"];
    
}
@end
