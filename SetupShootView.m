//
//  SetupShootView.m
//  ClayCounter
//
//  Created by ActivateInnovation on 1/30/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//

#import "SetupShootView.h"

@interface SetupShootView ()

@end

@implementation SetupShootView
@synthesize scoringPage;
@synthesize dateLabel;
@synthesize selectCoachView;
@synthesize container;
@synthesize navBar;
@synthesize beginShootOutlet;
@synthesize cancelSetUpButton;
@synthesize selectCoachTableView;
@synthesize coachArray;
@synthesize updateData;
@synthesize counter;
@synthesize selectedCoach;
@synthesize pracCompSegmentContol;
@synthesize finalShootersTableView;
@synthesize shotsInShootSegmentControl;
@synthesize shotsPerStationSegementControl;
@synthesize shootersSelectedArray;
@synthesize selectShootersView;
@synthesize shootersArray;
@synthesize shootersTableView;
@synthesize container2;
@synthesize shootersCounter;
@synthesize finalShootObject;
@synthesize scoringShootPageView;
@synthesize scoringPageTemp;
@synthesize inTransitPostNumber;
@synthesize shooterCounterLabel;
@synthesize checkmarksArray;
@synthesize numberOfShootersSelected;
@synthesize trapHousePicker;
@synthesize trapRangePicker;
@synthesize trapRanges;
@synthesize trapHouseNumbers;
@synthesize trapRangeLabel;
@synthesize trapHouseLabel;
@synthesize statesArray;
@synthesize selectShootersButton;
@synthesize dragShootersLabel;
@synthesize doneOnPickerOutlet;
@synthesize trapHouseNumberSelected;
@synthesize trapRangeSelected;
@synthesize pickerOpened;
@synthesize selectCoachOutlet;
@synthesize selectTrapHouseOutlet;
@synthesize selectTrapRangeOutlet;
@synthesize handicapSelected;
@synthesize handicapLabel;
@synthesize selectHandicapOutlet;
@synthesize handicapNumbersArray;
@synthesize handicapPicker;
@synthesize yrdsLabel;
@synthesize skipButtonOutlet;
@synthesize stateSelected;
@synthesize addTrapRangeLabel;
@synthesize dontSeeRangeLabel;
@synthesize addTrapRangeView;
@synthesize rangeName;
@synthesize phoneNum;
@synthesize street;
@synthesize city;
@synthesize state;
@synthesize zipCode;
@synthesize statePicker;
@synthesize addRangeButton;
@synthesize addRangeCounter;
@synthesize tradOrAdvanced;
@synthesize scoringTypeLabelOutlet;
@synthesize shotsInShootLabelOutlet;
@synthesize shotsPerPostLabelOutlet;
@synthesize typeOfShoot;
@synthesize label0;
@synthesize label1;
@synthesize label2;
@synthesize label3;
@synthesize label4;
@synthesize labelArray;
@synthesize currentStepperValues;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:YES];
    NSString *date = [self getDate];
    self.dateLabel.text = [NSString stringWithFormat:@"%@", date];
     self.beginShootOutlet.tag = 1;
    self.updateData = [[UpdatedData alloc] init];
    self.updateData.delegate = self;
    self.cancelSetUpButton.tag = 1;
    self.coachArray = [[NSMutableArray alloc] init];
    self.shootersSelectedArray = [[NSMutableArray alloc] init];
    
    //self.shootersArray = [[NSMutableArray alloc] init];
    self.checkmarksArray = [[NSMutableArray alloc] init];
    self.numberOfShootersSelected = 0;
  //   [updateData getShootersNew:NO];
    //[updateData getCoachesNew:NO];
    label0 = [[UILabel alloc] init];
    label0.tag = 0;
     label1 = [[UILabel alloc] init];
    label1.tag = 1;
     label2 = [[UILabel alloc] init];
    label2.tag = 2;
     label3 = [[UILabel alloc] init];
    label3.tag = 3;
     label4 = [[UILabel alloc] init];
    label4.tag = 4;
    
    self.currentStepperValues = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInt:18], [NSNumber numberWithInt:18],[NSNumber numberWithInt:18],[NSNumber numberWithInt:18],[NSNumber numberWithInt:18], nil];
    
    //self.currentStepperValues = [[NSMutableArray alloc] init];

    self.labelArray = [[NSMutableArray alloc] initWithObjects:label0, label1, label2, label3, label4, nil];
   // self.typeOfShoot = [[NSString alloc] init];
    self.stateSelected = 0;
    self.yrdsLabel.hidden = YES;
    self.pickerOpened = 0;
    self.trapRangeSelected = NO;
    self.trapHouseNumberSelected = NO;
    self.handicapSelected = NO;
    self.doneOnPickerOutlet.hidden = YES;
    self.skipButtonOutlet.hidden = YES;
    self.addTrapRangeLabel.hidden = YES;
    self.dontSeeRangeLabel.hidden = YES;
    self.addRangeCounter = 1;
    
    //self.finalShootersTableView.hidden = YES;
    self.dragShootersLabel.hidden = YES;
    
    
    [self.pracCompSegmentContol setBackgroundColor:[UIColor whiteColor]];
    [self.pracCompSegmentContol setTintColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f]];
    [self.pracCompSegmentContol.layer setBorderColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f].CGColor];
    [self.pracCompSegmentContol.layer setCornerRadius:5.0f];
    [self.pracCompSegmentContol.layer setBorderWidth:2.0f];
    
    [self.tradOrAdvanced setBackgroundColor:[UIColor whiteColor]];
    [self.tradOrAdvanced setTintColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f]];
    [self.tradOrAdvanced.layer setBorderColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f].CGColor];
    [self.tradOrAdvanced.layer setCornerRadius:5.0f];
    [self.tradOrAdvanced.layer setBorderWidth:2.0f];

    
    
    [self.shotsInShootSegmentControl setBackgroundColor:[UIColor whiteColor]];
    [self.shotsInShootSegmentControl setTintColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f]];
    [self.shotsInShootSegmentControl.layer setBorderColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f].CGColor];
    [self.shotsInShootSegmentControl.layer setCornerRadius:5.0f];
    [self.shotsInShootSegmentControl.layer setBorderWidth:2.0f];
    
    
    [self.shotsPerStationSegementControl setBackgroundColor:[UIColor whiteColor]];
    [self.shotsPerStationSegementControl setTintColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f]];
    [self.shotsPerStationSegementControl.layer setBorderColor:[UIColor colorWithRed:(160/255.f) green:(212/255.f) blue:(104/255.f) alpha:1.0f].CGColor];
    [self.shotsPerStationSegementControl.layer setCornerRadius:5.0f];
    [self.shotsPerStationSegementControl.layer setBorderWidth:2.0f];
    
    self.currentShoot = [PFObject objectWithClassName:@"CurrentShoot"];
    [self.currentShoot pin];
    
    PFQuery *currentQuery = [PFQuery queryWithClassName:@"CurrentShoot"];
    [currentQuery orderByAscending:@"createdAt"];
    [currentQuery fromLocalDatastore];
    self.currentShoot = [currentQuery getFirstObject];
    NSLog(@"TempShoot: %@", self.currentShoot);


    PFQuery *coachQuery = [PFQuery queryWithClassName:@"Coach"];
    [coachQuery fromLocalDatastore];
    [coachQuery whereKey:@"userId" containsString:[PFUser currentUser].objectId];
    [self.coachArray addObjectsFromArray: [coachQuery findObjects]];
    for(int x = 0; x < self.coachArray.count; x++)
    {
        [self.checkmarksArray insertObject: [NSNumber numberWithInt:0] atIndex:x];
    }
    
    self.statesArray = [[NSMutableArray alloc] initWithObjects: @"N/A", @"AL",@"AK",@"AZ",@"AR",@"CA",@"CO",@"CT",@"DE",@"DC",@"FL",@"GA",@"HI",@"ID",@"IL",@"IN",@"IA",@"KS",@"KY",@"LA",@"ME",@"MD",@"MA",@"MI",@"MN",@"MS",@"MO",@"MT",@"NE",@"NV",@"NH", @"NJ",@"NM",@"NY",@"NC",@"ND",@"OH",@"OK",@"OR",@"PA",@"RI",@"SC",@"SD",@"TN",@"TX",@"UT",@"VT",@"VA",@"WA",@"WV",@"WI",@"WY",nil];

    //Setting up data for both pickers
    self.trapHouseNumbers = [[NSMutableArray alloc] init];
    for(int x = 0; x < 101; x++){
        
        [self.trapHouseNumbers addObject:[NSString stringWithFormat:@"%d", x]];
    }
    
    self.handicapNumbersArray = [[NSMutableArray alloc] init];
    for(int x = 16; x < 28; x++){
        
        [self.handicapNumbersArray addObject:[NSString stringWithFormat:@"%d", x]];
    }

    
    //DEFAILT VALUES BEING SET IF THERE ARE DEFAULT VALUES
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"defaultDictionary"] == nil){
        
       // self.handicapLabel.text = [NSString stringWithFormat:@"%@", [self.handicapNumbersArray objectAtIndex:0]];
       // self.yrdsLabel.hidden = YES;
        
        self.trapHouseLabel.text = [NSString stringWithFormat:@"%@", [self.trapHouseNumbers objectAtIndex:0]];
    }
    else{
        
        NSMutableDictionary *dictonary = [[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultDictionary"] mutableCopy];
        NSString *tempTrapFieldId = [dictonary objectForKey:@"range"];
        if([tempTrapFieldId isEqualToString:@"None Selected"]){
            
            self.trapRangeLabel.text = @"None Selected";
        }
        else{
            
            PFQuery *trapFieldQuery  = [PFQuery queryWithClassName:@"TrapField"];
            [trapFieldQuery fromLocalDatastore];
            [trapFieldQuery whereKey:@"objectId" containsString:tempTrapFieldId];
            PFObject *trapField = [PFObject objectWithClassName:@"TrapField"];
            trapField = [trapFieldQuery getFirstObject];
    
            NSLog(@"trapField Defualt: %@", trapField);
            self.trapRangeLabel.text = trapField[@"Name"];
            
        }
    
        PFObject *coach = [PFObject objectWithClassName:@"Coach"];
        PFQuery *query = [PFQuery queryWithClassName:@"Coach"];
        [query fromLocalDatastore];
        [query whereKey:@"appObjectId" containsString:[dictonary objectForKey:@"coach"]];
        coach = [query getFirstObject];

        
        self.coachNameLabel.text = [NSString stringWithFormat:@"%@ %@", coach[@"fName"], coach[@"lName"]];
        self.selectedCoach = coach;
        [self.tradOrAdvanced setSelectedSegmentIndex:[[dictonary objectForKey:@"scoringType"] intValue]];
   
        
        self.trapHouseLabel.text = [NSString stringWithFormat:@"%@", [dictonary objectForKey:@"house"]];
        
    }
   
   

    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
    
    if(self.view.frame.size.width > 320)
    {
        [background setFrame: CGRectMake(0, 50, 780, 900)];
    }
    else
    {
        [background setFrame: CGRectMake(0, 50, 320, 540)];
    }
    [background setContentMode:UIViewContentModeScaleAspectFill];
    
    [self.view addSubview:background];
    [self.view sendSubviewToBack:background];
    

    
     //  NSLog(@"Trap Ranges start: %@", self.trapRanges);
   // self.trapRanges = [[TrapField MR_findAllSortedBy:@"name" ascending:YES] mutableCopy];
    
 
       self.container = [[UIView alloc] init];
    self.container.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.container];
    [self.view sendSubviewToBack:self.container];

    
    self.selectCoachTableView = [[UITableView alloc] init];
    self.selectCoachTableView.delegate = self;
  
    self.selectCoachTableView.dataSource = self;
    
    self.selectCoachView = [[UIView alloc] init];
    self.selectCoachView.clearsContextBeforeDrawing = YES;
      [self.selectCoachView setBackgroundColor:[UIColor lightGrayColor]];
    self.scoringPageTemp = [[ScoringShootPage alloc] init];
    
    
    self.addTrapRangeView = [[UIView alloc] init];
    self.addTrapRangeView.clearsContextBeforeDrawing = YES;
    self.scoringPageTemp = [[ScoringShootPage alloc] init];
    
    
    self.counter = 0;
    self.shootersCounter = 1;
    self.inTransitPostNumber = 0;
   
    self.cancelSetUpButton.tag = 1;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
   
    UIStoryboard* storyboard;
    if(self.view.frame.size.width > 320)
    {
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
        
    }
    else{
        
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    }
    
    self.scoringPage =
    [storyboard instantiateViewControllerWithIdentifier:@"Score Card"];

    
    self.scoringShootPageView =
    [storyboard instantiateViewControllerWithIdentifier:@"ScoringShootPage"];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)beginShoot:(id)sender {

    
    if([sender tag] == 1){
    
        if(self.shootersSelectedArray.count == 0 || self.selectedCoach == nil || [self.trapRangeLabel.text isEqualToString:@"(Trap Range Name)"] || [self.trapHouseLabel.text isEqualToString:@"(Trap House)"] ){
            
           // NSLog(@"Not all data here");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"Please make sure to have a coach, a value for the trap range (or \"None Selected\"), a trap house number, a handicap distance, and at least one shooter selected before beginning." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
            self.beginShootOutlet.tag = 1;
            
        }
        //This checks to make sure that when 25, or 75 shots selected that only 5 shots perpost can be selected. 
        else if(([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"25"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"10"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"25"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"2"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"50"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"2"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"75"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"2"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"100"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"2"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"200"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"2"]) || ([[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] isEqualToString:@"75"] && [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] isEqualToString:@"10"])){
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incorrect Data" message:@"The selected total shot count and shots per shoot are not allowed to be used together." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:YES];
        
            
        }
        else{
            
           // NSLog(@"Begin Shoot: %d", self.shootersSelectedArray.count);
            //[self presentViewController: scoringPage animated:YES completion:nil];
            
            NSString *finalStartingPosts = [[NSString alloc] init];
          
            int tempCounter = 0;
            
            NSMutableArray *indexesToDelete = [[NSMutableArray alloc] init];
            
            for(int x = 0; x < self.shootersSelectedArray.count; x++)
            {
                  
              if([[self.shootersSelectedArray objectAtIndex:x] isKindOfClass:[PFObject class]]){
                  
                //  NSLog(@"TEST");
                  
                  if(tempCounter == 0){
                      // NSLog(@"TEST1");
                      finalStartingPosts = [NSString stringWithFormat:@"%d", (x + 1)];
                      tempCounter++;
                  }
                  else{
                      
                      // NSLog(@"TEST2");
                      finalStartingPosts = [NSString stringWithFormat:@"%@, %d",finalStartingPosts, (x + 1)];
                      tempCounter++;
                  }
                  
              }
            
              else
              {
                  [indexesToDelete addObject:[self.shootersSelectedArray objectAtIndex:x] ];
              }
           
            
            }
            
           // NSLog(@"Selected Shooters: %@", self.shootersSelectedArray);
          //  NSLog(@"Indexes: %@", indexesToDelete);
            for(int j = 0; j < indexesToDelete.count; j++)
            {
                for(int x = 0; x < self.shootersSelectedArray.count; x++)
                {
                    if([self.shootersSelectedArray[x] isEqual: indexesToDelete[j]]){
                        
                        [self.shootersSelectedArray removeObjectAtIndex:x];
                        
                    }
                    
                }
                
               
            }
            
            
            if([[NSUserDefaults standardUserDefaults] objectForKey:@"defaultDictionary"] == nil){
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Default Values" message:[NSString stringWithFormat:@"Welcome to our scoring system.  The buttons on the right correspond to hits and misses that drive the scoring. Click a scoring button to see how it works! Would you like to set these as your Setup Shoot default values?\nCoach: %@\nScoring Type: %@\nRange: %@\nHouse #: %@\n", self.coachNameLabel.text, [self.tradOrAdvanced titleForSegmentAtIndex:[self.tradOrAdvanced selectedSegmentIndex]],self.trapRangeLabel.text, self.trapHouseLabel.text] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
                alert.tag = 3;
                [alert show];
                
            }
            else
            {
                
                
            }
        
        
           //NSLog(@"FINALPOSTS: %@", finalStartingPosts);
            
            NSMutableArray *finalHandicapValues = [[NSMutableArray alloc] init];
            NSString *finalHandicapString;
            if([typeOfShoot isEqualToString:@"Handicaps"]){
                
                for(int x = 0; x < currentStepperValues.count; x++){
                    
                    //NSLog(@"Final Handicaps: %@", currentStepperValues[x]);
                    if(currentStepperValues[x] != 0)
                    {
                        [finalHandicapValues addObject:currentStepperValues[x]];
                    }
    
                }
                
                finalHandicapString = [finalHandicapValues componentsJoinedByString:@","];
            }
            else{
                    
                for(int x = 0; x < shootersSelectedArray.count; x++){
                    
                    //NSLog(@"Final Handicaps: %@", currentStepperValues[x]);
                    [finalHandicapValues addObject:[NSNumber numberWithInt:0]];
                
                    
                }
    
                finalHandicapString = [finalHandicapValues componentsJoinedByString:@","];
                    
                    
            }
             
            
            self.finalShootObject = [[ShootObjectClass alloc] initWithItems:self.shootersSelectedArray : self.selectedCoach :[[self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]] intValue] : [[self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]] intValue]];
            NSString *tempString = [[NSString alloc] init];
            PFObject *tempShooter = [PFObject objectWithClassName:@"Shooter"];
            tempShooter = [self.shootersSelectedArray objectAtIndex:0];
            tempString = [NSString stringWithFormat:@"%@", tempShooter[@"appObjectId"]];
                          
            for(int x = 1; x < [self.shootersSelectedArray count]; x++){
                
                PFObject *temp = [PFObject objectWithClassName:@"Shooter"];
                temp = [self.shootersSelectedArray objectAtIndex:x];
                tempString = [NSString stringWithFormat:@"%@,%@", tempString, temp[@"appObjectId"]];
                
           // NSLog(@"tempString: %@", tempString);
            }
            
            //self.managedObjectContext = [NSManagedObjectContext MR_contextForCurrentThread];
            //TrapField *trapField;
            PFObject *trapField = [PFObject objectWithClassName:@"TrapField"];
            
            NSNumber *finalTrapHouse = [NSNumber numberWithInt:[self.trapHouseLabel.text intValue]];
            
            //CurrentShoot *currentShoot = [CurrentShoot MR_createInContext:self.managedObjectContext];
            //PFObject *currentShoot = [PFObject objectWithClassName:@"CurrentShoot"];
            self.currentShoot[@"coach"] = [NSString stringWithFormat:@"%@", self.selectedCoach[@"appObjectId"]];
            self.currentShoot[@"startingPost"] = finalStartingPosts;
            self.currentShoot[@"shooters"] = tempString;
            self.currentShoot[@"shotsAtPost"] = [NSNumber numberWithInt:self.finalShootObject.shotsAtEachPost] ;
            self.currentShoot[@"totalShots"] = [NSNumber numberWithInt:self.finalShootObject.shotsTotal];
            self.currentShoot[@"handicapString"] = finalHandicapString;
            self.currentShoot[@"typeShoot"] = self.typeOfShoot;
            if([self.trapRangeLabel.text isEqualToString:@"None Selected"]){
                
                 self.currentShoot[@"trapFieldId"] = @"NA";
                
            }
            else{
                
                PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
                [query whereKey:@"Name" containsString:self.trapRangeLabel.text];
                [query fromLocalDatastore];
                trapField = [query getFirstObject];
                 self.currentShoot[@"trapFieldId"] = trapField.objectId;
            }

            self.currentShoot[@"trapHouseNumber"] = finalTrapHouse;
            self.currentShoot[@"pracOrComp"] = [self.pracCompSegmentContol titleForSegmentAtIndex:[self.pracCompSegmentContol selectedSegmentIndex]];
            self.currentShoot[@"scoringType"] = [self.tradOrAdvanced titleForSegmentAtIndex:[self.tradOrAdvanced selectedSegmentIndex]];
        
            [self.currentShoot pin];
            // [scoringPageTemp setShootData:self.finalShootObject];
            
            [self presentViewController:self.scoringShootPageView animated: YES completion:nil];
            
            //NEED TO SEND THE SHOOTOBJECT TO THE SET SHOOT DATA METHOD IN THE SCORINGSHOOTPAGE AND TRANSITON THE VIEW
            
           // NSLog(@"Prac/Comp: %@, ShotsTotal: %@, Shots/Station: %@", [self.pracCompSegmentContol titleForSegmentAtIndex:[self.pracCompSegmentContol selectedSegmentIndex]], [self.shotsInShootSegmentControl titleForSegmentAtIndex:[self.shotsInShootSegmentControl selectedSegmentIndex]], [self.shotsPerStationSegementControl titleForSegmentAtIndex:[self.shotsPerStationSegementControl selectedSegmentIndex]]);
            
            //NSLog(@"Selected Shooters: %@", self.shootersSelectedArray);
        //    NSLog(@"Selected Coach: %@", selectedCoach);
        
            
        }
    }
    
    else if([sender tag] == 2){
        
        
       // NSLog(@"selectedShooters: %lu", (unsigned long)[self.shootersSelectedArray count]);
        [self doneClickedOnAddShooters];
        
    }
    else{
       
        
        
        
    }
    
    

    
    
}

-(void) retrieveData{
    
    
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (counter == 1) {
        
        /*self.coachArray = [[NSMutableArray alloc] init];
        self.coachArray = [[Coach MR_findAll] mutableCopy];*/
       // NSLog(@"HERERERERE : %@", coachArray);
       return [self.coachArray count];
        
   }
    else if(counter == 2){
        
        if(tableView.tag == 1){
            self.counter = 1;
           // NSLog(@"HERERERERE : %@", coachArray);
            return [self.coachArray count];
            
        }
        else{
       //  NSLog(@"HERERERERE2");
        //int shooterCap = [[[NSUserDefaults standardUserDefaults] objectForKey:@"shooterCap"] intValue];
        //NSLog(@"Shooter Cap: %d", shooterCap);
            PFQuery *subQuery = [PFQuery queryWithClassName:@"Subscription"];
            [subQuery whereKey:@"objectId" containsString:[PFUser currentUser][@"SubscriptionId"]];
            [subQuery fromLocalDatastore];
            PFObject *subscription = [PFObject objectWithClassName:@"Subscription"];
            subscription = [subQuery getFirstObject];
            int shooterCap = [subscription[@"shooterCap"] intValue];
          //  NSLog(@"Shooter Cap: %d", shooterCap);
        
        
        if(self.coachArray.count > shooterCap){
            
            return shooterCap;
            
        }
        else{
            
            return [self.coachArray count];
        }
        }
    
    }
    else if(tableView == self.finalShootersTableView){
        
        return [self.shootersSelectedArray count];
        
    }
    //NSLog(@"HERE");
    return 0;
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: nil];
    
    for(UIView *view in cell.contentView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
            
        }
    }
    
    if (cell == nil) {
        
        //NSLog(@"New");
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if(tableView == self.selectCoachTableView && counter == 2){
            if([[self.checkmarksArray objectAtIndex:indexPath.row] intValue] == 1)
            {
                
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];

            
            }
            else{
            
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            
            }
        }
        else{
            
             [cell setAccessoryType:UITableViewCellAccessoryNone];
            
        }
    }
    
  
    
    if(counter == 1){ //Coaches Selection TableView
        
        //[self.coachArray removeAllObjects];
        self.counter = 1;
        //self.coachArray = [[Coach MR_findAll] mutableCopy];
        PFObject *coachTemp = [PFObject objectWithClassName:@"Coach"];
        coachTemp = [self.coachArray objectAtIndex:indexPath.row];
    
    UILabel *cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 180, 30)];
    [cellTitle setText: [NSString stringWithFormat:@"%@ %@",coachTemp[@"fName"] , coachTemp[@"lName"]]];
    
    [cell addSubview: cellTitle];
        
        UILabel *leftOrRight = [[UILabel alloc] initWithFrame:CGRectMake(210, 10, 100, 25)];
        [cell addSubview:leftOrRight];
        [leftOrRight setText:[NSString stringWithFormat:@"%@ Handed", coachTemp[@"leftRightHanded"]]];
        [leftOrRight setFont:[UIFont systemFontOfSize:14]];
        [leftOrRight setTextColor:[UIColor darkGrayColor]];
    
    }
    else if(counter == 2) { //Shooter Selections Tableview
        
        PFObject *shooterTemp = [PFObject objectWithClassName:@"Shooter"];
        shooterTemp = [self.coachArray objectAtIndex:indexPath.row];
       // NSLog(@"ShooterTemp: %@", shooterTemp);
        UILabel *cellTitle2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 7, 180, 30)];
        [cell addSubview: cellTitle2];
        [cellTitle2 setText: [NSString stringWithFormat:@"%@ %@", shooterTemp[@"fName"], shooterTemp[@"lName"]]];

    }
    else if(tableView == self.finalShootersTableView){  //Tableview at the bottom of the page that has the selected shooters.
        
        UIStepper *stepper;
        UILabel *handicap;
        UILabel *position;
        UILabel *cellTitle3;
        if([self.typeOfShoot isEqualToString:@"Handicaps"]){
            
            
            position = [[UILabel alloc] initWithFrame:CGRectMake(28, 23, 80, 25)];
           cellTitle3 = [[UILabel alloc] initWithFrame:CGRectMake(8, 4, 180, 30)];
            
            //NSLog(@"HERE for Stepper");
            int currentValue = [[currentStepperValues objectAtIndex:indexPath.row] intValue];
            
            stepper = [[UIStepper alloc] init];
            stepper.frame = CGRectMake(110, 17, 50, 30);
            stepper.transform = CGAffineTransformMakeScale(0.87, 0.67);
            stepper.minimumValue = 18;
            stepper.maximumValue = 27;
            stepper.value = currentValue;
            stepper.tag = indexPath.row;
            

            
             
            [cell.contentView addSubview: stepper];
            [stepper addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
            
            
            
            [[labelArray objectAtIndex:indexPath.row] setFrame:CGRectMake(131, 2, 80, 18)];
            [[labelArray objectAtIndex:indexPath.row] setFont:[UIFont boldSystemFontOfSize: 16]];
            [[labelArray objectAtIndex:indexPath.row] setTextColor:[UIColor darkGrayColor]];
            [[labelArray objectAtIndex:indexPath.row] setText: [NSString stringWithFormat:@"%d yrds", currentValue]];
            [cell.contentView addSubview: [labelArray objectAtIndex:indexPath.row]];
    
        }
        else{ //Not Handicaps
            
             position = [[UILabel alloc] initWithFrame:CGRectMake(180, 11, 80, 25)];
            cellTitle3 = [[UILabel alloc] initWithFrame:CGRectMake(13, 7, 180, 30)];
            
            
        }
        
        
        [cell addSubview: cellTitle3];
        [cell setBackgroundColor: [UIColor clearColor]];
        UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iTrappLittleBlueRect.png"]];
        [background setFrame:CGRectMake(0, 0, 285, 44)];
        [background setContentMode:UIViewContentModeScaleAspectFill];
        
    
        
      
        
            
        [cell addSubview:background];
        [cell sendSubviewToBack:background];
            
    
        

        
        if([[self.shootersSelectedArray objectAtIndex:indexPath.row] isKindOfClass: [NSNumber class]]) {
        
            handicap.hidden = YES;
            [[labelArray objectAtIndex:indexPath.row] setHidden:YES];
            [[labelArray objectAtIndex:indexPath.row] setText: [NSString stringWithFormat:@"%1.0f yrds", stepper.value]];
            [currentStepperValues replaceObjectAtIndex:indexPath.row withObject: [NSNumber numberWithInt:0]];
            [cellTitle3 setText: @""];
            stepper.hidden = YES;
            
        }
        else
        {
         
            handicap.hidden = NO;
            stepper.hidden = NO;
            [[labelArray objectAtIndex:indexPath.row] setHidden:NO];
            PFObject *shooterTemp = [PFObject objectWithClassName:@"Shooter"];
            shooterTemp = [self.shootersSelectedArray objectAtIndex:indexPath.row];
            
            
            [cell addSubview:position];
            [position setText:[NSString stringWithFormat:@"Post #%ld", (long)(indexPath.row + 1) ]];
            [position setFont:[UIFont systemFontOfSize:14]];
            [position setTextColor:[UIColor darkGrayColor]];
            
            
            [cellTitle3 setText: [NSString stringWithFormat:@"%@ %@", shooterTemp[@"fName"], shooterTemp[@"lName"]]];

        }
        
        
    }
    
    return cell;
}


- (IBAction)valueChanged:(UIStepper *)sender {

    
      double value = [sender value];
    
        [[labelArray objectAtIndex:sender.tag] setText:[NSString stringWithFormat:@"%0.0f yrds", value]];
    [self.currentStepperValues replaceObjectAtIndex:sender.tag withObject:[NSNumber numberWithDouble:value]];
    

  
    //NSLog(@"VALUE: %f", value);
  
    
    
    // [self.finalShootersTableView reloadData];

    
    //[label setText:[NSString stringWithFormat:@"%d", (int)value]];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //NSLog(@"Counter %d", counter);
    
    UITableViewCell *thisCell = [tableView cellForRowAtIndexPath:indexPath];
    if(tableView == self.finalShootersTableView){
        
        self.finalShootersTableView.editing = YES;
        if(thisCell.accessoryType == UITableViewCellAccessoryNone) {
            
            thisCell.showsReorderControl = YES;
        }

        
    }
    
    else if(counter == 1){
        if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
      
        thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
            PFObject *coachTemp = [coachArray objectAtIndex:indexPath.row];
        coachTemp = [coachArray objectAtIndex:indexPath.row];
        self.selectedCoach = coachTemp;
        self.coachNameLabel.text = [NSString stringWithFormat:@"%@ %@",coachTemp[@"fName"], coachTemp[@"lName"]];
        [self getCoach];
            
    
        
        }else{
    	thisCell.accessoryType = UITableViewCellAccessoryNone;
        self.coachNameLabel.text = @"";
          
        
        }
    }else if(counter == 2){
        
        //SET THE CHECk MARK NUMBER
        if (thisCell.accessoryType == UITableViewCellAccessoryNone) {
            //NSLog(@"AT NOt Accessory");
            if(self.shootersCounter < 6){
      
                    //UILabel *counterAccessory = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 45, 20)];
                    //counterAccessory.text = [NSString stringWithFormat:@"%d", self.shootersCounter];
                    //[counterAccessory sizeToFit];
                    thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
              
                    [self.checkmarksArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInt:1]];
                
           
                
                
                    self.shooterCounterLabel.text = [NSString stringWithFormat: @"Shooters Selected: %d/5", self.shootersCounter];
                    // thisCell.accessoryType = UITableViewCellAccessoryCheckmark;
                    [self.shootersSelectedArray addObject:[self.coachArray objectAtIndex: indexPath.row]];
                
                if([[self.coachArray objectAtIndex: indexPath.row][@"handicap"] intValue] > 18){
                    //NSLog(@"HEREHERE");
                    [currentStepperValues replaceObjectAtIndex:(shootersCounter - 1) withObject: [self.coachArray objectAtIndex: indexPath.row][@"handicap"]];
                }
                //NSLog(@"CurrentSteppValues: %@", self.currentStepperValues);
                
                    //NSLog(@"%lu", (unsigned long)[self.shootersSelectedArray count]);
                
                    self.shootersCounter++;
                
                }
           
            }
            
        else{
            //NSLog(@"AT Accessory");
           
            
            if(self.shootersCounter != 0){
                
                thisCell.accessoryType = UITableViewCellAccessoryNone;
            [self.checkmarksArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInt:0]];
                [thisCell setSelected:NO];
                //NEED TO CHECK THE SHOOTERS ARRAY AND REMOVE THE RIGHT ONE IF ONE IS UNSELECTED
                [self.shootersSelectedArray removeObjectIdenticalTo: [self.coachArray objectAtIndex: indexPath.row]];
                
               /* if([[[self.coachArray objectAtIndex: indexPath.row] handicap] intValue] > 18){
                    
                    [currentStepperValues replaceObjectAtIndex:shootersCounter withObject: [[self.coachArray objectAtIndex: indexPath.row] handicap]];
                }*/

                
                [self.finalShootersTableView reloadData];
                self.shootersCounter--;
                 self.shooterCounterLabel.text = [NSString stringWithFormat: @"Shooters Selected: %d/5", (self.shootersCounter - 1)];
              
                
                
            }
            
        }

    }
    
}



- (IBAction)cancelClicked:(id)sender {
    
    self.pracCompSegmentContol.hidden = NO;
    self.dateLabel.hidden = NO;
    
    if([sender tag] == 1){ // Main Cancel
    
        [self presentViewController: scoringPage animated:YES completion:nil];
    }
    else if ([sender tag] == 2){  //Select Shooters
        
        //NSLog(@"selectedShooters: %lu", (unsigned long)[self.shootersSelectedArray count]);
        [self.beginShootOutlet setEnabled:YES];
        //[self.shootersSelectedArray removeAllObjects];
       self.selectCoachTableView.tag = 0;
        self.counter = 0;
        
        if(self.shootersSelectedArray.count < 5 && self.shootersSelectedArray.count > 0){
            
            for(int j = self.shootersSelectedArray.count; j < 5 ; j++){
                
                //NSLog(@"X: %d", j);
                NSNumber *j = [NSNumber numberWithInt:0];
                [self.shootersSelectedArray addObject: j];
                
                
            }
        }
        
        [self.finalShootersTableView reloadData];

        self.beginShootOutlet.title = @"Begin Shoot!";
        self.navBar.topItem.title = @"Setup Shoot";
        if(self.view.frame.size.width > 320){
            
            self.navBar.topItem.prompt = @"  ";
            
        }
        else{
            
            // self.navBar.topItem.prompt = nil;
        }
       // [self.navBar.topItem setPrompt:@"  "];
        self.shootersCounter = 1;
        [self.beginShootOutlet setEnabled: YES];
       
        self.beginShootOutlet.tag = 1;
        self.cancelSetUpButton.tag = 1;
        
        for(UIView *view in self.selectCoachView.subviews){
            if ([view isKindOfClass:[UIView class]]) {
                [view removeFromSuperview];
            }
        }
        
        [UIView transitionWithView:self.container
                          duration:0.6
                           options:UIViewAnimationOptionTransitionFlipFromTop
                        animations:^{
                            [self.selectCoachView removeFromSuperview];
                          
                        }
                        completion:NULL];
        
        [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
        
        
        
        self.dragShootersLabel.hidden = NO;
        [self.finalShootersTableView reloadData];
        
       
            
    }
    else{ //Add trap Range View
        
       
        [self.beginShootOutlet setEnabled:YES];
        self.beginShootOutlet.title = @"Begin Shoot!";
        self.navBar.topItem.title = @"Setup Shoot";
        if(self.view.frame.size.width > 320){
            
            self.navBar.topItem.prompt = @"  ";
            
        }
        else{
            
            // self.navBar.topItem.prompt = nil;
        }
       // [self.navBar.topItem setPrompt:@"  "];
        [self.beginShootOutlet setEnabled: YES];
        self.beginShootOutlet.tag = 1;
        self.cancelSetUpButton.tag = 1;
        
        for(UIView *view in self.addTrapRangeView.subviews){
            if ([view isKindOfClass:[UIView class]]) {
                [view removeFromSuperview];
            }
        }
        
        [UIView transitionWithView:self.container
                          duration:0.6
                           options:UIViewAnimationOptionTransitionFlipFromTop
                        animations:^{
                            [self.addTrapRangeView removeFromSuperview];
                            
                        }
                        completion:NULL];
        
        [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
        
    }
}



-(void) doneClickedOnAddShooters{
    
    self.beginShootOutlet.title = @"Begin Shoot!";
    
    if(self.view.frame.size.width > 320){
        
        self.navBar.topItem.prompt = @"  ";
        
    }
    else{
        
        // self.navBar.topItem.prompt = nil;
    }
   // self.navBar.topItem.prompt = @" ";
    self.pracCompSegmentContol.hidden = NO;
    self.dateLabel.hidden = NO;
    self.beginShootOutlet.tag = 1;
    self.beginShootOutlet.enabled = NO;
    self.cancelSetUpButton.tag = 1;
    self.finalShootersTableView.delegate = self;
    self.finalShootersTableView.dataSource = self;
    self.shootersCounter = 1;
    self.finalShootersTableView.hidden = NO;
    [self.selectCoachTableView removeFromSuperview];
    
    if(self.shootersSelectedArray.count == 0){
        
        [self.shootersSelectedArray removeAllObjects];
        
    }
    for(UIView *view in self.selectCoachView.subviews){
        if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.selectCoachView removeFromSuperview];
                      
                        
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    
   
    self.numberOfShootersSelected = self.shootersSelectedArray.count;
    
    //NSLog(@"selectedShootersArray1: %@", self.shootersSelectedArray);
    
    if(self.shootersSelectedArray.count < 5 && self.shootersSelectedArray.count > 0){
        
        for(int j = self.shootersSelectedArray.count; j < 5 ; j++){
            
                //NSLog(@"X: %d", j);
                NSNumber *j = [NSNumber numberWithInt:0];
                [self.shootersSelectedArray addObject: j];
           
            
        }
        
        //NSLog(@"selectedShootersArray2: %@", self.shootersSelectedArray);
        
        
    }
    
     self.counter = 0;
    /*[self.coachArray removeAllObjects];
    self.coachArray = [[Coach MR_findAll] mutableCopy];*/


    //[self.selectCoachTableView reloadData];
    [self.finalShootersTableView reloadData];
    self.beginShootOutlet.enabled = YES;
    self.finalShootersTableView.editing = YES;
    
}

-(NSString*) getDate {
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    
    dateString = [formatter stringFromDate:[NSDate date]];

    return dateString;
}


- (IBAction)selectCoach:(id)sender {
    
    
   
   /* [self.coachArray removeAllObjects];
    self.coachArray = [[Coach MR_findAll] mutableCopy];*/

     self.beginShootOutlet.title = @"Begin Shoot!";

    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpSelectCoachView];
                        [self.container addSubview: self.selectCoachView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion: nil];
     self.counter = 1;

    
}


-(void) setUpSelectCoachView{
    
    
    
    [self.coachArray removeAllObjects];
    
    PFQuery *coachQuery = [PFQuery queryWithClassName:@"Coach"];
    [coachQuery fromLocalDatastore];
    [coachQuery whereKey:@"userId" containsString:[PFUser currentUser].objectId];
    [self.coachArray addObjectsFromArray: [coachQuery findObjects]];

  //  NSLog(@"Coach Array: %@", updateData.coachesArray);
    //self.coachArray = [[Coach MR_findAll] mutableCopy];

    self.navBar.topItem.title = @"Select Coach";
   
    self.counter = 1;
 
    [self.selectCoachTableView reloadData];
    if(self.view.frame.size.width > 320){
        
        [self.container setFrame: CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height)];
        
    }
    else{
        
       [self.container setFrame: CGRectMake(0, 52, self.view.frame.size.width, self.view.frame.size.height)];
    }
    
    [self.selectCoachView setFrame:self.container.bounds];
   
    
    self.selectCoachTableView.tag = 1;
    [self.selectCoachTableView setFrame: CGRectMake(0, 0, self.container.frame.size.width, (self.container.frame.size.height - 55))];
    self.cancelSetUpButton.tag = 2;
    self.beginShootOutlet.tag = 2;
    [self.beginShootOutlet setEnabled:NO];
    
    
   [self.selectCoachTableView deselectRowAtIndexPath:[self.selectCoachTableView indexPathForSelectedRow] animated:NO];
    
    

    [self.selectCoachView setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.selectCoachView addSubview:self.selectCoachTableView];
   // NSLog(@"Counter:  %d", counter);
    self.counter = 1;
    [self.selectCoachTableView reloadData];
}

-(void) getCoach {
    
    [UIView transitionWithView:self.container
                      duration:0.6
                       options:UIViewAnimationOptionTransitionFlipFromTop
                    animations:^{
                        [self.selectCoachView removeFromSuperview];
                        [self.selectCoachTableView removeFromSuperview];
                        [self.beginShootOutlet setEnabled:YES];
                    }
                    completion:NULL];
    
    [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
    
    self.selectCoachTableView.tag = 0;
    self.navBar.topItem.title = @"Setup Shoot";
    self.counter = 1;
    self.cancelSetUpButton.tag = 1;
    self.beginShootOutlet.tag = 1;
}
- (IBAction)selectShooters:(id)sender {
    
    self.counter = 2;
     self.shootersCounter = 1;
    
    [self.shootersSelectedArray removeAllObjects];
    //NSLog(@"%lu", (unsigned long)self.shootersSelectedArray.count);
    [self.selectCoachTableView reloadData];

    
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpSelectShooterView];
                        [self.container addSubview: self.selectCoachView];
                        [self.view bringSubviewToFront:self.container];
                        self.finalShootersTableView.hidden = NO;
                        self.dragShootersLabel.hidden = NO;
                        
                    }
                    completion: nil];
    
   
    
}


-(void) setUpSelectShooterView {
    
    //self.counter = 2;
    self.pracCompSegmentContol.hidden = YES;
    self.dateLabel.hidden = YES;
    self.navBar.topItem.title = @"Select Shooters";
    self.navBar.topItem.prompt = @"Select up to 5 shooters";
    self.beginShootOutlet.title = @"DONE";
    
    if(self.view.frame.size.width > 320){
        
        [self.container setFrame: CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height)];
        
    }
    else{
        
         [self.container setFrame: CGRectMake(0, 52, self.view.frame.size.width, self.view.frame.size.height)];
    }
   
    [self.selectCoachView setFrame:self.container.bounds];
    
    [self.selectCoachTableView setFrame: CGRectMake(0, 30, self.container.frame.size.width, (self.container.frame.size.height - 85))];
    //[self.selectCoachTableView setBounces:NO];
   // self.selectCoachTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //[self.selectCoachTableView setPagingEnabled:YES];
       [self.selectCoachTableView setScrollEnabled:YES];
    
    self.shooterCounterLabel = [[UILabel alloc] initWithFrame: CGRectMake(60, 3 , 200, 28)];
    [self.shooterCounterLabel setTextAlignment:NSTextAlignmentCenter];
    [self.shooterCounterLabel setFont:[UIFont boldSystemFontOfSize:16]];
    self.shooterCounterLabel.text = [NSString stringWithFormat: @"Shooters Selected: %d/5", 0];
   self.cancelSetUpButton.tag = 2;
    self.beginShootOutlet.tag = 2;
    [self.coachArray removeAllObjects];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Shooter"];
    [query fromLocalDatastore];
    [query whereKey:@"userId" containsString:[PFUser currentUser].objectId];
    [self.coachArray addObjectsFromArray: [query findObjects]];
    
    NSLog(@"Coach Array: %@", self.coachArray);
    for(int x = 0; x < self.coachArray.count; x++)
    {
        [self.checkmarksArray insertObject: [NSNumber numberWithInt:0] atIndex:x];
        
    }
    //self.beginShootOutlet.tag = 2;
   [self.beginShootOutlet setEnabled:YES];
    //[self.shootersTableView deselectRowAtIndexPath:[self.shootersTableView indexPathForSelectedRow] animated:NO];
    //[self.selectCoachView addSubview:selectCoachTableView];
    //[self.selectShootersView setBackgroundColor:[UIColor lightGrayColor]];
    
    [self.selectCoachView addSubview: self.shooterCounterLabel];
    [self.selectCoachView addSubview:self.selectCoachTableView];
    [self.selectCoachTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return 44;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

-(UITableViewCellEditingStyle) tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return UITableViewCellEditingStyleNone;
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    
    id tempData = [shootersSelectedArray objectAtIndex: sourceIndexPath.row];
    id tempData2 = [currentStepperValues objectAtIndex:sourceIndexPath.row];
    [shootersSelectedArray replaceObjectAtIndex:sourceIndexPath.row withObject:[shootersSelectedArray objectAtIndex:destinationIndexPath.row]];
    [currentStepperValues replaceObjectAtIndex:sourceIndexPath.row withObject:[currentStepperValues objectAtIndex:destinationIndexPath.row]];
    
   [shootersSelectedArray replaceObjectAtIndex: destinationIndexPath.row withObject:tempData];
    [currentStepperValues replaceObjectAtIndex: destinationIndexPath.row withObject:tempData2];
    
   // [shootersSelectedArray replaceObjectAtIndex: sourceIndexPath.row withObject: [shootersSelectedArray objectAtIndex: destinationIndexPath.row]];
    //NSLog(@"SelectedShooters3: %@", self.shootersSelectedArray);
    
    [self.finalShootersTableView reloadData];
    
}

#pragma mark PICKERVIEW DELEGATE

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    if(pickerView == self.trapRangePicker){
        
        return 2;
    }
    else{
       
        return 1;
    }
    
}



// Total rows in our component.
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(pickerView == self.trapRangePicker){
        
        if(component == 0){
            return [self.statesArray count];
        }
        else{
            return [self.trapRanges count];
        }
        
    }
    else if(pickerView == self.trapHousePicker) //Trap house Picker
    {
        
        return [self.trapHouseNumbers count];
        
    }
    else if(pickerView == self.handicapPicker){
        
        return [self.handicapNumbersArray count];
        
    }
    else{
        return [self.statesArray count];
    }
    
}
/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = [[NSString alloc] init];
    
    if(pickerView == self.trapRangePicker){
        
        if(component == 0){
             title = [self.statesArray objectAtIndex:row];
        }
        else{
            TrapField *temp = [self.trapRanges objectAtIndex:row];
            title = temp.name;
            
        }
       
    }
    else //Trap house Picker
    {
        title = [self.trapHouseNumbers objectAtIndex:row];
        
    }
    
     return title;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
 {
 return (80.0);
 }*/

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if(pickerView == self.trapRangePicker){
        
        if(component == 0){
            
            //Need to check size and add an object that says there are no fields here so it doesnt crash
            NSString *stateTemp = [self.statesArray objectAtIndex:row];
            self.stateSelected = (int)row;
            [self.trapRanges removeAllObjects];
            PFQuery *trapQuery = [PFQuery queryWithClassName:@"TrapField"];
            [trapQuery orderByAscending:@"Name"];
            [trapQuery fromLocalDatastore];
            [trapQuery whereKey:@"State" containsString:stateTemp];
            self.trapRanges = [[trapQuery findObjects] mutableCopy];
            //self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:stateTemp andOrderBy:@"name" ascending:YES] mutableCopy];
            if(self.trapRanges.count == 0)
            {
                //[self.trapRanges removeAllObjects];
                [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", stateTemp]];
                self.trapRangeLabel.text = @"None Selected";
                [self.trapRangePicker reloadAllComponents];
            }
            else{
                
                
                PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
                temp = [self.trapRanges objectAtIndex:0];
                self.trapRangeLabel.text = temp[@"Name"];
                [self.trapRangePicker reloadAllComponents];
            }
            
        }
        else{
            
            if([[self.trapRanges objectAtIndex:row] isKindOfClass:[PFObject class]] )
            {
                PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
                temp = [self.trapRanges objectAtIndex:row];
                self.trapRangeLabel.text = temp[@"Name"];
                [self.trapRangePicker reloadAllComponents];
            }
            else{
                
                self.trapRangeLabel.text = @"None Selected";
            }

            
        }

    }
    else if(pickerView == self.trapHousePicker) //Trap house Picker
    {
        self.trapHouseLabel.text = [NSString stringWithFormat:@"%@", [self.trapHouseNumbers objectAtIndex:row]];
        
    }
    else if(pickerView == self.handicapPicker){ //handicap Picker
        
        self.handicapLabel.text = [NSString stringWithFormat:@"%@", [self.handicapNumbersArray objectAtIndex:row]];
        
    }
    else{ //statepicker
      
        
        self.state.text = [NSString stringWithFormat:@"%@", [self.statesArray objectAtIndex:row]];
        
    }
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
   
            
            if(pickerView == self.trapRangePicker){
                
                if(component == 0){
                    
                    return 60;
                }
                else{
                    
                    return 260;
                    
                }
            }
            else{//If it is the trap house number picker or handicap
                
                return 33;
            }
}
  
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
	UILabel *pickerLabel = (UILabel *)view;
	// Reuse the label if possible, otherwise create and configure a new one
	if ((pickerLabel == nil) || ([pickerLabel class] != [UILabel class]))
    {  //newlabel
		
		pickerLabel = [[UILabel alloc] init];
		
		pickerLabel.backgroundColor = [UIColor clearColor];
        [pickerLabel sizeToFit];
    }
	pickerLabel.textColor = [UIColor blackColor];
    NSString*  text = @"";
    
    if(pickerView == self.trapRangePicker){
        
        if (component == 0)
        {
            [pickerLabel setFrame: CGRectMake(15.0, 0.0, 46, 32.0)];
            pickerLabel.textAlignment = NSTextAlignmentCenter;
            text = [self.statesArray objectAtIndex:row];
        }
        else
        {
            [pickerLabel setFrame: CGRectMake(50.0, 0.0, 270, 32.0)];
            pickerLabel.textAlignment = NSTextAlignmentCenter;
            if([[self.trapRanges objectAtIndex:row] isKindOfClass:[PFObject class]] )
            {
                PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
                temp = [self.trapRanges objectAtIndex:row];
                text = temp[@"Name"];
            }
            else{
                
                text = [self.trapRanges objectAtIndex:0];
            }
            
         
        }

    }
    else if(pickerView == self.trapHousePicker){
        
        text = [self.trapHouseNumbers objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    else if(pickerView == self.handicapPicker){
       
        text = [self.handicapNumbersArray objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    else{
        
        text = [self.statesArray objectAtIndex:row];
        //[pickerLabel setFrame: CGRectMake(5.0, 0.0, 46, 32.0)];
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        
    }
    pickerLabel.text = text;
    
	return pickerLabel;
}


- (IBAction)selectTrapRange:(id)sender {
    
    self.selectShootersButton.enabled = NO;
    self.selectTrapRangeOutlet.enabled = NO;
    self.selectCoachOutlet.enabled = NO;
    self.selectTrapHouseOutlet.enabled = NO;
    self.beginShootOutlet.enabled = NO;
    if(self.view.frame.size.width > 320){
        
         self.trapRangePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(215, 430, 380, 236)];
    }
    else{
        
         self.trapRangePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 300, 320, 216)];
        
    }
   
    self.trapRangePicker.delegate = self;
    self.selectHandicapOutlet.enabled = NO;
    self.trapRangePicker.dataSource = self;
    self.trapRangePicker.showsSelectionIndicator = YES;
    self.finalShootersTableView.hidden = YES;
    self.selectShootersButton.hidden = YES;
    self.dragShootersLabel.hidden = YES;
    self.doneOnPickerOutlet.hidden = NO;
    self.skipButtonOutlet.hidden = NO;
    self.pickerOpened = 1;
    self.addTrapRangeLabel.hidden = NO;
    self.dontSeeRangeLabel.hidden = NO;
    
    
    [self.trapRanges removeAllObjects];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL defaultOrNO = [userDefaults boolForKey:@"defaultOrNot"];
    if(defaultOrNO == NO)
    {
        
        //NSLog(@"No Default");
        NSString *stateTemp = [self.statesArray objectAtIndex:0];
        PFQuery *trapQuery = [PFQuery queryWithClassName:@"TrapField"];
        [trapQuery orderByAscending:@"Name"];
        [trapQuery fromLocalDatastore];
        [trapQuery whereKey:@"State" containsString:stateTemp];
        self.trapRanges = [[trapQuery findObjects] mutableCopy];
        
        //self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:stateTemp andOrderBy:@"name" ascending:YES] mutableCopy];
        if(trapRanges.count == 0)
        {
            
            [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", stateTemp]];
        }
    }
    else{
        
        //NSLog(@"Default");
        int tempState = (int)[userDefaults integerForKey:@"defaultState"];
        NSString *stateString = [self.statesArray objectAtIndex:tempState];
      
        PFQuery *trapQuery = [PFQuery queryWithClassName:@"TrapField"];
        [trapQuery orderByAscending:@"Name"];
        [trapQuery fromLocalDatastore];
        [trapQuery whereKey:@"State" containsString:stateString];
        self.trapRanges = [[trapQuery findObjects] mutableCopy];
       // self.trapRanges = [[TrapField MR_findByAttribute:@"state" withValue:stateString andOrderBy:@"name" ascending:YES] mutableCopy];
        [self.trapRangePicker selectRow:tempState inComponent:0 animated:YES];
        if(trapRanges.count == 0)
        {
            
            [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", stateString]];
        }
        
        
    }

    
    if(self.trapRanges.count == 1){
       
        if([[self.trapRanges objectAtIndex:0] isKindOfClass:[PFObject class]] )
        {
            
             PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
            temp = [self.trapRanges objectAtIndex:0];
            self.trapRangeLabel.text = temp[@"Name"];
        }
        else{
            
            self.trapRangeLabel.text = @"None Selected";
        }

    }
    else{
         PFObject *temp = [PFObject objectWithClassName:@"TrapField"];
        temp = [self.trapRanges objectAtIndex:0];
        self.trapRangeLabel.text = temp[@"Name"];
    }
    
    [self.view addSubview: self.trapRangePicker];
    [self.trapRangePicker reloadAllComponents];
}


//NEED TO ADD TAG CHECKING - 1 FOR HOUSE - 2 FOR HANDICAP
- (IBAction)selectTrapHouse:(id)sender {
    
    self.selectShootersButton.enabled = NO;
    self.selectTrapRangeOutlet.enabled = NO;
    self.selectCoachOutlet.enabled = NO;
    self.selectTrapHouseOutlet.enabled = NO;
    self.beginShootOutlet.enabled = NO;
    self.selectHandicapOutlet.enabled = NO;
    self.finalShootersTableView.hidden = YES;
    self.selectShootersButton.hidden = YES;
    self.dragShootersLabel.hidden = YES;
    self.doneOnPickerOutlet.hidden = NO;
    self.skipButtonOutlet.hidden = NO;

    
    if([sender tag] == 1){
    
        if(self.view.frame.size.width > 320){
            
            self.trapHousePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(215, 430, 380, 236)];
        }
        else{
            
            self.trapHousePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 300, 320, 216)];
            
        }
    self.trapHousePicker.delegate = self;
    self.trapHousePicker.dataSource = self;
        self.trapHousePicker.showsSelectionIndicator = YES;
    self.pickerOpened = 2;

    self.trapHouseLabel.text = [NSString stringWithFormat:@"%@", [self.trapHouseNumbers objectAtIndex:0]];
    [self.view addSubview: self.trapHousePicker];
    }
    else{//HANDICAP PICKER
        
        if(self.view.frame.size.width > 320){
            
            self.handicapPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(276, 430, 520, 216)];
        }
        else{
            
            self.handicapPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 260, 320, 216)];
            
        }
        self.handicapPicker.delegate = self;
        self.handicapPicker.dataSource = self;
        self.handicapPicker.showsSelectionIndicator = YES;
        self.pickerOpened = 3;
        
        self.handicapLabel.text = [NSString stringWithFormat:@"%@", [self.handicapNumbersArray objectAtIndex:0]];
        self.yrdsLabel.hidden = NO;
        [self.view addSubview: self.handicapPicker];
        
        
        
    }
    
    
}

- (IBAction)doneOnPicker:(id)sender {
    
    
    
    if(self.pickerOpened == 1){
        
        self.addTrapRangeLabel.hidden = YES;
        self.dontSeeRangeLabel.hidden = YES;
        //Prompts the user to set a default state
        self.trapRangeSelected = YES;
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        BOOL defaultOrNO = [userDefaults boolForKey:@"defaultOrNot"];
        if(defaultOrNO == NO)
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Default State" message:[NSString stringWithFormat:@"Would you like to set %@ as your default state?",[self.statesArray objectAtIndex:self.stateSelected] ] delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            alert.tag = 2;
            [alert show];
            
        }

        
    }
    else if(pickerOpened == 2){
        
        self.trapHouseNumberSelected = YES;
         self.finalShootersTableView.hidden = NO;
    }
    else{
        
        self.handicapSelected = YES;
    }
    
    self.selectShootersButton.enabled = YES;
    self.selectTrapRangeOutlet.enabled = YES;
    self.selectCoachOutlet.enabled = YES;
    self.selectTrapHouseOutlet.enabled = YES;
    self.beginShootOutlet.enabled = YES;
    self.selectHandicapOutlet.enabled = YES;
    self.finalShootersTableView.hidden = NO;
    self.selectShootersButton.hidden = NO;
    self.dragShootersLabel.hidden = NO;
    self.doneOnPickerOutlet.hidden = YES;
    self.skipButtonOutlet.hidden = YES;
    [self.trapHousePicker removeFromSuperview];
    [self.trapRangePicker removeFromSuperview];
    [self.handicapPicker removeFromSuperview];
    
}
- (IBAction)skipTrapRange:(id)sender {
    
    if(self.pickerOpened == 1){
        
        self.trapRangeSelected = YES;
        self.trapRangeLabel.text = @"None Selected";
        
        self.addTrapRangeLabel.hidden = YES;
        self.dontSeeRangeLabel.hidden = YES;
        self.selectShootersButton.enabled = YES;
        self.selectTrapRangeOutlet.enabled = YES;
        self.selectCoachOutlet.enabled = YES;
        self.selectTrapHouseOutlet.enabled = YES;
        self.beginShootOutlet.enabled = YES;
        self.selectHandicapOutlet.enabled = YES;
        self.finalShootersTableView.hidden = NO;
        self.selectShootersButton.hidden = NO;
        self.dragShootersLabel.hidden = NO;
        self.doneOnPickerOutlet.hidden = YES;
        self.skipButtonOutlet.hidden = YES;
        [self.trapRangePicker removeFromSuperview];

    }
    else{
        
        
        self.trapHouseLabel.text = @"0";
        
        self.addTrapRangeLabel.hidden = YES;
        self.dontSeeRangeLabel.hidden = YES;
        self.selectShootersButton.enabled = YES;
        self.selectTrapRangeOutlet.enabled = YES;
        self.selectCoachOutlet.enabled = YES;
        self.selectTrapHouseOutlet.enabled = YES;
        self.beginShootOutlet.enabled = YES;
        self.selectHandicapOutlet.enabled = YES;
        self.finalShootersTableView.hidden = NO;
        self.selectShootersButton.hidden = NO;
        self.dragShootersLabel.hidden = NO;
        self.doneOnPickerOutlet.hidden = YES;
        self.skipButtonOutlet.hidden = YES;
        [self.trapHousePicker removeFromSuperview];

        
        
    }

    
    
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == 2){
        
        if(buttonIndex == 0){
            
            //NSLog(@"0");
            
        }
        else{  //YES clicked on Default state for Trap Ranges
            
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setBool:YES forKey:@"defaultOrNot"];
            [userDefaults setInteger:self.stateSelected forKey:@"defaultState"];
        }
        
    }
    if(alertView.tag == 3){
        
        if(buttonIndex == 0){
            
            
        }
        else{
            
            //NSLog(@"GOT HEEERE");
            NSString *trapFieldId;
            if([self.trapRangeLabel.text isEqualToString:@"None Selected"])
            {
                trapFieldId = @"None Selected";
                
            }
            else{
                
                 PFObject *trapField = [PFObject objectWithClassName:@"TrapField"];
                PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
                [query whereKey:@"Name" containsString: self.trapRangeLabel.text];
                [query fromLocalDatastore];
                trapField = [query getFirstObject];
                trapFieldId = trapField.objectId;
            }
            
            NSMutableDictionary *defaultDictionary = [[NSMutableDictionary alloc] init];
            [defaultDictionary setObject:self.selectedCoach[@"appObjectId"] forKey:@"coach"];
            [defaultDictionary setObject:[NSNumber numberWithInt:(int)[self.tradOrAdvanced selectedSegmentIndex]] forKey: @"scoringType"];
            [defaultDictionary setObject:trapFieldId forKey:@"range"];
            [defaultDictionary setObject:self.trapHouseLabel.text forKey:@"house"];
           // [defaultDictionary setObject:self.handicapLabel.text forKey:@"distance"];
            
            [[NSUserDefaults standardUserDefaults] setObject:defaultDictionary forKey:@"defaultDictionary"];
            
            
        }
        
    }
}
- (IBAction)addTrapRange:(id)sender {
    
    //NSLog(@"Add Trap Range");
        
    self.counter = 3;
    
    self.beginShootOutlet.title = @"Begin Shoot!";
    
    [UIView transitionWithView:self.container
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromBottom
                    animations:^{
                        
                        [self setUpAddTrapRangeView];
                        [self.container addSubview: self.addTrapRangeView];
                        [self.view bringSubviewToFront:self.container];
                        
                    }
                    completion: nil];
 
}

-(void) setUpAddTrapRangeView {
    
   
    self.pracCompSegmentContol.hidden = YES;
    self.dateLabel.hidden = YES;
    self.navBar.topItem.title = @"Add Trap Range";
    if(self.view.frame.size.width > 320){
        
        self.navBar.topItem.prompt = @"  ";
        
    }
    else{
        
       // self.navBar.topItem.prompt = nil;
    }

    //self.beginShootOutlet.title = @"DONE";
    [self.container setFrame: CGRectMake(0, 55, self.view.frame.size.width, self.view.frame.size.height)];
    [self.addTrapRangeView setFrame:self.container.bounds];
   // [self.addTrapRangeView setBackgroundColor:[UIColor lightGrayColor]];
    [self.addTrapRangeView setFrame: CGRectMake(0, 0, self.container.frame.size.width, self.container.frame.size.height)];
    self.addRangeCounter = 1;
    self.cancelSetUpButton.tag = 3;
    self.beginShootOutlet.tag = 3;
    [self.beginShootOutlet setEnabled:NO];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"SmallerBackground.jpg"]];
    [imageView setFrame:CGRectMake(0, 0, self.addTrapRangeView.frame.size.width, self.addTrapRangeView.frame.size.height)];
    [self.addTrapRangeView addSubview:imageView];
    [self.addTrapRangeView sendSubviewToBack:imageView];
    
    
   /* UILabel *internetLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 3, 280, 25)];
    [internetLabel setTextAlignment:NSTextAlignmentCenter];
    [internetLabel setText:@"(Internet Connection Required)"];
    [internetLabel setTextColor:[UIColor darkGrayColor]];*/
    
    
    //NEED TO WORK ON THIS FOR IPAD
    
    if(self.view.frame.size.width > 320)
    {
        rangeName = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 15, 25, 280, 50)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 18 , 80, 285, 50)];
        street = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 -15, 135, 280, 50)];
        city = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 15, 190, 280, 50)];
        state = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 15, 245, 280, 50)];
        zipCode= [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width/3 - 15, 300, 280, 50)];
        addRangeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2.4 - 15, 355, 150, 50)];
    }
    else{
        rangeName = [[UITextField alloc] initWithFrame:CGRectMake(20, 5, 280, 50)];
        phoneNum = [[UITextField alloc] initWithFrame:CGRectMake(16, 55, 287, 50)];
        street = [[UITextField alloc] initWithFrame:CGRectMake(20, 105, 280, 50)];
        city = [[UITextField alloc] initWithFrame:CGRectMake(20, 155, 280, 50)];
        state = [[UITextField alloc] initWithFrame:CGRectMake(20, 205, 280, 50)];
        zipCode= [[UITextField alloc] initWithFrame:CGRectMake(20, 255, 280, 50)];
        addRangeButton = [[UIButton alloc] initWithFrame:CGRectMake(85, 305, 150, 50)];
    
    }
    
  
    [rangeName setKeyboardType:UIKeyboardTypeDefault];
    [rangeName setReturnKeyType:UIReturnKeyNext];
    self.rangeName.delegate = self;
    [rangeName setBackgroundColor:[UIColor clearColor]];
    [rangeName setBorderStyle:UITextBorderStyleNone];
    [rangeName setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [rangeName setPlaceholder:@"Range Name"];
    [rangeName setTextAlignment:NSTextAlignmentCenter];
    [rangeName addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [phoneNum setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    phoneNum.delegate = self;
    [phoneNum setReturnKeyType:UIReturnKeyNext];
    [phoneNum setBackgroundColor:[UIColor clearColor]];
    [phoneNum setBorderStyle:UITextBorderStyleNone];
    [phoneNum setBackground:[UIImage imageNamed:@"iTRAPPphone.png"]];
    [phoneNum setPlaceholder:@"Phone Number"];
    [phoneNum setTextAlignment:NSTextAlignmentCenter];
    [phoneNum addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [street setKeyboardType:UIKeyboardTypeDefault];
    street.delegate = self;
    [street setReturnKeyType:UIReturnKeyNext];
    [street setBackgroundColor:[UIColor clearColor]];
    [street setBorderStyle:UITextBorderStyleNone];
    [street setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [street setPlaceholder:@"Street Address"];
    [street setTextAlignment:NSTextAlignmentCenter];
    [street addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [city setKeyboardType:UIKeyboardTypeDefault];
    [city setReturnKeyType:UIReturnKeyNext];
    city.delegate = self;
    [city setBackgroundColor:[UIColor clearColor]];
    [city setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [city setBorderStyle:UITextBorderStyleNone];
    [city setPlaceholder:@"City"];
    [city setTextAlignment:NSTextAlignmentCenter];
    [city addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];
    
    [state setReturnKeyType:UIReturnKeyDone];
    [state setBackgroundColor:[UIColor clearColor]];
    [state setBorderStyle:UITextBorderStyleNone];
    [state setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [state setPlaceholder:@"State"];
    [state setTextAlignment:NSTextAlignmentCenter];
     self.state.delegate = self;
    [state addTarget:self action:@selector(addSelectState) forControlEvents:UIControlEventEditingDidBegin];
    [state addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidBegin];
    UIView *dummyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    [dummyView setBackgroundColor:[UIColor clearColor]];
    [self.state setInputView: dummyView];
   
    UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAll)];
    NSArray *array = [[NSArray alloc] initWithObjects:tapGesture, nil];
    [self.addTrapRangeView setGestureRecognizers: array];
    
    
    [zipCode setKeyboardType:UIKeyboardTypeNumbersAndPunctuation];
    [zipCode setReturnKeyType:UIReturnKeyDone];
    
    [zipCode setBackground:[UIImage imageNamed:@"iTRAPPlastName.png"]];
    [zipCode setBackgroundColor:[UIColor clearColor]];
    zipCode.delegate = self;
    [zipCode setBorderStyle:UITextBorderStyleNone];
    [zipCode setPlaceholder:@"Zip Code"];
    [zipCode setTextAlignment:NSTextAlignmentCenter];
    [zipCode addTarget:self action:@selector(nextOnKeyboard:) forControlEvents:UIControlEventEditingDidEndOnExit];

    
    
    
    
    [addRangeButton setTitle:@"Save" forState:UIControlStateNormal];
    [addRangeButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [addRangeButton setTitleColor:[UIColor colorWithRed:(226/255.f) green:(58/255.f) blue:(50/255.f) alpha:1.0] forState:UIControlStateNormal];
    [addRangeButton.titleLabel setFont:[UIFont boldSystemFontOfSize: 22]];
    [addRangeButton addTarget:self action:@selector(addRange) forControlEvents:UIControlEventTouchUpInside];
    [addRangeButton setEnabled:true];

    
    [self.addTrapRangeView addSubview:rangeName];
    [self.addTrapRangeView addSubview:phoneNum];
    [self.addTrapRangeView addSubview:street];
    [self.addTrapRangeView addSubview:city];
    [self.addTrapRangeView addSubview:state];
   // [self.addTrapRangeView addSubview:internetLabel];
    [self.addTrapRangeView addSubview:zipCode];
    [self.addTrapRangeView addSubview:addRangeButton];

    
}

-(void)addRange{
    
    
    //NSLog(@"addRange");
    
    if(addRangeCounter == 1)
    {
        
        if(self.rangeName.text.length > 0 && self.phoneNum.text.length > 0 && self.street.text.length > 0 && self.city.text.length > 0 && self.state.text.length > 0 && self.zipCode.text.length > 0){
        
            PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
            [query fromLocalDatastore];
            [query whereKey:@"Street" equalTo:self.street.text];
            [query whereKey:@"Name" equalTo:self.rangeName.text];
            [query whereKey:@"ZipCode" equalTo:self.zipCode.text];
            NSArray *queryArray = [query findObjects];
            if([queryArray count] != 0)
            {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Data Error" message:@"There is alredy a range matching these details." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
            }
            else{ //Save to Parse and CoreData
                
                [self.beginShootOutlet setEnabled:YES];
                self.beginShootOutlet.title = @"Begin Shoot!";
                self.navBar.topItem.title = @"Setup Shoot";
                if(self.view.frame.size.width > 320){
                    
                    self.navBar.topItem.prompt = @"  ";
                    
                }
                else{
                    
                    // self.navBar.topItem.prompt = nil;
                }
                //[self.navBar.topItem setPrompt:@"  "];
                [self.beginShootOutlet setEnabled: YES];
                self.beginShootOutlet.tag = 1;
                self.cancelSetUpButton.tag = 1;
                // self.addRangeCounter = 1;
    
                
                PFObject *trapField = [PFObject objectWithClassName: @"TrapField"];
                trapField[@"Name"] = self.rangeName.text;
                trapField[@"Phone"] = self.phoneNum.text;
                trapField[@"Street"] = self.street.text;
                trapField[@"City"] = self.city.text;
                trapField[@"State"] = self.state.text;
                trapField[@"ZipCode"] = self.zipCode.text;
                
                [trapField pin];
                [trapField saveEventually];
                

                
                for(UIView *view in self.addTrapRangeView.subviews){
                    if ([view isKindOfClass:[UIView class]]) {
                        [view removeFromSuperview];
                    }
                }
    
                [UIView transitionWithView:self.container
                                  duration:0.6
                                   options:UIViewAnimationOptionTransitionFlipFromTop
                                animations:^{
                                    [self.addTrapRangeView  removeFromSuperview];
                        
                                }
                                completion:NULL];
    
            [self.view performSelector: @selector(sendSubviewToBack:) withObject:self.container afterDelay:0.3];
                
                UIAlertView *success = [[UIAlertView alloc] initWithTitle:@"Success" message:@"The new trap range was saved successfully. Thank you!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [success show];
                
                [self.trapRanges removeAllObjects];
                PFQuery *query = [PFQuery queryWithClassName:@"TrapField"];
                [query fromLocalDatastore];
                [query whereKey:@"Name" containsString:[self.statesArray objectAtIndex:self.stateSelected]];
                self.trapRanges = [[query findObjects] mutableCopy];
                
                if(self.trapRanges.count == 0)
                {
                    //[self.trapRanges removeAllObjects];
                    [self.trapRanges addObject:[NSString stringWithFormat:@"No Listings for %@", [self.statesArray objectAtIndex:self.stateSelected] ]];
                    self.trapRangeLabel.text = @"None Selected";
                    [self.trapRangePicker reloadAllComponents];
                }

                [self.trapRangePicker reloadAllComponents];
                
                
            }
        }
        else{
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Missing Information" message:@"All fields must be filled out correctly before submitting." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            
        }
    }
    else{
    
        [self.statePicker removeFromSuperview];
        [self.view sendSubviewToBack: self.statePicker];
        [self.state setUserInteractionEnabled:YES];
        [self.state resignFirstResponder];
     
        [self.phoneNum setUserInteractionEnabled: YES];
        
        [self.city setUserInteractionEnabled:YES];
        [self.zipCode setUserInteractionEnabled:YES];
        [self.rangeName setUserInteractionEnabled:YES];
        [self.street setUserInteractionEnabled:YES];
        [self.city setUserInteractionEnabled:YES];
        //[self.zipCode becomeFirstResponder];
        rangeName.hidden = NO;
        phoneNum.hidden = NO;
        street.hidden = NO;
        city.hidden = NO;
        zipCode.hidden = NO;
        
        if(self.view.frame.size.width > 320){
            
             [self.addRangeButton setFrame:CGRectMake(self.view.frame.size.width/2.4 - 15, 355, 150, 50)];
            
        }
        else{
            
            [self.addRangeButton setFrame:CGRectMake(85, 305, 150, 50)];
        }
    
       
        

        self.addRangeCounter = 1;
        self.statePicker = nil;
        
    }
    
}

-(void) dismissAll {
    
    
    [self.rangeName resignFirstResponder];
    [self.phoneNum resignFirstResponder];
    [self.street resignFirstResponder];
    [self.state resignFirstResponder];
    [self.city resignFirstResponder];
    [self.zipCode resignFirstResponder];
}

-(void) nextOnKeyboard: (id) sender{

    //NSLog(@"NextOnKeyboard");
    if(sender == self.rangeName){
        
        [self.rangeName resignFirstResponder];
        [self.phoneNum becomeFirstResponder];
        
    }
    else if(sender == self.phoneNum){
        [self.phoneNum resignFirstResponder];
        [self.street becomeFirstResponder];
        
        
    }
    else if(sender == self.street){
        [self.street resignFirstResponder];
        [self.city becomeFirstResponder];
        
    }
    else if (sender == self.city){
        [self.addTrapRangeView endEditing:YES];
        [self.city resignFirstResponder];
        [self.state resignFirstResponder];
        [self addSelectState];
        
    }
    else if(sender == state){
        [self.city resignFirstResponder];
        [self.state resignFirstResponder];
        
    }
    else{//sender == zipCode
        
        [self.zipCode becomeFirstResponder];
        self.addRangeButton.titleLabel.text = @"Save";
        self.addRangeCounter = 1;
        [self.statePicker removeFromSuperview];
        
    }
}

-(void) addSelectState {
    
    
    if(addRangeCounter == 2){
        
        
    }
    else{
    
        rangeName.hidden = YES;
        phoneNum.hidden = YES;
        street.hidden = YES;
        city.hidden = YES;
        zipCode.hidden = YES;
        if(self.view.frame.size.width > 320){
            
            [self.addRangeButton setFrame:CGRectMake(305, 475, 150, 50)];
            
        }
        else{
         
            [self.addRangeButton setFrame:CGRectMake(self.state.frame.origin.x + (self.state.frame.size.width * 0.55), self.state.frame.origin.y, 150, 50)];
        }
        

        
    //NSLog(@"addSelectState");
    
    self.addRangeButton.titleLabel.text = @"Save";
    
    self.addRangeCounter = 2;
        
        if(self.view.frame.size.width > 320){
            
             self.statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(215, 270, 320, 216)];
        }
        else{
            
             self.statePicker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];
        }
   
    self.statePicker.delegate = self;
    self.statePicker.dataSource = self;
    self.statePicker.showsSelectionIndicator = YES;

    [self.state setUserInteractionEnabled:NO];
    self.state.text = [self.statesArray objectAtIndex:0];
        
    [self.addTrapRangeView endEditing:YES];
        
        
        [self.city setUserInteractionEnabled:NO];
        [self.phoneNum setUserInteractionEnabled:NO];
        [self.zipCode setUserInteractionEnabled:NO];
        [self.rangeName setUserInteractionEnabled:NO];
        [self.street setUserInteractionEnabled:NO];
        [self.city setUserInteractionEnabled:NO];
        [self.state resignFirstResponder];
        [self.addTrapRangeView addSubview:self.statePicker];
    }
    
}


-(BOOL)isNetworkAvailable
{
    char *hostname;
    struct hostent *hostinfo;
    hostname = "google.com";
    hostinfo = gethostbyname (hostname);
    if (hostinfo == NULL){
        //NSLog(@"-> no connection!\n");
        return NO;
    }
    else{
        //NSLog(@"-> connection established!\n");
        return YES;
    }
}


-(void) textFieldDidBeginEditing:(UITextField *)textField{
    
    if(textField == self.state){
   
        rangeName.hidden = YES;
        phoneNum.hidden = YES;
        street.hidden = YES;
        city.hidden = YES;
        zipCode.hidden = YES;
        self.addRangeCounter = 2;
        if(self.view.frame.size.width > 320){
            
            [self.statePicker setFrame:CGRectMake(215, 270, 320, 216)];
            [self.addRangeButton setFrame:CGRectMake(305, 475, 150, 50)];
            
        }
        else{
            
            [self.addRangeButton setFrame:CGRectMake(self.state.frame.origin.x + (self.state.frame.size.width * 0.55), self.state.frame.origin.y, 150, 50)];
  
            
        }
       
        
    }
}



-(void) typeOfShootSelected :(NSString *)typeOfShoot1 {
    
    
    self.typeOfShoot = typeOfShoot1;
    //NSLog(@"Type Of Shoot:%@", typeOfShoot);
    
    if([self.typeOfShoot isEqualToString:@"Doubles"]){
        
        self.dateLabel.hidden = YES;
        self.finalShootersTableView.hidden = YES;
        self.pracCompSegmentContol.hidden = YES;
        self.selectCoachOutlet.hidden = YES;
        self.coachNameLabel.hidden = YES;
        self.trapRangeLabel.hidden = YES;
        self.selectTrapRangeOutlet.hidden = YES;
        self.selectTrapHouseOutlet.hidden = YES;
        self.trapHouseLabel.hidden = YES;
        self.shotsInShootSegmentControl.hidden = YES;
        self.shotsPerStationSegementControl.hidden = YES;
        self.tradOrAdvanced.hidden = YES;
        self.yrdsLabel.hidden = YES;
        self.selectShootersButton.hidden = YES;
        self.beginShootOutlet.enabled = NO;
        self.shotsInShootLabelOutlet.hidden = YES;
        self.scoringTypeLabelOutlet.hidden = YES;
        self.shotsPerPostLabelOutlet.hidden = YES;
        self.roundTypeOutlet.hidden = YES;
        
        UILabel *comingSoon;
        UITextView *tempDisabled;
        if(self.view.frame.size.width > 320){
        
            
            comingSoon = [[UILabel alloc] initWithFrame:CGRectMake(240, 300, 280, 40)];
            tempDisabled = [[UITextView alloc] initWithFrame:CGRectMake(240, 340, 280, 140)];
        }
        else{
            
            comingSoon = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, 280, 40)];
            tempDisabled = [[UITextView alloc] initWithFrame:CGRectMake(20, 240, 280, 140)];
            
        }
  // comingSoon = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, 280, 40)];
        [comingSoon setText:@"Coming Soon!"];
        [comingSoon setTextAlignment:NSTextAlignmentCenter];
        [comingSoon setFont:[UIFont systemFontOfSize:26]];
        
        
        
      //  tempDisabled = [[UITextView alloc] initWithFrame:CGRectMake(20, 240, 280, 140)];
        [tempDisabled setBackgroundColor:[UIColor clearColor]];
        [tempDisabled setTextColor:[UIColor darkGrayColor]];
        [tempDisabled setFont:[UIFont systemFontOfSize:18]];
        [tempDisabled setTextAlignment:NSTextAlignmentCenter];
        [tempDisabled setText:@"Our team is working very hard to develop this feature!  Please check for iTrapp updates regularly as we will have this up and running in no time."];
        [self.view addSubview:tempDisabled];
        [self.view  addSubview:comingSoon];
        
        
    }
    else if([self.typeOfShoot isEqualToString:@"Singles"]){
        
      
        
        
        
    }
    else{  //Handicaps view
        
        //NSLog(@"HANDICAPS");
    
    }
}





@end
