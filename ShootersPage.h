//
//  ShootersPage.h
//  ClayCounter
//
//  Created by Test on 1/8/14.
//  Copyright (c) 2014 Activate Innovation. All rights reserved.
//
#import "SettingsPage.h"
#import "ScoringPage.h"
#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import "Shooter.h"
#import <ParseUI/ParseUI.h>
#import "UpdatedData.h"
#import <MSCMoreOptionTableViewCell/MSCMoreOptionTableViewCell.h>
#import <MessageUI/MFMailComposeViewController.h>

@class MSCMoreOptionTableViewCell;
@class ScoringPage;
@class SettingsPage;
@class AppDelegate;
@class UpdatedData;


@interface ShootersPage : UIViewController <UITabBarDelegate, UITableViewDelegate, UITableViewDataSource, PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate, UIAlertViewDelegate, UIScrollViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UpdateDataDelegate, MSCMoreOptionTableViewCellDelegate, MFMailComposeViewControllerDelegate>

@property UpdatedData *updatedData;
@property int numOfParseShooters;
@property int numOfSavedShooters;
@property BOOL connection;
@property (strong, nonatomic) NSIndexPath *indexPathToDelete;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) IBOutlet UINavigationItem *navBar;
@property AppDelegate *appDelegate;
- (IBAction)addShooter:(id)sender;
@property (strong, nonatomic) NSMutableArray *parseShooters;
@property (strong, nonatomic) NSMutableArray *shootersArray;
@property (strong, nonatomic) UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *addShooterOutlet;
@property (strong, nonatomic) UITextField *firstName;
@property (strong, nonatomic) UITextField *lastName;
@property (strong, nonatomic) PFObject *editShooter;
@property int editShooterIndex;
- (IBAction)cancelAddShooter:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *cancelAddShooterOutlet;
@property ScoringPage *scoringPage;
@property SettingsPage *settingsPage;
@property (strong, nonatomic) PFObject *shooterToDelete;
@property (strong, nonatomic) IBOutlet UITabBar *tabBar;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UISwitch *stayLoggedIn;
@property (strong, nonatomic) PFLogInViewController *logInViewController;
@property (strong, nonatomic) PFSignUpViewController *signUpViewController;
@property (strong, nonatomic) NSUserDefaults *userDefaults;
@property (strong, nonatomic) UIView *container;
@property (strong, nonatomic) UIView *addShooterView;
@property (strong, nonatomic) UITextField *phoneNum;
@property (strong, nonatomic) UITextField *emailAddress;
@property (strong, nonatomic) UITextField *emergencyContact;
@property (strong, nonatomic) UITextField *emergencyContactNum;
@property (strong, nonatomic) UISegmentedControl *chokeType;
@property (strong, nonatomic) UITextField *ataNumber;
@property (strong, nonatomic) UITextField *handicap;
@property (strong, nonatomic) UITextField *ataClass;
@property (strong, nonatomic) UITextField *ataCategory;
@property (strong, nonatomic) NSMutableArray *ataClassesArray;
@property (strong, nonatomic) NSMutableArray *ataCategoriesArray;
@property (strong, nonatomic) NSMutableArray *handicapNumbers;
@property (strong, nonatomic) UIPickerView *pickerView; 



@end

