
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 1
#define COCOAPODS_VERSION_PATCH_Bolts 4

// Facebook-iOS-SDK
#define COCOAPODS_POD_AVAILABLE_Facebook_iOS_SDK
#define COCOAPODS_VERSION_MAJOR_Facebook_iOS_SDK 3
#define COCOAPODS_VERSION_MINOR_Facebook_iOS_SDK 23
#define COCOAPODS_VERSION_PATCH_Facebook_iOS_SDK 2

// IQKeyboardManager
#define COCOAPODS_POD_AVAILABLE_IQKeyboardManager
#define COCOAPODS_VERSION_MAJOR_IQKeyboardManager 3
#define COCOAPODS_VERSION_MINOR_IQKeyboardManager 2
#define COCOAPODS_VERSION_PATCH_IQKeyboardManager 3

// MSCMoreOptionTableViewCell
#define COCOAPODS_POD_AVAILABLE_MSCMoreOptionTableViewCell
#define COCOAPODS_VERSION_MAJOR_MSCMoreOptionTableViewCell 2
#define COCOAPODS_VERSION_MINOR_MSCMoreOptionTableViewCell 2
#define COCOAPODS_VERSION_PATCH_MSCMoreOptionTableViewCell 0

// ODRefreshControl
#define COCOAPODS_POD_AVAILABLE_ODRefreshControl
#define COCOAPODS_VERSION_MAJOR_ODRefreshControl 1
#define COCOAPODS_VERSION_MINOR_ODRefreshControl 1
#define COCOAPODS_VERSION_PATCH_ODRefreshControl 0

// Parse
#define COCOAPODS_POD_AVAILABLE_Parse
#define COCOAPODS_VERSION_MAJOR_Parse 1
#define COCOAPODS_VERSION_MINOR_Parse 7
#define COCOAPODS_VERSION_PATCH_Parse 1

// ParseCrashReporting
#define COCOAPODS_POD_AVAILABLE_ParseCrashReporting
#define COCOAPODS_VERSION_MAJOR_ParseCrashReporting 1
#define COCOAPODS_VERSION_MINOR_ParseCrashReporting 7
#define COCOAPODS_VERSION_PATCH_ParseCrashReporting 1

// ParseFacebookUtils
#define COCOAPODS_POD_AVAILABLE_ParseFacebookUtils
#define COCOAPODS_VERSION_MAJOR_ParseFacebookUtils 1
#define COCOAPODS_VERSION_MINOR_ParseFacebookUtils 7
#define COCOAPODS_VERSION_PATCH_ParseFacebookUtils 1

// ParseUI
#define COCOAPODS_POD_AVAILABLE_ParseUI
#define COCOAPODS_VERSION_MAJOR_ParseUI 1
#define COCOAPODS_VERSION_MINOR_ParseUI 1
#define COCOAPODS_VERSION_PATCH_ParseUI 3

